import cascading.flow.FlowProcess;
import cascading.operation.AggregatorCall;
import cascading.operation.FilterCall;
import cascading.operation.FunctionCall;
import cascading.tuple.Tuple;
import cascalog.CascalogAggregator;
import cascalog.CascalogFilter;
import cascalog.CascalogFunction;
import jcascalog.Api;
import jcascalog.Subquery;
import jcascalog.op.Avg;
import jcascalog.op.Count;
import jcascalog.op.Sum;
import org.apache.log4j.Logger;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class RFMCalc {
    public static class ParseCustomerAddressRecord extends CascalogFunction {
        public void operate(FlowProcess process, FunctionCall call) {
            String line = call.getArguments().getString(0);
            String lineWithoutEscapeSequences = RemoveEscapeSequences(line);
            String[] parts = lineWithoutEscapeSequences.split(",");

            //`id`, `customer_id`, `first_name`, `last_name`, `company`, `address1`, `address2`, `city`, `province`, `country`, `province_code`, `country_code`, `address_type`, `zip`, `telephone`, `created`, `modified`
            //String id = parts[0]; // not nullable, Note: the customer id is actually the id column in the customer_addresses table
            String customerId = (parts.length > 1) ? parts[1] : null; // not nullable
            String firstName = (parts.length > 2) ? parts[2] : null;
            String lastName = (parts.length > 3) ? parts[3] : null;
            String company = (parts.length > 4) ? parts[4] : null;
            String address1 = (parts.length > 5) ? parts[5] : null;
            String address2 = (parts.length > 6) ? parts[6] : null;
            String city = (parts.length > 7) ? parts[7] : null;
            String province = (parts.length > 8) ? parts[8] : null;
            String country = (parts.length > 9) ? parts[9] : null;
            String provinceCode = (parts.length > 10) ? parts[10] : null;
            String countryCode = (parts.length > 11) ? parts[11] : null;
            String addressType = (parts.length > 12) ? parts[12] : null;
            String zip = (parts.length > 13) ? parts[13] : null;
            String telephone = (parts.length > 14) ? parts[14] : null;
            String created = (parts.length > 15) ? parts[15] : null;
            String modified = (parts.length > 16) ? parts[16] : null;

            call.getOutputCollector().add(new Tuple(customerId, firstName, lastName, company, address1, address2, city, province, country, provinceCode, countryCode, addressType, zip,
                    telephone, created, modified));
        }
    }

    public static Subquery ParseCustomerAddressData(String path)
    {
        return new Subquery("?id", "!firstName", "!lastName", "!company", "!address1", "!address2", "!city", "!province", "!country", "!provinceCode", "!countryCode",
            "!addressType", "!zip", "!telephone", "!created", "!modified")
            .predicate(Api.hfsTextline(path), "?line")
            .predicate(new ParseCustomerAddressRecord(), "?line")
            .out("?id", "!firstName", "!lastName", "!company", "!address1", "!address2", "!city", "!province", "!country", "!provinceCode", "!countryCode",
                    "!addressType", "!zip", "!telephone", "!created", "!modified");
    }

    public static class ParseCustomerRecord extends CascalogFunction {
        public void operate(FlowProcess process, FunctionCall call) {
            String line = call.getArguments().getString(0);
            String lineWithoutEscapeSequences = RemoveEscapeSequences(line);
            String[] parts = lineWithoutEscapeSequences.split(",");

            //id,application_id,customer_id,customer_group,first_name,last_name,email,optin_newsletter,created_at,updated_at,is_synced,created,modified,consumer_customer_id,is_partial_data
            String id = parts[0];
            String applicationId = parts[1];
            String customerId = parts[2];
            String customerGroup = parts[3];
            String firstName = NormalizeName(parts[4]);
            String lastName = NormalizeName(parts[5]);
            String email = parts[6];
            String optInNewsletter = parts[7];
            String createdAt = parts[8];
            String updatedAt = parts[9];
            String isSynced = parts[10];
            String created = parts[11];
            String modified = parts[12];
            String consumerCustomerId = parts[13];
            String isPartialData = parts[14];

            call.getOutputCollector().add(new Tuple(id, applicationId, customerId, customerGroup, firstName, lastName, email, optInNewsletter, createdAt, updatedAt, isSynced, created,
                    modified, consumerCustomerId, isPartialData));
        }
    }

    public static Subquery ParseCustomerData(String path)
    {
        return new Subquery("?id", "?applicationId", "!customerId", "!customerGroup", "!firstName", "!lastName", "?email", "!optInNewsletter", "!createdAt", "!updatedAt", "!isSynced",
            "!created", "!modified", "!consumerCustomerId", "!isPartialData")
            .predicate(Api.hfsTextline(path), "?line")
            .predicate(new ParseCustomerRecord(), "?line")
            .out("?id", "?applicationId", "!customerId", "!customerGroup", "!firstName", "!lastName", "?email", "!optInNewsletter", "!createdAt", "!updatedAt", "!isSynced",
                    "!created", "!modified", "!consumerCustomerId", "!isPartialData");
    }

    public static class ParseOrderRecord extends CascalogFunction {
        public void operate(FlowProcess process, FunctionCall call) {
            String line = call.getArguments().getString(0);
            String lineWithoutEscapeSequences = RemoveEscapeSequences(line);
            String[] parts = lineWithoutEscapeSequences.split(",");

            //id,application_id,order_id,order_number,status,financial_status,customer_is_guest,customer_id,first_name,last_name,email,subtotal_price,total_discounts,store_credit,
            //total_price,currency_code,source,created_at,updated_at,customer_created_at,is_synced,billing_address_id,shipping_address_id,created,modified,consumer_order_id,previous_status,
            //is_partial_data
            String id = parts[0];
            String applicationId = parts[1];
            String orderId = parts[2];
            String orderNumber = parts[3];
            String status = parts[4];
            String financialStatus = parts[5];
            String customerIsGuest = parts[6];
            String customerId = parts[7];
            String firstName = NormalizeName(parts[8]);
            String lastName = NormalizeName(parts[9]);
            String email = parts[10];
            BigDecimal subTotalPrice = TryParseBigDecimal(parts[11]);
            BigDecimal totalDiscounts = TryParseBigDecimal(parts[12]);
            BigDecimal storeCredit = TryParseBigDecimal(parts[13]);
            BigDecimal totalPrice = TryParseBigDecimal(parts[14]);
            String currencyCode = parts[15];
            String source = parts[16];
            String createdAt = parts[17];
            String updatedAt = parts[18];
            String customerCreatedAt = parts[19];
            String isSynced = parts[20];
            String billingAddressId = parts[21];
            String shippingAddressId = parts[22];
            String created = parts[23];
            String modified = parts[24];
            String consumerOrderId = parts[25];
            String previousStatus = parts[26];
            String isPartialData = parts[27];

            call.getOutputCollector().add(new Tuple(id, applicationId, orderId, orderNumber, status, financialStatus, customerIsGuest, customerId, firstName, lastName, email, subTotalPrice,
                totalDiscounts, storeCredit, totalPrice, currencyCode, source, createdAt, updatedAt, customerCreatedAt, isSynced, billingAddressId, shippingAddressId, created, modified,
                consumerOrderId, previousStatus, isPartialData));
        }
    }

    public static Subquery ParseOrderData(String path)
    {
        return new Subquery("?id", "?applicationId", "!orderId", "!orderNumber", "!status", "!financialStatus", "!customerIsGuest", "!customerId", "!firstName", "!lastName", "!email",
            "!subTotalPrice", "!totalDiscounts", "!storeCredit", "!totalPrice", "!currencyCode", "!source", "!createdAt", "!updatedAt", "!customerCreatedAt", "?isSynced",
            "!billingAddressId", "!shippingAddressId", "!created", "!modified", "!consumerOrderId", "!previousStatus", "!isPartialData")
            .predicate(Api.hfsTextline(path), "?line")
            .predicate(new ParseOrderRecord(), "?line")
            .out("?id", "?applicationId", "!orderId", "!orderNumber", "!status", "!financialStatus", "!customerIsGuest", "!customerId", "!firstName", "!lastName", "!email",
                "!subTotalPrice", "!totalDiscounts", "!storeCredit", "!totalPrice", "!currencyCode", "!source", "!createdAt", "!updatedAt", "!customerCreatedAt", "?isSynced",
                "!billingAddressId", "!shippingAddressId", "!created", "!modified", "!consumerOrderId", "!previousStatus", "!isPartialData");
    }

    public static String RemoveEscapeSequences(String line) {
        // This is the escape sequence used to output the data (by default backslash escape commas in the string)
        // e.g. - sqoop --escaped-by \\ ...
        String escapeSequence = "\\,";
        String lineWithoutEscapeSequences = line;

        if (line != null && line.length() > 0 && line.contains(escapeSequence)) {
            String regexEscapeSequence = "\\\\,";
            lineWithoutEscapeSequences = line.replaceAll(regexEscapeSequence, "");
        }

        return lineWithoutEscapeSequences;
    }

    public static String NormalizeName(String name) {
        String normalizedName = name;

        if (name.length() > 0) {
            // Remove quotes and spaces from names
            normalizedName = name.replace("\"", "").replaceAll("\\s+", "");
        }

        return normalizedName;
    }

    public static BigDecimal TryParseBigDecimal(String decimalString) {
        BigDecimal decimal = null;

        try {
            decimal = new BigDecimal(decimalString);
        }
        catch (NumberFormatException ex) {
        }

        return decimal;
    }

    public static class GetMinDate extends CascalogAggregator
    {
        public void start(FlowProcess process, AggregatorCall call) {
            Date minDate = null;
            call.setContext(minDate);
        }

        public void aggregate(FlowProcess process, AggregatorCall call) {
            Date currentMinDate = (Date)call.getContext();
            String currentDateString = call.getArguments().getString(0);

            if (currentDateString.length() > 0) {
                currentDateString = currentDateString.replace("\"", "");
            }

            //2015-07-13 09:07:26
            SimpleDateFormat format = GetInputDateFormatter();
            Date currentDate = null;

            try {
                if (currentDateString != null && currentDateString.length() > 0) {
                    currentDate = format.parse(currentDateString);
                }

                if ((currentMinDate == null && currentDate != null) || (currentDate != null && currentDate.before(currentMinDate))) {
                    call.setContext(currentDate);
                }
            }
            catch (ParseException ex) {
                //ex.printStackTrace();
            }
        }

        public void complete(FlowProcess process, AggregatorCall call) {
            Date minDate = (Date) call.getContext();
            call.getOutputCollector().add(new Tuple(minDate));
        }
    }

    private static String formatDate(Date date) {
        String formattedDate = null;

        if (date != null) {
            SimpleDateFormat formatter = GetOutputDateFormatter();
            formattedDate = formatter.format(date);
        }

        return formattedDate;
    }

    public static class GetFirstOrderAmount extends CascalogAggregator
    {
        public void start(FlowProcess process, AggregatorCall call) {
            Date minDate = null;
            BigDecimal amount = null;
            call.setContext(new Tuple(minDate, amount));
        }

        public void aggregate(FlowProcess process, AggregatorCall call) {
            Tuple currentMinTuple = (Tuple)call.getContext();
            Date currentMinDate = (Date)currentMinTuple.getObject(0);

            String currentDateString = call.getArguments().getString(0);
            BigDecimal currentFirstAmount = (BigDecimal)call.getArguments().getObject(1);

            if (currentDateString.length() > 0) {
                currentDateString = currentDateString.replace("\"", "");
            }

            //2015-07-13 09:07:26
            SimpleDateFormat format = GetInputDateFormatter();
            Date currentDate = null;

            try {
                if (currentDateString != null && currentDateString.length() > 0) {
                    currentDate = format.parse(currentDateString);
                }

                if (currentMinDate == null || (currentDate != null && currentDate.before(currentMinDate))) {
                    call.setContext(new Tuple(currentDate, currentFirstAmount));
                }
            }
            catch (ParseException ex) {
                //ex.printStackTrace();
            }
        }

        public void complete(FlowProcess process, AggregatorCall call) {
            Tuple minTuple = (Tuple)call.getContext();
            BigDecimal firstAmount = (BigDecimal)minTuple.getObject(1);

            call.getOutputCollector().add(new Tuple(firstAmount));
        }
    }

    public static SimpleDateFormat GetInputDateFormatter() {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
    }

    public static SimpleDateFormat GetOutputDateFormatter() {
        return new SimpleDateFormat("MM/dd/yyyy", Locale.US);
    }

    public static class GetMaxDate extends CascalogAggregator
    {
        public void start(FlowProcess process, AggregatorCall call) {
            Date maxDate = null;
            call.setContext(maxDate);
        }

        public void aggregate(FlowProcess process, AggregatorCall call) {
            Date currentMaxDate = (Date)call.getContext();
            String currentDateString = call.getArguments().getString(0);

            if (currentDateString.length() > 0) {
                currentDateString = currentDateString.replace("\"", "");
            }

            //2015-07-13 09:07:26
            SimpleDateFormat format = GetInputDateFormatter();
            Date currentDate = null;

            try {
                if (currentDateString != null && currentDateString.length() > 0) {
                    currentDate = format.parse(currentDateString);
                }

                if (currentMaxDate == null || (currentDate != null && currentDate.after(currentMaxDate))) {
                    call.setContext(currentDate);
                }
            }
            catch (ParseException ex) {
                //ex.printStackTrace();
            }
        }

        public void complete(FlowProcess process, AggregatorCall call) {
            Date maxDate =  (Date) call.getContext();
            call.getOutputCollector().add(new Tuple(maxDate));
        }
    }

    public static class GetLastOrderAmount extends CascalogAggregator
    {
        public void start(FlowProcess process, AggregatorCall call) {
            Date maxDate = null;
            BigDecimal amount = null;
            call.setContext(new Tuple(maxDate, amount));
        }

        public void aggregate(FlowProcess process, AggregatorCall call) {
            Tuple currentMaxTuple = (Tuple)call.getContext();
            Date currentMaxDate = (Date)currentMaxTuple.getObject(0);

            String currentDateString = call.getArguments().getString(0);
            BigDecimal currentLastAmount = (BigDecimal)call.getArguments().getObject(1);

            if (currentDateString.length() > 0) {
                currentDateString = currentDateString.replace("\"", "");
            }

            //2015-07-13 09:07:26
            SimpleDateFormat format = GetInputDateFormatter();
            Date currentDate = null;

            try {
                currentDate = format.parse(currentDateString);

                if (currentMaxDate == null || (currentDate != null && currentDate.after(currentMaxDate))) {
                    call.setContext(new Tuple(currentDate, currentLastAmount));
                }
            }
            catch (ParseException ex) {
                //ex.printStackTrace();
            }
        }

        public void complete(FlowProcess process, AggregatorCall call) {
            Tuple minTuple = (Tuple)call.getContext();
            BigDecimal lastAmount = (BigDecimal)minTuple.getObject(1);

            call.getOutputCollector().add(new Tuple(lastAmount));
        }
    }

    public static class GetLatestString extends CascalogAggregator
    {
        public void start(FlowProcess process, AggregatorCall call) {
            Date maxDate = null;
            String name = null;
            call.setContext(new Tuple(maxDate, name));
        }

        public void aggregate(FlowProcess process, AggregatorCall call) {
            Tuple currentMaxTuple = (Tuple)call.getContext();
            Date currentMaxDate = (Date)currentMaxTuple.getObject(0);

            String currentDateString = call.getArguments().getString(0);
            String currentName = call.getArguments().getString(1);

            if (currentDateString.length() > 0) {
                currentDateString = currentDateString.replace("\"", "");
            }

            //2015-07-13 09:07:26
            SimpleDateFormat format = GetInputDateFormatter();
            Date currentDate = null;

            try {
                currentDate = format.parse(currentDateString);

                if (currentMaxDate == null || (currentDate != null && currentDate.after(currentMaxDate))) {
                    call.setContext(new Tuple(currentDate, currentName));
                }
            }
            catch (ParseException ex) {
                //ex.printStackTrace();
            }
        }

        public void complete(FlowProcess process, AggregatorCall call) {
            Tuple minTuple = (Tuple)call.getContext();
            String latestName = minTuple.getString(1);

            call.getOutputCollector().add(new Tuple(latestName));
        }
    }

    public static class GetAverageDaysBetweenOrders extends CascalogFunction {
        public void operate(FlowProcess process, FunctionCall call) {
            Date firstOrderDate = (Date)call.getArguments().getObject(0);
            Date lastOrderDate = (Date)call.getArguments().getObject(1);
            Integer orderCount = call.getArguments().getInteger(2);
            Integer averageDaysBetweenOrders = null;

            if (firstOrderDate != null && lastOrderDate != null && orderCount != null && orderCount > 1) {
                // (last order date - first order date) / (number of orders - 1)
                long daysBetweenOrders = GetDifferenceDays(firstOrderDate, lastOrderDate);
                averageDaysBetweenOrders = (int)daysBetweenOrders / (orderCount - 1);
            }

            call.getOutputCollector().add(new Tuple(averageDaysBetweenOrders));
        }
    }

    public static class FormatDateAsString extends CascalogFunction {
        public void operate(FlowProcess process, FunctionCall call) {
            Date date = (Date) call.getArguments().getObject(0);
            String formattedDate = formatDate(date);

            call.getOutputCollector().add(new Tuple(formattedDate));
        }
    }

    public static class FormatCurrency extends CascalogFunction {
        public void operate(FlowProcess process, FunctionCall call) {
            String formattedCurrency = null;
            String decimalString = call.getArguments().getString(0);

            if (decimalString != null && decimalString.length() > 0) {
                BigDecimal decimal = new BigDecimal(decimalString);
                DecimalFormatSymbols symbols = new DecimalFormatSymbols();
                DecimalFormat formatter = new DecimalFormat("0.00", symbols);

                decimal.setScale(2, BigDecimal.ROUND_HALF_UP);
                symbols.setDecimalSeparator('.');

                formattedCurrency = formatter.format(decimal);
            }

            call.getOutputCollector().add(new Tuple(formattedCurrency));
        }
    }

    public static class GetFullName extends CascalogFunction {
        public void operate(FlowProcess process, FunctionCall call) {
            String firstName = call.getArguments().getString(0);
            String lastName = call.getArguments().getString(1);
            String fullName = String.format("%s %s", firstName, lastName);

            call.getOutputCollector().add(new Tuple(fullName));
        }
    }

    public static class BlankColumn extends CascalogFunction {
        public void operate(FlowProcess process, FunctionCall call) {
            call.getOutputCollector().add(new Tuple(""));
        }
    }

    public static long GetDifferenceDays(Date d1, Date d2) {
        long diff = d2.getTime() - d1.getTime();
        return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
    }

    public static class DateIsWithinRangeFilter extends CascalogFilter {
        public boolean isKeep(FlowProcess process, FilterCall call) {
            String currentDateString = call.getArguments().getString(0);
            String startDateString = call.getArguments().getString(1);
            String endDateString = call.getArguments().getString(2);

            if (currentDateString != null && currentDateString.length() > 0) {
                currentDateString = currentDateString.replace("\"", "");
            }

            if (startDateString != null && startDateString.length() > 0) {
                startDateString = startDateString.replace("\"", "");
            }
            else {
                return true;
            }

            if (endDateString != null && endDateString.length() > 0) {
                endDateString = endDateString.replace("\"", "");
            }
            else {
                return true;
            }

            //2015-07-13 09:07:26
            SimpleDateFormat inputDateFormatter = GetInputDateFormatter();
            SimpleDateFormat outputDateFormatter = GetOutputDateFormatter();
            Date currentDate = null;
            Date startDate = null;
            Date endDate = null;

            try {
                currentDate = inputDateFormatter.parse(currentDateString);
                startDate = outputDateFormatter.parse(startDateString);
                endDate = outputDateFormatter.parse(endDateString);

                if (currentDate != null && startDate != null && endDate != null && currentDate.compareTo(startDate) >= 0 && currentDate.compareTo(endDate) <= 0) {
                    return true;
                }
            }
            catch (ParseException ex) {
                //ex.printStackTrace();
            }

            return false;
        }
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        String customersPath = args[0];
        String customerAddressesPath = args[1];
        String ordersPath = args[2];
        String resultsPath = args[3];
        String startDateString = (args.length > 4) ? args[4] : null;// "01/01/2015";
        String endDateString = (args.length > 5) ? args[5] : null;//12/31/2015";

        final Logger logger = Logger.getLogger(RFMCalc.class);

        logger.info(String.format("Starting %s.", RFMCalc.class));
        logger.info(String.format("Customers path: %s", customersPath));
        logger.info(String.format("Customers address path: %s", customerAddressesPath));
        logger.info(String.format("Orders path: %s", ordersPath));
        logger.info(String.format("Results path: %s", resultsPath));

        if (startDateString != null && startDateString.length() > 0) {
            logger.info(String.format("Start date: %s", startDateString));
        }

        if (endDateString != null && endDateString.length() > 0) {
            logger.info(String.format("End date: %s", endDateString));
        }

        String[] outputColumns = new String[] { "!customerId", "!latestFullName", "!company", "!customerGroup", "!latestCity", "!latestState", "!latestCountry", "?email",
                "!blankColumn1", "!blankColumn2", "!formattedSumOfAllOrders", "!blankColumn3", "!blankColumn4", "!orderCount", "!formattedDateOfFirstOrder", "!formattedDateOfLastOrder", "!averageDaysBetweenOrders", "!formattedFirstOrderAmount",
                "!formattedLastOrderAmount", "!formattedAvgOrderPrice", "!formattedCustomerCreatedAtMinDate" };

        Subquery rfmQuery = new Subquery(outputColumns)
            .predicate(ParseCustomerData(customersPath), "?id", "?applicationId", "!customerId", "!customerGroup", "!firstName", "!lastName", "?email", "_", "_", "_", "_", "_", "_", "_", "_")
            .predicate(ParseOrderData(ordersPath), "_", "?applicationId", "!orderId", "_", "_", "_", "_", "!customerId", "_", "_", "_", "_", "_", "_", "!totalPrice", "_", "_",
                    "!createdAt", "_", "!customerCreatedAt", "_", "_", "_", "_", "_", "_", "_", "_")
            .predicate(ParseCustomerAddressData(customerAddressesPath), "?id", "_", "_", "!company", "_", "_", "!city", "!province", "!country", "_", "_",
                    "_", "_", "_", "_", "_")
            .predicate(new Count(), "!orderCount")
            .predicate(new Sum(), "!totalPrice").out("!sumOfAllOrders")
            .predicate(new Avg(), "!totalPrice").out("!avgOrderPrice")
            .predicate(new GetLatestString(), "!createdAt", "!firstName").out("!latestFirstName")
            .predicate(new GetLatestString(), "!createdAt", "!lastName").out("!latestLastName")
            .predicate(new GetFullName(), "!latestFirstName", "!latestLastName").out("!latestFullName")
            .predicate(new GetLatestString(), "!createdAt", "!city").out("!latestCity")
            .predicate(new GetLatestString(), "!createdAt", "!province").out("!latestState")
            .predicate(new GetLatestString(), "!createdAt", "!country").out("!latestCountry")
            .predicate(new GetMinDate(), "!createdAt").out("!dateOfFirstOrder")
            .predicate(new GetMaxDate(), "!createdAt").out("!dateOfLastOrder")
            .predicate(new GetMinDate(), "!customerCreatedAt").out("!customerCreatedAtMinDate")
            .predicate(new GetAverageDaysBetweenOrders(), "!dateOfFirstOrder", "!dateOfLastOrder", "!orderCount").out("!averageDaysBetweenOrders")
            .predicate(new GetFirstOrderAmount(), "!createdAt", "!totalPrice").out("!firstOrderAmount")
            .predicate(new GetLastOrderAmount(), "!createdAt", "!totalPrice").out("!lastOrderAmount")
            .predicate(new FormatDateAsString(), "!dateOfFirstOrder").out("!formattedDateOfFirstOrder")
            .predicate(new FormatDateAsString(), "!dateOfLastOrder").out("!formattedDateOfLastOrder")
            .predicate(new FormatDateAsString(), "!customerCreatedAtMinDate").out("!formattedCustomerCreatedAtMinDate")
            .predicate(new FormatCurrency(), "!sumOfAllOrders").out("!formattedSumOfAllOrders")
            .predicate(new FormatCurrency(), "!avgOrderPrice").out("!formattedAvgOrderPrice")
            .predicate(new FormatCurrency(), "!firstOrderAmount").out("!formattedFirstOrderAmount")
            .predicate(new FormatCurrency(), "!lastOrderAmount").out("!formattedLastOrderAmount")
            .predicate(new BlankColumn()).out("!blankColumn1")
            .predicate(new BlankColumn()).out("!blankColumn2")
            .predicate(new BlankColumn()).out("!blankColumn3")
            .predicate(new BlankColumn()).out("!blankColumn4")
            .predicate(new DateIsWithinRangeFilter(), "!createdAt", startDateString, endDateString);

        Api.execute(Api.hfsTextline(resultsPath),
            new Subquery(outputColumns)
            .predicate(rfmQuery, outputColumns)
        );

        logger.info(String.format("%s completed.", RFMCalc.class));

        // To convert the output to be a sorted csv sorted by largest to smallest customer
        // cat results/part-00000 | sort -t$'\t' -r -nk11 | tr "\\t" "," > results/centsofstyle_rfm_TEST.csv

        // To verify the output results don't contain duplicate customer id's.  If it does, the output is invalid.
        // cat results/part-00000 | awk '{print $1}' | sort -u -r -nk1 | wc -l (compare output to, should be the same as) cat results/part-00000 | wc -l

        // DONE: Order records from sum of all orders from greatest to least (probably can't do because sorting is done pre-aggregation => use sort bash script,
        //  sort -t$'\t' -r -nk5 /tmp/unsorted_rfm.csv/part-00000)
        // DONE: Output results to a file
        // DONE: take paths from input args
        // DONE: Make date output consistent
        // DONE: Round monetary values to 2 decimal places
        // DONE: add post build event to remove manifest files to be able to run jar file from command line
        // DONE: check code into github
        // DONE: Add missing columns for RFM spreadsheet
        // DONE: Setup dev servers on Rackspace
        // DONE: Aggregate duplicate customers together
        // DONE: Talk with Parag about average days between orders being wrong per Justin
        // X: Get marketing system ids (not needed)
        // DONE: Move github repos to RevenueConduitAnalytics
        // DONE: Total amount and number of orders is off for cents of style (per Justin), investigate and fix!
        // DONE: Add processing on date range (Currently not working on subset of dates)
        // DONE: Get cents of style 2015 rfm values and upload to dropbox for Justin
        // TODO: Get working with all clients
        // X: Check / deal with invalid date parsing
        // TODO: Create bash script to run RFM and sort results?
        // TODO: Upload results to dropbox (copy to dropbox folder?)
    }
}