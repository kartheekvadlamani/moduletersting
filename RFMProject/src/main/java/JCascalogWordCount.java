import cascading.flow.FlowProcess;
import cascading.operation.FunctionCall;
import cascading.tuple.Tuple;
import cascalog.CascalogFunction;

import com.twitter.maple.tap.StdoutTap;

import jcascalog.Api;
import jcascalog.Playground;
import jcascalog.Subquery;
import jcascalog.op.Count;

public class JCascalogWordCount {
    public static class Split extends CascalogFunction {
        public void operate(FlowProcess process, FunctionCall call) {
            String sentence = call.getArguments().getString(0);
            for (String word: sentence.split(" ")) {
                call.getOutputCollector().add(new Tuple(word));
            }
        }
    }
    /**
     * @param args
     */
    public static void main(String[] args) {
        Api.execute(new StdoutTap(),
                new Subquery("?word", "?count")
                        .predicate(Api.hfsTextline("/tmp/pg4300.txt"), "?sentence")
                        .predicate(new Split(), "?sentence").out("?word")
                        .predicate(new Count(), "?count"));
    }
}