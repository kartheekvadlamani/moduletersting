import com.datastax.spark.connector.ColumnRef;
import com.datastax.spark.connector.cql.TableDef;
import com.datastax.spark.connector.writer.RowWriter;
import com.datastax.spark.connector.writer.RowWriterFactory;
import scala.collection.IndexedSeq;

import java.io.Serializable;

/**
 * Created by brettcooper on 8/25/16.
 */
public class CustomerAddressWriterFactory implements RowWriterFactory<CustomerAddress>, Serializable {
    private static final CustomerAddressWriter rowWriter = new CustomerAddressWriter();

    public RowWriter<CustomerAddress> rowWriter(TableDef tableDef, IndexedSeq<ColumnRef> columns) {
        return rowWriter;
    }
}


