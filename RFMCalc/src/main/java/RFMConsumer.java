import com.datastax.driver.core.Session;
import com.datastax.spark.connector.cql.CassandraConnector;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static com.datastax.spark.connector.japi.CassandraJavaUtil.javaFunctions;

/**
 * Created by brettcooper on 8/26/16.
 */
public class RFMConsumer implements Runnable {
    private static final Integer SLEEP_TIME_MINUTES = 1;
    private static Logger logger = LoggerFactory.getLogger(RFMConsumer.class);
    private final String cassandraSchemaName;
    private final String cassandraRfmLastRunTableName;
    private final String cassandraCustomersTableName;
    private final String cassandraCustomerAddressesTableName;
    private final String cassandraOrdersTableName;
    private final String cassandraRFMSpeedResultsTableName;
    private final String cassandraSummaryStatisticsSpeedTableName;
    private final String cassandraTop1PercentSummaryStatisticsSpeedTableName;
    private final String cassandraTop5PercentSummaryStatisticsSpeedTableName;
    private final String cassandraTop15PercentSummaryStatisticsSpeedTableName;
    private final String cassandraTop25PercentSummaryStatisticsSpeedTableName;
    private final String cassandraTop50PercentSummaryStatisticsSpeedTableName;
    private final String cassandraBottom50PercentSummaryStatisticsSpeedTableName;
    private JavaSparkContext sparkContext;

    public RFMConsumer(JavaSparkContext sparkContext, String cassandraSchemaName,
                       String cassandraRfmLastRunTableName, String cassandraCustomersTableName,
                       String cassandraCustomerAddressesTableName, String cassandraOrdersTableName,
                       String cassandraRFMSpeedResultsTableName,
                       String cassandraSummaryStatisticsSpeedTableName, String cassandraTop1PercentSummaryStatisticsSpeedTableName, String cassandraTop5PercentSummaryStatisticsSpeedTableName, String cassandraTop15PercentSummaryStatisticsSpeedTableName,
                       String cassandraTop25PercentSummaryStatisticsSpeedTableName, String cassandraTop50PercentSummaryStatisticsSpeedTableName, String cassandraBottom50PercentSummaryStatisticsSpeedTableName) {
        this.sparkContext = sparkContext;
        this.cassandraSchemaName = cassandraSchemaName;
        this.cassandraRfmLastRunTableName = cassandraRfmLastRunTableName;
        this.cassandraCustomersTableName = cassandraCustomersTableName;
        this.cassandraCustomerAddressesTableName = cassandraCustomerAddressesTableName;
        this.cassandraOrdersTableName = cassandraOrdersTableName;
        this.cassandraRFMSpeedResultsTableName = cassandraRFMSpeedResultsTableName;
        this.cassandraSummaryStatisticsSpeedTableName = cassandraSummaryStatisticsSpeedTableName;
        this.cassandraTop1PercentSummaryStatisticsSpeedTableName = cassandraTop1PercentSummaryStatisticsSpeedTableName;
        this.cassandraTop5PercentSummaryStatisticsSpeedTableName = cassandraTop5PercentSummaryStatisticsSpeedTableName;
        this.cassandraTop15PercentSummaryStatisticsSpeedTableName =
                cassandraTop15PercentSummaryStatisticsSpeedTableName;
        this.cassandraTop25PercentSummaryStatisticsSpeedTableName =
                cassandraTop25PercentSummaryStatisticsSpeedTableName;
        this.cassandraTop50PercentSummaryStatisticsSpeedTableName =
                cassandraTop50PercentSummaryStatisticsSpeedTableName;
        this.cassandraBottom50PercentSummaryStatisticsSpeedTableName =
                cassandraBottom50PercentSummaryStatisticsSpeedTableName;
    }

    private static JavaRDD<Customer> getCustomerJavaRDDByApplication(JavaRDD<Customer> customersRDD,
                                                                     int applicationId) {
        return customersRDD.filter((Function<Customer, Boolean>) customer -> {
            if (customer != null && customer.getApplicationId() != null) {
                return customer.getApplicationId() == applicationId;
            } else {
                return false;
            }
        });
    }

    private static JavaRDD<CustomerAddress> getCustomerAddressJavaRDDByApplication(
            JavaRDD<CustomerAddress> customerAddressesRDD, int applicationId) {
        return customerAddressesRDD.filter((Function<CustomerAddress, Boolean>) customerAddress -> {
            if (customerAddress != null && customerAddress.getApplicationId() != null) {
                return customerAddress.getApplicationId() == applicationId;
            } else {
                return false;
            }
        });
    }

    private static JavaRDD<Order> getOrdersJavaRDDByApplication(JavaRDD<Order> ordersRDD, int applicationId) {
        return ordersRDD.filter((Function<Order, Boolean>) order -> {
            if (order != null && order.getApplicationId() != null) {
                return order.getApplicationId() == applicationId;
            } else {
                return false;
            }
        });
    }

    @Override
    public void run() {
        logger.info("RFM speed layer job started.");

        Cassandra.initializeSpeedLayerTables(sparkContext, logger, cassandraSchemaName,
                                             cassandraCustomersTableName, cassandraCustomerAddressesTableName,
                                             cassandraOrdersTableName, cassandraRFMSpeedResultsTableName,
                                             cassandraSummaryStatisticsSpeedTableName,
                                             cassandraTop1PercentSummaryStatisticsSpeedTableName,
                                             cassandraTop5PercentSummaryStatisticsSpeedTableName,
                                             cassandraTop15PercentSummaryStatisticsSpeedTableName,
                                             cassandraTop25PercentSummaryStatisticsSpeedTableName,
                                             cassandraTop50PercentSummaryStatisticsSpeedTableName,
                                             cassandraBottom50PercentSummaryStatisticsSpeedTableName);

        // Loop forever!
        while (true) {
            Session session = null;

            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Date now = new Date();
            String dateAndTimeNow = dateFormat.format(now);

            logger.info(String.format("Starting speed layer loop at %s.", dateAndTimeNow));

            try {
                CassandraConnector connector = CassandraConnector.apply(sparkContext.getConf());
                session = connector.openSession();

                // Get filtered customers, addresses and orders from Cassandra
                JavaRDD<Customer> filteredCustomersJavaRDD =
                        Cassandra.getSpeedLayerFilteredCustomers(sparkContext, session,
                                                                 cassandraSchemaName,
                                                                 cassandraRfmLastRunTableName,
                                                                 cassandraCustomersTableName);
                JavaRDD<CustomerAddress> filteredCustomerAddressJavaRDD =
                        Cassandra.getSpeedLayerFilteredCustomerAddresses(
                                sparkContext, session, cassandraSchemaName, cassandraRfmLastRunTableName,
                                cassandraCustomerAddressesTableName);
                JavaRDD<Order> filteredOrderJavaRDD = Cassandra.getSpeedLayerFilteredOrders(sparkContext, session,
                                                                                            cassandraSchemaName,
                                                                                            cassandraRfmLastRunTableName,
                                                                                            cassandraOrdersTableName);

                // Get unique customers, addresses and orders.  Filter by paid orders too.
                JavaRDD<Customer> filteredUniqueCustomers =
                        RFMSparkTransformations.getUniqueCustomers(filteredCustomersJavaRDD);

                JavaRDD<CustomerAddress> filteredUniqueCustomerAddresses =
                        RFMSparkTransformations.getUniqueCustomerAddresses(filteredCustomerAddressJavaRDD);

                JavaRDD<Order> filteredUniqueOrders = RFMSparkTransformations.getUniqueOrders(filteredOrderJavaRDD);

                // Only continue of there are filtered unique orders
                if (filteredUniqueOrders != null && !filteredUniqueOrders.isEmpty()) {
                    JavaRDD<Order> filteredUniquePaidOrders =
                            RFMSparkTransformations.getPaidOrders(filteredUniqueOrders);

                    // Only continue if there are filtered unique paid orders
                    if (filteredUniquePaidOrders != null && !filteredUniquePaidOrders.isEmpty()) {

                        // Get a list of customers that are grouped by application id
                        JavaPairRDD<String, String> customerPairsRdd =
                                RFMSparkTransformations.getCustomerPairsRdd(filteredUniqueCustomers);
                        JavaPairRDD<String, Iterable<String>> customersByApplication = customerPairsRdd.groupByKey();
                        List<String> customersApplicationIds = customersByApplication.keys().toArray();

                        logger.info(String.format("Starting RFM data speed layer calculation for %s applications.",
                                                  customersApplicationIds.size()));

                        // Loop through each application and calculate RFM, Summary Statistics and 80/20 summary stats
                        for (String applicationId : customersApplicationIds) {
                            int applicationIdInt = new Integer(applicationId);

                            // Get latest customer, addresses and orders from Cassandra speed tables
                            JavaRDD<Customer> customersRddByApplication =
                                    getCustomerJavaRDDByApplication(filteredUniqueCustomers, applicationIdInt);
                            JavaRDD<CustomerAddress> customerAddressesRddByApplication =
                                    getCustomerAddressJavaRDDByApplication(filteredUniqueCustomerAddresses,
                                                                           applicationIdInt);
                            JavaRDD<Order> ordersRddByApplication =
                                    getOrdersJavaRDDByApplication(filteredUniquePaidOrders, applicationIdInt);

                            // Calculate and save RFM data to Cassandra
                            JavaRDD<RFMData> rfmDataJavaRDD =
                                    calculateAndSaveRFMData(applicationIdInt, customersRddByApplication,
                                                            customerAddressesRddByApplication, ordersRddByApplication);

                            if (rfmDataJavaRDD != null && rfmDataJavaRDD.count() > 0) {
                                // Calculate and save summary statistics to Cassandra
                                calculateAndSaveSummaryStatistics(applicationIdInt, rfmDataJavaRDD);

                                // Calculate ans ave 80/20 summary stats
                                calculateAndSaveEightyTwentySummaryStatistics(applicationIdInt, rfmDataJavaRDD);
                            }
                        }
                    } else {
                        logger.info("There are currently no filtered unique paid orders.");
                    }
                } else {
                    logger.info("There are currently no filtered unique orders.");
                }

                logger.info(String.format("Sleeping for %s minute(s).", SLEEP_TIME_MINUTES));

                // Sleep for x minutes before running again
                Thread.sleep(SLEEP_TIME_MINUTES * 60 * 1000);
            } catch (Exception ex) {
                logger.error("Exception: {}", ex);
            } finally {
                if (session != null && !session.isClosed()) {
                    session.close();
                }
            }
        }
    }

    private void calculateAndSaveEightyTwentySummaryStatistics(int applicationIdInt, JavaRDD<RFMData> rfmDataJavaRDD) {
        List<JavaRDD<RFMSummaryStatistics>> eightyTwentySummaryStatisticsRdds =
                RFMSparkTransformations.calculateEightyTwentySummaryStatistics(
                        sparkContext, applicationIdInt, rfmDataJavaRDD);

        // Save 80/20 summary statistics to Cassandra
        javaFunctions(eightyTwentySummaryStatisticsRdds.get(0)).writerBuilder(cassandraSchemaName,
                                                                              cassandraTop1PercentSummaryStatisticsSpeedTableName,
                                                                              new RFMSummaryStatisticsWriterFactory())
                .saveToCassandra();
        javaFunctions(eightyTwentySummaryStatisticsRdds.get(1)).writerBuilder(cassandraSchemaName,
                                                                              cassandraTop5PercentSummaryStatisticsSpeedTableName,
                                                                              new RFMSummaryStatisticsWriterFactory())
                .saveToCassandra();
        javaFunctions(eightyTwentySummaryStatisticsRdds.get(2)).writerBuilder(cassandraSchemaName,
                                                                              cassandraTop15PercentSummaryStatisticsSpeedTableName,
                                                                              new RFMSummaryStatisticsWriterFactory())
                .saveToCassandra();
        javaFunctions(eightyTwentySummaryStatisticsRdds.get(3)).writerBuilder(cassandraSchemaName,
                                                                              cassandraTop25PercentSummaryStatisticsSpeedTableName,
                                                                              new RFMSummaryStatisticsWriterFactory())
                .saveToCassandra();
        javaFunctions(eightyTwentySummaryStatisticsRdds.get(4)).writerBuilder(cassandraSchemaName,
                                                                              cassandraTop50PercentSummaryStatisticsSpeedTableName,
                                                                              new RFMSummaryStatisticsWriterFactory())
                .saveToCassandra();
        javaFunctions(eightyTwentySummaryStatisticsRdds.get(5)).writerBuilder(cassandraSchemaName,
                                                                              cassandraBottom50PercentSummaryStatisticsSpeedTableName,
                                                                              new RFMSummaryStatisticsWriterFactory())
                .saveToCassandra();
    }

    private void calculateAndSaveSummaryStatistics(int applicationIdInt, JavaRDD<RFMData> rfmDataJavaRDD) {
        // Calculate summary statistics for application
        JavaRDD<RFMSummaryStatistics> rfmSummaryStatisticsJavaRDD = RFMSparkTransformations.calculateSummaryStatistics(
                sparkContext, applicationIdInt, rfmDataJavaRDD, rfmDataJavaRDD);

        // Save summary statistics to Cassandra
        javaFunctions(rfmSummaryStatisticsJavaRDD).writerBuilder(cassandraSchemaName,
                                                                 cassandraSummaryStatisticsSpeedTableName,
                                                                 new RFMSummaryStatisticsWriterFactory())
                .saveToCassandra();
    }

    private JavaRDD<RFMData> calculateAndSaveRFMData(int applicationIdInt, JavaRDD<Customer> customersRddByApplication, JavaRDD<CustomerAddress> customerAddressesRddByApplication, JavaRDD<Order> ordersRddByApplication) {
        // Calculate RFM data for this application
        JavaRDD<RFMData> rfmDataJavaRDD = RFMSparkTransformations
                .getRfmDataJavaRDD(applicationIdInt, customersRddByApplication, customerAddressesRddByApplication,
                                   ordersRddByApplication);

        if (rfmDataJavaRDD != null) {
            // Cache the RFM data before further processing
            rfmDataJavaRDD.cache();

            // Save the application's RFM data to Cassandra
            javaFunctions(rfmDataJavaRDD)
                    .writerBuilder(cassandraSchemaName, cassandraRFMSpeedResultsTableName,
                                   new RFMDataRowWriterFactory())
                    .saveToCassandra();
        }

        return rfmDataJavaRDD;
    }
}




