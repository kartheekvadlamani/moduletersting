import java.math.BigInteger;

/**
 * Created by brettcooper on 8/31/16.
 */
public class RFMLastRun {
    private Integer _id;
    private String _lastRunDate;
    private BigInteger _lastRunCustomerId;
    private Integer _lastRunCustomerAddressId;
    private Integer _lastRunOrderId;

    public RFMLastRun(Integer id, String lastRunDate, BigInteger lastRunCustomerId, Integer lastRunCustomerAddressId, Integer lastRunOrderId) {
        _id = id;
        _lastRunDate = lastRunDate;
        _lastRunCustomerId = lastRunCustomerId;
        _lastRunCustomerAddressId = lastRunCustomerAddressId;
        _lastRunOrderId = lastRunOrderId;
    }

    public Integer getId() { return _id; }
    public String getLastRunDate() { return _lastRunDate; }
    public BigInteger getlastRunCustomerId() { return _lastRunCustomerId; }
    public Integer getlastRunCustomerAddressId() { return _lastRunCustomerAddressId; }
    public Integer getlastRunOrderId() { return _lastRunOrderId; }
}
