import com.datastax.spark.connector.ColumnRef;
import com.datastax.spark.connector.cql.TableDef;
import com.datastax.spark.connector.writer.RowWriter;
import com.datastax.spark.connector.writer.RowWriterFactory;
import scala.collection.IndexedSeq;

import java.io.Serializable;

/**
 * Created by brettcooper on 5/12/16.
 */
public class RFMDataRowWriterFactory implements RowWriterFactory<RFMData>, Serializable {
    private static final RFMDataRowWriter rowWriter = new RFMDataRowWriter();

    public RowWriter<RFMData> rowWriter(TableDef tableDef, IndexedSeq<ColumnRef> columns) {
        return rowWriter;
    }
}
