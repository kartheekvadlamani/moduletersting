/**
 * Created by brettcooper on 8/25/16.
 */
import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ProtocolVersion;
import com.datastax.spark.connector.writer.RowWriter;
import scala.collection.JavaConversions;
import scala.collection.immutable.IndexedSeq;
import scala.collection.immutable.Seq;

import java.io.Serializable;
import java.util.Arrays;

public class CustomerWriter implements RowWriter<Customer>, Serializable {
    // Return the list of column names in the Cassandra table
    public Seq<String> columnNames() {
        return toIndexedSeq(Arrays.asList("id", "application_id", "customer_id", "customer_group", "first_name", "last_name", "email", "opt_in_newsletter", "created_at", "updated_at", "is_synced", "created", "modified", "consumer_customer_id", "is_partial_data"));
    }

    public int estimateSizeInBytes(Customer customer) {
        return 100;
    }

    public BoundStatement bind(Customer customer, PreparedStatement statement, ProtocolVersion version) {
        BoundStatement boundStatement = new BoundStatement(statement);

        boundStatement.bind(customer.getId(), customer.getApplicationId(), customer.getCustomerId(), customer.getCustomerGroup(), customer.getFirstName(), customer.getLastName(), customer.getEmail(), customer.getOptInNewsletter(), customer.getCreatedAt(), customer.getUpdatedAt(),
                customer.getIsSynced(), customer.getCreated(), customer.getModified(), customer.getConsumerCustomerId(), customer.getIsPartialData());

        return boundStatement;
    }

    private <T> IndexedSeq<T> toIndexedSeq(Iterable<T> iterable) {
        return JavaConversions.asScalaIterable(iterable).toIndexedSeq();
    }

    public void readColumnValues(Customer customer, Object[] buffer) {
        buffer[0] = customer.getId();
        buffer[1] = customer.getApplicationId();
        buffer[2] = customer.getCustomerId();
        buffer[3] = customer.getCustomerGroup();
        buffer[4] = customer.getFirstName();
        buffer[5] = customer.getLastName();
        buffer[6] = customer.getEmail();
        buffer[7] = customer.getOptInNewsletter();
        buffer[8] = customer.getCreatedAt();
        buffer[9] = customer.getUpdatedAt();
        buffer[10] = customer.getIsSynced();
        buffer[11] = customer.getCreated();
        buffer[12] = customer.getModified();
        buffer[13] = customer.getConsumerCustomerId();
        buffer[14] = customer.getIsPartialData();
    }
}




