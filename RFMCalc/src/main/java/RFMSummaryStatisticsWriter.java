import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ProtocolVersion;
import com.datastax.spark.connector.writer.RowWriter;
import scala.collection.JavaConversions;
import scala.collection.immutable.IndexedSeq;
import scala.collection.immutable.Seq;

import java.io.Serializable;
import java.util.Arrays;

/**
 * Created by brettcooper on 5/18/16.
 */
public class RFMSummaryStatisticsWriter implements RowWriter<RFMSummaryStatistics>, Serializable {
    // Return the list of column names in the Cassandra table
    public Seq<String> columnNames() {
        return toIndexedSeq(Arrays.asList("application_id", "total_customers", "total_amount_of_orders", "percentage_total_amount_of_orders", "average_order_value", "average_customer_lifetime_value", "total_number_of_orders", "percentage_total_number_of_orders", "average_number_of_orders",
                "percentage_of_repeat_buyers", "percentage_revenue_from_repeat_buyers", "average_days_since_last_order"));
    }

    public int estimateSizeInBytes(RFMSummaryStatistics rfmSummaryStatistics) {
        return 100;
    }

    public BoundStatement bind(RFMSummaryStatistics rfmSummaryStatistics, PreparedStatement statement, ProtocolVersion version) {
        BoundStatement boundStatement = new BoundStatement(statement);

        boundStatement.bind(rfmSummaryStatistics.getApplicationId(), rfmSummaryStatistics.getTotalCustomers(), rfmSummaryStatistics.getTotalAmountOfOrders(), rfmSummaryStatistics.getPercentageTotalAmountOfOrders(), rfmSummaryStatistics.getAverageOrderValue(),
                rfmSummaryStatistics.getAverageCustomerLifetimeValue(), rfmSummaryStatistics.getTotalNumberOfOrders(), rfmSummaryStatistics.getPercentageTotalNumberOfOrders(), rfmSummaryStatistics.getAverageNumberOfOrders(), rfmSummaryStatistics.getPercentageOfRepeatBuyers(),
                rfmSummaryStatistics.getPercentageRevenueFromRepeatBuyers(), rfmSummaryStatistics.getAverageDaysSinceLastOrder());

        return boundStatement;
    }

    private <T> IndexedSeq<T> toIndexedSeq(Iterable<T> iterable) {
        return JavaConversions.asScalaIterable(iterable).toIndexedSeq();
    }

    public void readColumnValues(RFMSummaryStatistics rfmSummaryStatistics, Object[] buffer) {
        buffer[0] = rfmSummaryStatistics.getApplicationId();
        buffer[1] = rfmSummaryStatistics.getTotalCustomers();
        buffer[2] = rfmSummaryStatistics.getTotalAmountOfOrders();
        buffer[3] = rfmSummaryStatistics.getPercentageTotalAmountOfOrders();
        buffer[4] = rfmSummaryStatistics.getAverageOrderValue();
        buffer[5] = rfmSummaryStatistics.getAverageCustomerLifetimeValue();
        buffer[6] = rfmSummaryStatistics.getTotalNumberOfOrders();
        buffer[7] = rfmSummaryStatistics.getPercentageTotalNumberOfOrders();
        buffer[8] = rfmSummaryStatistics.getAverageNumberOfOrders();
        buffer[9] = rfmSummaryStatistics.getPercentageOfRepeatBuyers();
        buffer[10] = rfmSummaryStatistics.getPercentageRevenueFromRepeatBuyers();
        buffer[11] = rfmSummaryStatistics.getAverageDaysSinceLastOrder();
    }
}



