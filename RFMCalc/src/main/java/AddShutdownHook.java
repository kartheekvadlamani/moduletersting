import org.apache.spark.api.java.JavaSparkContext;

/**
 * Created by brettcooper on 9/1/16.
 */
public class AddShutdownHook {
    JavaSparkContext _sparkContext = null;

    public AddShutdownHook(JavaSparkContext sparkContext) {
        _sparkContext = sparkContext;
    }

    public void attachShutDownHook() {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                if (_sparkContext != null) {
                    _sparkContext.stop();
                }
            }
        });
    }
}