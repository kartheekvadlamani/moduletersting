import io.confluent.kafka.serializers.KafkaAvroDecoder;
import kafka.consumer.Consumer;
import kafka.consumer.ConsumerConfig;
import kafka.consumer.KafkaStream;
import kafka.javaapi.consumer.ConsumerConnector;
import kafka.utils.VerifiableProperties;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.IOException;
import java.math.BigInteger;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static com.datastax.spark.connector.japi.CassandraJavaUtil.javaFunctions;

/**
 * Created by brettcooper on 4/13/16.
 */
public class RFMCalc {
    public static final String DELIMITER = "|";
    private static final String CURRENT_DATE_TIME_PATH = RFMSparkTransformations.getCurrentDateTimePath();
    private static final String CASSANDRA_SCHEMA_NAME = "revenue_conduit";
    private static final String CASSANDRA_RFM_RESULTS_TABLE_NAME = String.format("rfm_results_%s",
                                                                                 CURRENT_DATE_TIME_PATH);
    private static final String CASSANDRA_SUMMARY_STATISTICS_TABLE_NAME = String.format("summary_stats_%s",
                                                                                        CURRENT_DATE_TIME_PATH);
    private static final String
            CASSANDRA_TOP_1_PERCENT_SUMMARY_STATISTICS_TABLE_NAME = String.format("summary_stats_top_1_%s",
                                                                                  CURRENT_DATE_TIME_PATH);
    private static final String
            CASSANDRA_TOP_5_PERCENT_SUMMARY_STATISTICS_TABLE_NAME = String.format("summary_stats_top_5_%s",
                                                                                  CURRENT_DATE_TIME_PATH);
    private static final String
            CASSANDRA_TOP_15_PERCENT_SUMMARY_STATISTICS_TABLE_NAME = String.format("summary_stats_top_15_%s",
                                                                                   CURRENT_DATE_TIME_PATH);
    private static final String
            CASSANDRA_TOP_25_PERCENT_SUMMARY_STATISTICS_TABLE_NAME = String.format("summary_stats_top_25_%s",
                                                                                   CURRENT_DATE_TIME_PATH);
    private static final String
            CASSANDRA_TOP_50_PERCENT_SUMMARY_STATISTICS_TABLE_NAME = String.format("summary_stats_top_50_%s",
                                                                                   CURRENT_DATE_TIME_PATH);
    private static final String
            CASSANDRA_BOTTOM_50_PERCENT_SUMMARY_STATISTICS_TABLE_NAME = String.format("summary_stats_bottom_50_%s",
                                                                                      CURRENT_DATE_TIME_PATH);
    private static final String CASSANDRA_RFM_LAST_RUN_TABLE_NAME = "rfm_last_run";
    private static final String CASSANDRA_RFM_CUSTOMERS_TABLE = "rfm_customers";
    private static final String CASSANDRA_RFM_CUSTOMER_ADDRESSES_TABLE = "rfm_customer_addresses";
    private static final String CASSANDRA_RFM_ORDERS_TABLE = "rfm_orders";
    private static final String CASSANDRA_RFM_SPEED_RESULTS_TABLE_NAME = "rfm_results";
    private static final String CASSANDRA_SUMMARY_STATISTICS_SPEED_TABLE_NAME = "summary_stats";
    private static final String CASSANDRA_TOP_1_PERCENT_SUMMARY_STATISTICS_SPEED_TABLE_NAME = "summary_stats_top_1";
    private static final String CASSANDRA_TOP_5_PERCENT_SUMMARY_STATISTICS_SPEED_TABLE_NAME = "summary_stats_top_5";
    private static final String CASSANDRA_TOP_15_PERCENT_SUMMARY_STATISTICS_SPEED_TABLE_NAME = "summary_stats_top_15";
    private static final String CASSANDRA_TOP_25_PERCENT_SUMMARY_STATISTICS_SPEED_TABLE_NAME = "summary_stats_top_25";
    private static final String CASSANDRA_TOP_50_PERCENT_SUMMARY_STATISTICS_SPEED_TABLE_NAME = "summary_stats_top_50";
    private static final String CASSANDRA_BOTTOM_50_PERCENT_SUMMARY_STATISTICS_SPEED_TABLE_NAME =
            "summary_stats_bottom_50";
    private static final String IMPORT_CUSTOMERS_TOPIC = "import_customers";
    private static final String IMPORT_CUSTOMER_ADDRESSES_TOPIC = "import_customer_addresses";
    private static final String IMPORT_ORDERS_TOPIC = "import_orders";
    private static final String BROKERS = "localhost:9092";
    private static Logger logger = LoggerFactory.getLogger(RFMCalc.class);
    private static BigInteger lastRunCustomerId;
    private static Integer lastRunCustomerAddressId;
    private static Integer lastRunOrderId;

    // To run with debug, do: spark-submit --class "RFMCalc" --master local[1] --driver-memory 2g --driver-java-options -agentlib:jdwp=transport=dt_socket,server=y,suspend=y,address=5005 --jars /vagrant/RFMCalc/spark-cassandra-connector_2.10-1.1.1.jar,/vagrant/RFMCalc/spark-cassandra-connector-java_2.10-1.1.1.jar,/vagrant/RFMCalc/cassandra-driver-core-2.1.3.jar,/vagrant/RFMCalc/guava-14.0.1.jar,/vagrant/RFMCalc/cassandra-thrift-2.1.2.jar RFMCalc.jar hdfs://precise64:8020 hdfs://precise64:8020/vagrant/master-data 192.168.50.4 cassandra cassandra hdfs://precise64:8020/vagrant/new-data/customers/* hdfs://precise64:8020/vagrant/new-data/customer_addresses/* hdfs://precise64:8020/vagrant/new-data/orders/* hdfs://precise64:8020/vagrant/RFMBatchResults/ hdfs://precise64:8020/vagrant/RFMSummaryStatistics/
    // spark-submit --class "RFMCalc" --master local[1] --driver-memory 2g --driver-java-options -agentlib:jdwp=transport=dt_socket,server=y,suspend=y,address=5005 --jars /vagrant/RFMCalc/spark-cassandra-connector_2.10-1.6.0-M1.jar,/vagrant/RFMCalc/spark-cassandra-connector-java_2.10-1.6.0-M1.jar,/vagrant/RFMCalc/cassandra-driver-core-3.0.0.jar,/vagrant/RFMCalc/guava-16.0.1.jar,/vagrant/RFMCalc/cassandra-thrift-2.1.2.jar,/vagrant/RFMCalc/spark-streaming-twitter_2.10-1.6.0.jar,/vagrant/RFMCalc/jsr166e-1.1.0.jar RFMCalc.jar hdfs://precise64:8020 hdfs://precise64:8020/vagrant/master-data 192.168.50.4 cassandra cassandra hdfs://precise64:8020/vagrant/new-data/customers/* hdfs://precise64:8020/vagrant/new-data/customer_addresses/* hdfs://precise64:8020/vagrant/new-data/orders/* hdfs://precise64:8020/vagrant/RFMBatchResults/ hdfs://precise64:8020/vagrant/RFMSummaryStatistics/
    // To run without debug, do: spark-submit --class "RFMCalc" --master local[*] --driver-memory 2g --jars /vagrant/RFMCalc/spark-cassandra-connector_2.10-1.1.1.jar,/vagrant/RFMCalc/spark-cassandra-connector-java_2.10-1.1.1.jar,/vagrant/RFMCalc/cassandra-driver-core-2.1.3.jar,/vagrant/RFMCalc/guava-14.0.1.jar,/vagrant/RFMCalc/cassandra-thrift-2.1.2.jar RFMCalc.jar hdfs://precise64:8020 hdfs://precise64:8020/vagrant/master-data localhost hdfs://precise64:8020/vagrant/new-data/customers/* hdfs://precise64:8020/vagrant/new-data/customer_addresses/* hdfs://precise64:8020/vagrant/new-data/orders/* hdfs://precise64:8020/vagrant/RFMBatchResults/ hdfs://precise64:8020/vagrant/RFMSummaryStatistics/
    public static void main(String[] args) throws IOException {
        String hdfsPath = args[0];
        String masterDataPath = args[1];
        String cassandraHostname = args[2];
        String cassandraUsername = args[3];
        String cassandraPassword = args[4];
        String customersPath = args[5];
        String customerAddressesPath = args[6];
        String ordersPath = args[7];
        String rfmBatchResultsPath = args[8];
        String summaryStatisticsResultsPath = args[9];
        String zookeeperUrl = args[10];
        String schemaRegistryUrl = args[11];
        String combinedArguments = Arrays.toString(args);
        Boolean isStreaming = false;

        if (combinedArguments.contains("--speed")) {
            isStreaming = true;
        }

        logger.info("Starting RFMCalc.");
        logger.info("hdfsPath:" + hdfsPath);
        logger.info("masterDataPath:" + masterDataPath);
        logger.info("cassandraHostname:" + cassandraHostname);
        logger.info("cassandraUsername:" + cassandraUsername);
        logger.info("cassandraPassword:" + cassandraPassword);
        logger.info("customersPath:" + customersPath);
        logger.info("customerAddressesPath:" + customerAddressesPath);
        logger.info("ordersPath:" + ordersPath);
        logger.info("rfmBatchResultsPath:" + rfmBatchResultsPath);
        logger.info("summaryStatisticsResultsPath:" + summaryStatisticsResultsPath);
        logger.info("zookeeperUrl:" + zookeeperUrl);
        logger.info("schemaRegistryUrl:" + schemaRegistryUrl);
        logger.info("isStreaming:" + isStreaming);

        // Create Spark configuration
        SparkConf conf = new SparkConf(true).setAppName(RFMCalc.class.getSimpleName())
                .set("spark.cassandra.connection.host", cassandraHostname)
                .set("spark.cassandra.auth.username", cassandraUsername)
                .set("spark.cassandra.auth.password", cassandraPassword)
                .set("spark.cassandra.connection.timeout_ms", "300000")     // 5 minutes
                .set("spark.cassandra.connection.keep_alive_ms", "300000")  // 5 minutes
                .set("spark.cassandra.connection.compression", "SNAPPY");
        //.set("spark.dynamicAllocation.enabled", "true")             // Enable dynamic resource allocation, See: http://spark.apache.org/docs/1.6.1/job-scheduling.html#configuration-and-setup
        //.set("spark.shuffle.service.enabled", "true");              // Required for dynamic resource allocation

        // Create Spark context
        JavaSparkContext sc = new JavaSparkContext(conf);

        // Attach hook to shutdown the SparkContext when the program exits
        AddShutdownHook shutdownHook = new AddShutdownHook(sc);
        shutdownHook.attachShutDownHook();

        if (isStreaming) {
            processSpeedLayer(sc, zookeeperUrl, schemaRegistryUrl);
        } else {
            processBatchLayer(sc, hdfsPath, masterDataPath, customersPath, customerAddressesPath, ordersPath,
                              rfmBatchResultsPath, summaryStatisticsResultsPath);

            // Trigger shutdown hook when the batch job finishes running
            System.exit(0);
        }
    }

    private static void processSpeedLayer(JavaSparkContext sc, String zooKeeperUrl, String schemaRegistryUrl) {
        Map<String, Integer> importCustomersTopicMap = new HashMap<String, Integer>();
        Map<String, Integer> importCustomerAddressesTopicMap = new HashMap<String, Integer>();
        Map<String, Integer> importOrdersTopicMap = new HashMap<String, Integer>();

        Set<String> importCustomersTopicsSet = new HashSet(Arrays.asList(IMPORT_CUSTOMERS_TOPIC));
        Set<String> importCustomerAddressesTopicSet =
                new HashSet(Arrays.asList(IMPORT_CUSTOMER_ADDRESSES_TOPIC));
        Set<String> importOrdersTopicSet = new HashSet(Arrays.asList(IMPORT_ORDERS_TOPIC));

        Object[] importCustomersTopicsArray = importCustomersTopicsSet.toArray();
        Object[] importCustomerAddressesTopicsArray = importCustomerAddressesTopicSet.toArray();
        Object[] importOrdersTopicsArray = importOrdersTopicSet.toArray();

        for (String topic : importCustomersTopicsSet) {
            importCustomersTopicMap.put(topic, new Integer(1));
        }

        for (String topic : importCustomerAddressesTopicSet) {
            importCustomerAddressesTopicMap.put(topic, new Integer(1));
        }

        for (String topic : importOrdersTopicSet) {
            importOrdersTopicMap.put(topic, new Integer(1));
        }

        Properties props = new Properties();
        props.put("zookeeper.connect", zooKeeperUrl);
        props.put("group.id", "rfm_group");
        props.put("metadata.broker.list", BROKERS);
        props.put("schema.registry.url", schemaRegistryUrl);

        VerifiableProperties vProps = new VerifiableProperties(props);
        KafkaAvroDecoder keyDecoder = new KafkaAvroDecoder(vProps);
        KafkaAvroDecoder valueDecoder = new KafkaAvroDecoder(vProps);

        ConsumerConnector customerConsumer = Consumer.createJavaConsumerConnector(new ConsumerConfig(props));
        Map<String, List<KafkaStream<Object, Object>>> customerConsumerMap =
                customerConsumer.createMessageStreams(importCustomersTopicMap, keyDecoder, valueDecoder);

        ConsumerConnector customerAddressConsumer = Consumer.createJavaConsumerConnector(new ConsumerConfig(props));
        Map<String, List<KafkaStream<Object, Object>>> customerAddressConsumerMap =
                customerAddressConsumer.createMessageStreams(importCustomerAddressesTopicMap, keyDecoder, valueDecoder);

        ConsumerConnector orderConsumer = Consumer.createJavaConsumerConnector(new ConsumerConfig(props));
        Map<String, List<KafkaStream<Object, Object>>> orderConsumerMap =
                orderConsumer.createMessageStreams(importOrdersTopicMap, keyDecoder, valueDecoder);

        List<KafkaStream<Object, Object>> customerKafkaStreams = customerConsumerMap.get(importCustomersTopicsArray[0]);
        List<KafkaStream<Object, Object>> customerAddressesKafkaStreams =
                customerAddressConsumerMap.get(importCustomerAddressesTopicsArray[0]);
        List<KafkaStream<Object, Object>> ordersKafkaStreams = orderConsumerMap.get(importOrdersTopicsArray[0]);

        ExecutorService customerExecutorService = Executors.newFixedThreadPool(1);
        ExecutorService customerAddressesExecutorService = Executors.newFixedThreadPool(1);
        ExecutorService ordersExecutorService = Executors.newFixedThreadPool(1);
        ExecutorService rfmExecutorService = Executors.newFixedThreadPool(1);

        // Start child threads
        customerExecutorService.submit(new CustomerConsumer(customerKafkaStreams.get(0), sc, logger,
                                                            CASSANDRA_SCHEMA_NAME, CASSANDRA_RFM_CUSTOMERS_TABLE));
        customerAddressesExecutorService.submit(new CustomerAddressConsumer(customerAddressesKafkaStreams.get(0), sc,
                                                                            logger,
                                                                            CASSANDRA_SCHEMA_NAME,
                                                                            CASSANDRA_RFM_CUSTOMER_ADDRESSES_TABLE));
        ordersExecutorService.submit(new OrderConsumer(ordersKafkaStreams.get(0), sc, logger, CASSANDRA_SCHEMA_NAME,
                                                       CASSANDRA_RFM_ORDERS_TABLE));
        rfmExecutorService.submit(new RFMConsumer(sc, CASSANDRA_SCHEMA_NAME, CASSANDRA_RFM_LAST_RUN_TABLE_NAME,
                                                  CASSANDRA_RFM_CUSTOMERS_TABLE, CASSANDRA_RFM_CUSTOMER_ADDRESSES_TABLE,
                                                  CASSANDRA_RFM_ORDERS_TABLE, CASSANDRA_RFM_SPEED_RESULTS_TABLE_NAME,
                                                  CASSANDRA_SUMMARY_STATISTICS_SPEED_TABLE_NAME,
                                                  CASSANDRA_TOP_1_PERCENT_SUMMARY_STATISTICS_SPEED_TABLE_NAME,
                                                  CASSANDRA_TOP_5_PERCENT_SUMMARY_STATISTICS_SPEED_TABLE_NAME,
                                                  CASSANDRA_TOP_15_PERCENT_SUMMARY_STATISTICS_SPEED_TABLE_NAME,
                                                  CASSANDRA_TOP_25_PERCENT_SUMMARY_STATISTICS_SPEED_TABLE_NAME,
                                                  CASSANDRA_TOP_50_PERCENT_SUMMARY_STATISTICS_SPEED_TABLE_NAME,
                                                  CASSANDRA_BOTTOM_50_PERCENT_SUMMARY_STATISTICS_SPEED_TABLE_NAME));
    }

    private static void processBatchLayer(JavaSparkContext sc, String hdfsPath, String masterDataPath,
                                          String customersPath, String customerAddressesPath, String ordersPath,
                                          String rfmBatchResultsPath, String summaryStatisticsResultsPath) throws
            IOException {
        // Partition new data from the database into the master data set
        ingestNewData(hdfsPath, masterDataPath, customersPath, customerAddressesPath, ordersPath, sc);

        ArrayList<Integer> applicationIds = Hadoop.GetApplicationIds(masterDataPath, hdfsPath, logger);

        // Loop through each application and run RFM calculations on it based on the master data sets customers, addresses and orders for that application.
        if (applicationIds != null && applicationIds.size() > 0) {
            logger.info(String.format("Starting RFM data calculation for %s applications.", applicationIds.size()));

            // Set hadoop to read input directory recursively so we get all customers, addresses, and orders for the entire master data set for the given application.
            sc.hadoopConfiguration().set("mapreduce.input.fileinputformat.input.dir.recursive", "true");

            Cassandra.initializeBatchLayerTables(sc, CASSANDRA_SCHEMA_NAME, CASSANDRA_RFM_RESULTS_TABLE_NAME,
                                                 CASSANDRA_SUMMARY_STATISTICS_TABLE_NAME,
                                                 CASSANDRA_TOP_1_PERCENT_SUMMARY_STATISTICS_TABLE_NAME,
                                                 CASSANDRA_TOP_5_PERCENT_SUMMARY_STATISTICS_TABLE_NAME,
                                                 CASSANDRA_TOP_15_PERCENT_SUMMARY_STATISTICS_TABLE_NAME,
                                                 CASSANDRA_TOP_25_PERCENT_SUMMARY_STATISTICS_TABLE_NAME,
                                                 CASSANDRA_TOP_50_PERCENT_SUMMARY_STATISTICS_TABLE_NAME,
                                                 CASSANDRA_BOTTOM_50_PERCENT_SUMMARY_STATISTICS_TABLE_NAME,
                                                 CASSANDRA_RFM_LAST_RUN_TABLE_NAME);

            // Loop through each application and calculate its RFM data and save it.
            for (int i = 0; i < applicationIds.size(); i++) {
                int applicationId = applicationIds.get(i);
                String customersMasterDataPath = String.format("%s/customers/%s/*", masterDataPath, applicationId);
                String customerAddressesMasterDataPath =
                        String.format("%s/customers_addresses/%s/*", masterDataPath, applicationId);
                String ordersMasterDataPath = String.format("%s/orders/%s/*", masterDataPath, applicationId);
                String rfmOutputResultsPath = String.format("%s%s/%s", rfmBatchResultsPath, applicationId,
                                                            CURRENT_DATE_TIME_PATH);
                String summaryStatisticsOutputResultsPath =
                        String.format("%s%s/%s", summaryStatisticsResultsPath, applicationId,
                                      CURRENT_DATE_TIME_PATH);

                // Calculate RFM data for the customer
                JavaRDD<RFMData> customersAndAddressesAndOrdersAggregateRFMDataRdd =
                        calculateAndSaveRFM(sc, applicationId, customersMasterDataPath, customerAddressesMasterDataPath,
                                            ordersMasterDataPath, rfmOutputResultsPath);

                if (customersAndAddressesAndOrdersAggregateRFMDataRdd != null &&
                    !customersAndAddressesAndOrdersAggregateRFMDataRdd.isEmpty()) {
                    customersAndAddressesAndOrdersAggregateRFMDataRdd.cache();

                    // Calculate summary statistics
                    calculateAndSaveSummaryStatistics(sc, applicationId, summaryStatisticsOutputResultsPath,
                                                      customersAndAddressesAndOrdersAggregateRFMDataRdd);

                    // Calculate summary statistics broken out by x percent (e.g. - top 1%, top 5%, top X%)
                    calculateAndSaveEightyTwentySummaryStatistics(sc, summaryStatisticsResultsPath, applicationId,
                                                                  customersAndAddressesAndOrdersAggregateRFMDataRdd);
                }
            }

            Cassandra.cleanupCassandra(sc, CASSANDRA_SCHEMA_NAME, CASSANDRA_RFM_LAST_RUN_TABLE_NAME,
                                       CURRENT_DATE_TIME_PATH,
                                       lastRunCustomerId, lastRunCustomerAddressId, lastRunOrderId);

            cleanupSpeedLayerTables(sc);

            logger.info("RFM data calculation complete.");
        }

        logger.info("RFMCalc complete.");
    }

    private static void cleanupSpeedLayerTables(JavaSparkContext sc) {
        List<String> speedLayerTableNames = new ArrayList<String>();

        speedLayerTableNames.add(CASSANDRA_RFM_SPEED_RESULTS_TABLE_NAME);
        speedLayerTableNames.add(CASSANDRA_SUMMARY_STATISTICS_SPEED_TABLE_NAME);
        speedLayerTableNames.add(CASSANDRA_TOP_1_PERCENT_SUMMARY_STATISTICS_SPEED_TABLE_NAME);
        speedLayerTableNames.add(CASSANDRA_TOP_5_PERCENT_SUMMARY_STATISTICS_SPEED_TABLE_NAME);
        speedLayerTableNames.add(CASSANDRA_TOP_15_PERCENT_SUMMARY_STATISTICS_SPEED_TABLE_NAME);
        speedLayerTableNames.add(CASSANDRA_TOP_25_PERCENT_SUMMARY_STATISTICS_SPEED_TABLE_NAME);
        speedLayerTableNames.add(CASSANDRA_TOP_50_PERCENT_SUMMARY_STATISTICS_SPEED_TABLE_NAME);
        speedLayerTableNames.add(CASSANDRA_BOTTOM_50_PERCENT_SUMMARY_STATISTICS_SPEED_TABLE_NAME);

        Cassandra.truncateSpeedLayerTables(sc, CASSANDRA_SCHEMA_NAME, speedLayerTableNames);
    }

    private static void calculateAndSaveEightyTwentySummaryStatistics(JavaSparkContext sc,
                                                                      String summaryStatisticsResultsPath,
                                                                      int applicationId,
                                                                      JavaRDD<RFMData> customersAndAddressesAndOrdersAggregateRFMDataRdd) {
        List<JavaRDD<RFMSummaryStatistics>> eightyTwentySummaryStatisticsRdds = RFMSparkTransformations
                .calculateEightyTwentySummaryStatistics(sc, applicationId,
                                                        customersAndAddressesAndOrdersAggregateRFMDataRdd);

        saveSummaryStatisticsTopXPercent(summaryStatisticsResultsPath, sc, CURRENT_DATE_TIME_PATH, applicationId,
                                         EightyTwentySummaryStatisticsPercentages.OnePercent,
                                         CASSANDRA_TOP_1_PERCENT_SUMMARY_STATISTICS_TABLE_NAME,
                                         eightyTwentySummaryStatisticsRdds.get(0));
        saveSummaryStatisticsTopXPercent(summaryStatisticsResultsPath, sc, CURRENT_DATE_TIME_PATH, applicationId,
                                         EightyTwentySummaryStatisticsPercentages.FivePercent,
                                         CASSANDRA_TOP_5_PERCENT_SUMMARY_STATISTICS_TABLE_NAME,
                                         eightyTwentySummaryStatisticsRdds.get(1));
        saveSummaryStatisticsTopXPercent(summaryStatisticsResultsPath, sc, CURRENT_DATE_TIME_PATH, applicationId,
                                         EightyTwentySummaryStatisticsPercentages.FifteenPercent,
                                         CASSANDRA_TOP_15_PERCENT_SUMMARY_STATISTICS_TABLE_NAME,
                                         eightyTwentySummaryStatisticsRdds.get(2));
        saveSummaryStatisticsTopXPercent(summaryStatisticsResultsPath, sc, CURRENT_DATE_TIME_PATH, applicationId,
                                         EightyTwentySummaryStatisticsPercentages.TwentyFivePercent,
                                         CASSANDRA_TOP_25_PERCENT_SUMMARY_STATISTICS_TABLE_NAME,
                                         eightyTwentySummaryStatisticsRdds.get(3));
        saveSummaryStatisticsTopXPercent(summaryStatisticsResultsPath, sc, CURRENT_DATE_TIME_PATH, applicationId,
                                         EightyTwentySummaryStatisticsPercentages.FiftyPercent,
                                         CASSANDRA_TOP_50_PERCENT_SUMMARY_STATISTICS_TABLE_NAME,
                                         eightyTwentySummaryStatisticsRdds.get(4));
        saveSummaryStatisticsBottomXPercent(summaryStatisticsResultsPath, sc, CURRENT_DATE_TIME_PATH, applicationId,
                                            EightyTwentySummaryStatisticsPercentages.BottomFiftyPercent,
                                            CASSANDRA_BOTTOM_50_PERCENT_SUMMARY_STATISTICS_TABLE_NAME,
                                            eightyTwentySummaryStatisticsRdds.get(5));
    }

    private static void calculateAndSaveSummaryStatistics(JavaSparkContext sc, int applicationId,
                                                          String summaryStatisticsOutputResultsPath,
                                                          JavaRDD<RFMData> customersAndAddressesAndOrdersAggregateRFMDataRdd) {
        // Calculate the summary statistics based on the customer's RFM data
        JavaRDD<RFMSummaryStatistics> rfmSummaryStatisticsRDD = RFMSparkTransformations
                .calculateSummaryStatistics(sc, applicationId, customersAndAddressesAndOrdersAggregateRFMDataRdd,
                                            customersAndAddressesAndOrdersAggregateRFMDataRdd);

        // Save summary statistics to Hadoop and Cassandra
        saveApplicationSummaryStatistics(summaryStatisticsOutputResultsPath, rfmSummaryStatisticsRDD,
                                         CASSANDRA_SUMMARY_STATISTICS_TABLE_NAME, sc, applicationId);
    }

    private static void saveSummaryStatisticsTopXPercent(String summaryStatisticsResultsPath, JavaSparkContext sc,
                                                         String currentDateTimePath, int applicationId, Double xPercent,
                                                         String cassandraTableName,
                                                         JavaRDD<RFMSummaryStatistics> topXPercentRFMSummaryStatisticsJavaRDD) {
        // Save summary statistics to Hadoop and Cassandra
        String[] summaryStatisticsResultsPathArray = summaryStatisticsResultsPath.split("/");
        String summaryStatisticsFolderName =
                summaryStatisticsResultsPathArray[summaryStatisticsResultsPathArray.length - 1];
        Integer xPercentTimes100 = ((Double) (xPercent * 100)).intValue();
        String topXPercentSummaryStatisticsFolderName =
                String.format("%s_top_%s_percent", summaryStatisticsFolderName, xPercentTimes100);
        String topXPercentSummaryStatisticsResultsPath = summaryStatisticsResultsPath
                .replaceFirst(summaryStatisticsFolderName, topXPercentSummaryStatisticsFolderName);
        String topXPercentSummaryStatisticsOutputResultsPath =
                String.format("%s%s/%s", topXPercentSummaryStatisticsResultsPath, applicationId, currentDateTimePath);

        saveApplicationSummaryStatistics(topXPercentSummaryStatisticsOutputResultsPath,
                                         topXPercentRFMSummaryStatisticsJavaRDD, cassandraTableName, sc, applicationId);
    }

    private static void saveSummaryStatisticsBottomXPercent(String summaryStatisticsResultsPath, JavaSparkContext sc,
                                                            String currentDateTimePath, int applicationId,
                                                            Double xPercent, String cassandraTableName,
                                                            JavaRDD<RFMSummaryStatistics> bottomXPercentRFMSummaryStatisticsJavaRDD) {
        // Save summary statistics to Hadoop and Cassandra
        String[] summaryStatisticsResultsPathArray = summaryStatisticsResultsPath.split("/");
        String summaryStatisticsFolderName =
                summaryStatisticsResultsPathArray[summaryStatisticsResultsPathArray.length - 1];
        Integer xPercentTimes100 = ((Double) (xPercent * 100)).intValue();
        String bottomXPercentSummaryStatisticsFolderName =
                String.format("%s_bottom_%s_percent", summaryStatisticsFolderName, xPercentTimes100);
        String bottomXPercentSummaryStatisticsResultsPath = summaryStatisticsResultsPath
                .replaceFirst(summaryStatisticsFolderName, bottomXPercentSummaryStatisticsFolderName);
        String bottomXPercentSummaryStatisticsOutputResultsPath =
                String.format("%s%s/%s", bottomXPercentSummaryStatisticsResultsPath, applicationId,
                              currentDateTimePath);

        saveApplicationSummaryStatistics(bottomXPercentSummaryStatisticsOutputResultsPath,
                                         bottomXPercentRFMSummaryStatisticsJavaRDD, cassandraTableName, sc,
                                         applicationId);
    }

    private static void saveApplicationSummaryStatistics(String summaryStatisticsOutputResultsPath,
                                                         JavaRDD<RFMSummaryStatistics> rfmSummaryStatisticsRDD,
                                                         String cassandraTableName, JavaSparkContext sc,
                                                         Integer applicationId) {
        // Save to Hadoop
        rfmSummaryStatisticsRDD.saveAsTextFile(summaryStatisticsOutputResultsPath);

        // Save to Cassandra
        javaFunctions(rfmSummaryStatisticsRDD)
                .writerBuilder(CASSANDRA_SCHEMA_NAME, cassandraTableName, new RFMSummaryStatisticsWriterFactory())
                .saveToCassandra();
    }

    private static void ingestNewData(String hdfsPath, String masterDataPath, String customersPath,
                                      String customerAddressesPath, String ordersPath, JavaSparkContext sc) throws
            IOException {
        JavaRDD<Customer> customers = null;
        JavaRDD<CustomerAddress> customerAddresses = null;
        JavaRDD<Order> orders = null;

        // If there is new data for customers, then parse that data, partition it by application id into the master data set, then delete the new data
        if (Hadoop.DoesPathExist(customersPath, hdfsPath, logger)) {
            customers = Hadoop.ParseCustomerData(customersPath, sc, logger);
            RFMSparkTransformations
                    .partitionCustomersByApplication(sc, customers, CURRENT_DATE_TIME_PATH, masterDataPath);
            Hadoop.DeleteNewData(customersPath, masterDataPath);
        }

        // If there is new data for customer addresses, then parse that data, partition it by application id into the master data set, then delete the new data
        if (Hadoop.DoesPathExist(customerAddressesPath, hdfsPath, logger)) {
            customerAddresses = Hadoop.ParseCustomerAddresses(customerAddressesPath, sc, logger);
            RFMSparkTransformations.partitionCustomerAddressesByApplication(sc, customerAddresses,
                                                                            CURRENT_DATE_TIME_PATH,
                                                                            masterDataPath);
            Hadoop.DeleteNewData(customerAddressesPath, masterDataPath);
        }

        // If there is new data for orders, then parse that data, partition it by application id into the master data set, then delete the new data
        if (Hadoop.DoesPathExist(ordersPath, hdfsPath, logger)) {
            orders = Hadoop.ParseOrders(ordersPath, sc, logger);
            RFMSparkTransformations.partitionOrdersByApplication(sc, orders, CURRENT_DATE_TIME_PATH, masterDataPath);
            Hadoop.DeleteNewData(ordersPath, masterDataPath);
        }
    }

    private static JavaRDD<RFMData> calculateAndSaveRFM(JavaSparkContext sc, int applicationId,
                                                        String customersMasterDataPath,
                                                        String customerAddressesMasterDataPath,
                                                        String ordersMasterDataPath, String rfmOutputResultsPath) {
        // Load up customers, customer addresses and orders as Spark RDD's
        JavaRDD<Customer> customers = Hadoop.ParseCustomerData(customersMasterDataPath, sc, logger);
        JavaRDD<CustomerAddress> customerAddresses = Hadoop.ParseCustomerAddresses(customerAddressesMasterDataPath, sc,
                                                                                   logger);
        JavaRDD<Order> orders = Hadoop.ParseOrders(ordersMasterDataPath, sc, logger);


        // Set the last run customer id, customer address id, and order id to be used by the speed layer
        setLastRunCustomerId(customers);
        setLastRunCustomerAddressId(customerAddresses);
        setLastRunOrderId(orders);

        logger.info("Last run customer id: {}, application id: {}", lastRunCustomerId, applicationId);
        logger.info("Last run customer address id: {}, application id: {}", lastRunCustomerAddressId, applicationId);
        logger.info("Last run order id: {}, application id: {}", lastRunOrderId, applicationId);

        // Get the RFM data JavaRdd
        JavaRDD<RFMData> customersAndAddressesAndOrdersAggregateRFMDataRdd =
                RFMSparkTransformations.getRfmDataJavaRDD(applicationId, customers, customerAddresses, orders);

        if (customersAndAddressesAndOrdersAggregateRFMDataRdd != null &&
            !customersAndAddressesAndOrdersAggregateRFMDataRdd.isEmpty()) {
            // Save the RFM data
            saveRFMData(rfmOutputResultsPath, customersAndAddressesAndOrdersAggregateRFMDataRdd);
        }

        return customersAndAddressesAndOrdersAggregateRFMDataRdd;
    }

    private static void setLastRunCustomerId(JavaRDD<Customer> customers) {
        if (customers != null && customers.count() > 0) {
            JavaRDD<Customer> customersSortedJavaRDD = customers.sortBy(new Function<Customer, BigInteger>() {
                @Override
                public BigInteger call(Customer customer) throws Exception {
                    if (customer != null && customer.getId() != null) {
                        return customer.getId();
                    } else {
                        return BigInteger.valueOf(-1L);
                    }
                }
            }, false, 1);

            // Get last value (descending order)
            Customer lastCustomer = customersSortedJavaRDD.first();

            if (lastCustomer != null) {
                BigInteger lastCustomerId = lastCustomer.getId();

                if (lastRunCustomerId == null) {
                    lastRunCustomerId = lastCustomerId;
                } else if (lastCustomerId != null && lastCustomerId.compareTo(lastRunCustomerId) > 0) {
                    lastRunCustomerId = lastCustomerId;
                }
            }
        }
    }

    private static void setLastRunCustomerAddressId(JavaRDD<CustomerAddress> customerAddresses) {
        if (customerAddresses != null && customerAddresses.count() > 0) {
            JavaRDD<CustomerAddress> customerAddressSortedJavaRDD =
                    customerAddresses.sortBy(new Function<CustomerAddress, Integer>() {
                        @Override
                        public Integer call(CustomerAddress customerAddress) throws Exception {
                            if (customerAddress != null) {
                                return customerAddress.getId();
                            } else {
                                return -1;
                            }
                        }
                    }, false, 1);

            // Get last value (descending order)
            CustomerAddress lastCustomerAddress = customerAddressSortedJavaRDD.first();

            if (lastCustomerAddress != null) {
                Integer lastCustomerAddressId = lastCustomerAddress.getId();

                if (lastRunCustomerAddressId == null) {
                    lastRunCustomerAddressId = lastCustomerAddressId;
                } else if (lastCustomerAddressId != null && lastCustomerAddressId > lastRunCustomerAddressId) {
                    lastRunCustomerAddressId = lastCustomerAddressId;
                }
            }
        }
    }

    private static void setLastRunOrderId(JavaRDD<Order> orders) {
        if (orders != null && orders.count() > 0) {
            JavaRDD<Order> ordersSortedJavaRDD = orders.sortBy(new Function<Order, Integer>() {
                @Override
                public Integer call(Order order) throws Exception {
                    if (order != null) {
                        return order.getId();
                    } else {
                        return -1;
                    }
                }
            }, false, 1);

            // Get last value (descending order)
            Order lastOrder = ordersSortedJavaRDD.first();

            if (lastOrder != null) {
                Integer lastOrderId = lastOrder.getId();

                if (lastRunOrderId == null) {
                    lastRunOrderId = lastOrderId;
                } else if (lastOrderId != null && lastOrderId > lastRunOrderId) {
                    lastRunOrderId = lastOrderId;
                }
            }
        }
    }

    private static void saveRFMData(String rfmOutputResultsPath,
                                    JavaRDD<RFMData> customersAndAddressesAndOrdersAggregateRFMDataRdd) {
        // Save output data to rfmBatchResultsPath for this application's RFM data.
        customersAndAddressesAndOrdersAggregateRFMDataRdd.saveAsTextFile(rfmOutputResultsPath);

        // Save the customers' RFM data to Cassandra
        javaFunctions(customersAndAddressesAndOrdersAggregateRFMDataRdd).writerBuilder(CASSANDRA_SCHEMA_NAME,
                                                                                       CASSANDRA_RFM_RESULTS_TABLE_NAME,
                                                                                       new RFMDataRowWriterFactory())
                .saveToCassandra();
    }
}
