import com.clearspring.analytics.util.Lists;
import com.google.common.base.Optional;
import org.apache.spark.api.java.JavaDoubleRDD;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.DoubleFunction;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import scala.Tuple11;
import scala.Tuple2;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by brettcooper on 7/27/16.
 */
public class RFMSparkTransformations {
    private static Logger logger = LoggerFactory.getLogger(RFMSparkTransformations.class);

    public static int partitionCustomerAddressesByApplication(JavaSparkContext sc,
                                                              JavaRDD<CustomerAddress> customerAddressesDataCsv,
                                                              String currentDateTime, String masterDataPath) {
        JavaPairRDD<String, String> customersPairByApplicationIdRdd =
                getCustomerAddressesPairRdd(customerAddressesDataCsv);

        JavaPairRDD<String, Iterable<String>> customersPairGroupedByApplicationIdRdd =
                customersPairByApplicationIdRdd.groupByKey();

        List<String> keys = customersPairGroupedByApplicationIdRdd.keys().collect();
        List<Iterable<String>> values = customersPairGroupedByApplicationIdRdd.values().collect();

        for (int i = 0; i < keys.size(); i++) {
            String applicationIdKey = keys.get(i);

            if (applicationIdKey != null) {
                List<String> customerAddressesByAppValuesList = Lists.newArrayList(values.get(i));
                JavaRDD<String> customerAddressesByAppValuesRdd = sc.parallelize(customerAddressesByAppValuesList);

                String customerAddressesPath =
                        String.format("%s/customers_addresses/%s/%s/", masterDataPath, applicationIdKey.toString(),
                                      currentDateTime);
                customerAddressesByAppValuesRdd.saveAsTextFile(customerAddressesPath);
            }
        }

        return keys.size();
    }

    public static JavaPairRDD<String, String> getCustomerAddressesPairRdd(
            JavaRDD<CustomerAddress> customerAddressesDataCsv) {
        // Map customer addresses to a JavaPairRDD of (application_id => Customer Address)
        return customerAddressesDataCsv.mapToPair((PairFunction<CustomerAddress, String, String>) customerAddress -> {
            if (customerAddress != null && customerAddress.getApplicationId() != null) {
                return new Tuple2<>(customerAddress.getApplicationId().toString(),
                                    customerAddress.toString());
            } else {
                return new Tuple2<>(null, null);
            }
        });
    }

    public static int partitionCustomersByApplication(JavaSparkContext sc, JavaRDD<Customer> customersDataCsv,
                                                      String currentDateTime, String masterDataPath) {
        JavaPairRDD<String, String> customersPairByApplicationIdRdd = getCustomerPairsRdd(customersDataCsv);
        JavaPairRDD<String, Iterable<String>> customersPairGroupedByApplicationIdRdd =
                customersPairByApplicationIdRdd.groupByKey();

        List<String> keys = customersPairGroupedByApplicationIdRdd.keys().collect();
        List<Iterable<String>> values = customersPairGroupedByApplicationIdRdd.values().collect();

        for (int i = 0; i < keys.size(); i++) {
            String applicationIdKey = keys.get(i);

            if (applicationIdKey != null) {
                List<String> customersByAppValuesList = Lists.newArrayList(values.get(i));
                JavaRDD<String> customersByAppValuesRdd = sc.parallelize(customersByAppValuesList);

                String customersPath = String.format("%s/customers/%s/%s/", masterDataPath, applicationIdKey.toString(),
                                                     currentDateTime);

                customersByAppValuesRdd.saveAsTextFile(customersPath);
            }
        }

        return keys.size();
    }

    public static JavaPairRDD<String, String> getCustomerPairsRdd(JavaRDD<Customer> customersDataCsv) {
        // Map customers to a JavaPairRDD of (application_id => Customer)
        return customersDataCsv.mapToPair((PairFunction<Customer, String, String>) customer -> {
            if (customer != null && customer.getApplicationId() != null) {
                return new Tuple2<>(customer.getApplicationId().toString(), customer.toString());
            } else {
                return new Tuple2<>(null, null);
            }
        });
    }

    public static int partitionOrdersByApplication(JavaSparkContext sc, JavaRDD<Order> orderDataCsv,
                                                   String currentDateTime, String masterDataPath) {
        JavaPairRDD<String, String> ordersPairByApplicationIdRdd = getOrdersPairRdd(orderDataCsv);


        JavaPairRDD<String, Iterable<String>> ordersPairGroupedByApplicationIdRdd =
                ordersPairByApplicationIdRdd.groupByKey();

        List<String> keys = ordersPairGroupedByApplicationIdRdd.keys().collect();
        List<Iterable<String>> values = ordersPairGroupedByApplicationIdRdd.values().collect();

        for (int i = 0; i < keys.size(); i++) {
            String applicationIdKey = keys.get(i);

            if (applicationIdKey != null) {
                List<String> ordersByAppValuesList = Lists.newArrayList(values.get(i));
                JavaRDD<String> ordersByAppValuesRdd = sc.parallelize(ordersByAppValuesList);

                String ordersPath =
                        String.format("%s/orders/%s/%s/", masterDataPath, applicationIdKey.toString(), currentDateTime);

                ordersByAppValuesRdd.saveAsTextFile(ordersPath);
            }
        }

        return keys.size();
    }

    public static JavaPairRDD<String, String> getOrdersPairRdd(JavaRDD<Order> orderDataCsv) {
        // Map orders to a JavaPairRDD of (application_id => Order)
        return orderDataCsv.mapToPair((PairFunction<Order, String, String>) order -> {
            if (order != null && order.getApplicationId() != null) {
                return new Tuple2<>(order.getApplicationId().toString(), order.toString());
            } else {
                return new Tuple2<>(null, null);
            }
        });
    }

    public static JavaRDD<Customer> getUniqueCustomers(JavaRDD<Customer> customersDataCsv) {
        // Map customers to a JavaPairRDD of (customer id => Customer)
        JavaPairRDD<BigInteger, Customer> customersPairRdd =
                customersDataCsv.mapToPair((PairFunction<Customer, BigInteger, Customer>) customer -> {
                    if (customer != null) {
                        return new Tuple2<>(customer.getCustomerId(), customer);
                    } else {
                        return new Tuple2<>(null, null);
                    }
                });

        // Get unique customers (if multiple customers have the same id, take the latest one)
        JavaPairRDD<BigInteger, Customer> uniqueCustomersPairRdd = customersPairRdd.reduceByKey(
                (Function2<Customer, Customer, Customer>) (customer1, customer2) -> {
                    if (customer1 == null) {
                        return customer2;
                    } else if (customer2 == null) {
                        return customer1;
                    } else if (customer1.getCreatedAt() == null) {
                        return customer2;
                    } else if (customer2.getCreatedAt() == null) {
                        return customer1;
                    } else if (customer1.getCreatedAt().compareTo(customer2.getCreatedAt()) > 0) {
                        return customer1;
                    } else {
                        return customer2;
                    }
                });

        JavaRDD<Customer> customers = uniqueCustomersPairRdd
                .map((Function<Tuple2<BigInteger, Customer>, Customer>) customerPair -> customerPair._2());

        return customers;
    }

    public static JavaRDD<CustomerAddress> getUniqueCustomerAddresses(
            JavaRDD<CustomerAddress> customerAddressesDataCsv) {
        // Map customers to a JavaPairRDD of (customer id => CustomerAddress)
        JavaPairRDD<BigInteger, CustomerAddress> customerAddressesPairRdd =
                customerAddressesDataCsv.mapToPair(new PairFunction<CustomerAddress, BigInteger, CustomerAddress>() {
                    @Override
                    public Tuple2<BigInteger, CustomerAddress> call(CustomerAddress customerAddress) throws Exception {
                        if (customerAddress != null) {
                            return new Tuple2<BigInteger, CustomerAddress>(customerAddress.getCustomerId(),
                                                                           customerAddress);
                        } else {
                            return new Tuple2<BigInteger, CustomerAddress>(null, null);
                        }
                    }
                });


        JavaPairRDD<BigInteger, CustomerAddress> uniqueCustomerAddresses = customerAddressesPairRdd
                .reduceByKey(new Function2<CustomerAddress, CustomerAddress, CustomerAddress>() {
                    @Override
                    public CustomerAddress call(CustomerAddress customerAddress1,
                                                CustomerAddress customerAddress2) throws
                            Exception {
                        if (customerAddress1 == null) {
                            return customerAddress2;
                        } else if (customerAddress2 == null) {
                            return customerAddress1;
                        }
                        if (customerAddress1.getCreated() == null) {
                            return customerAddress2;
                        } else if (customerAddress2.getCreated() == null) {
                            return customerAddress1;
                        } else if (customerAddress1.getCreated().compareTo(customerAddress2.getCreated()) > 0) {
                            return customerAddress1;
                        } else {
                            return customerAddress2;
                        }
                    }
                });

        JavaRDD<CustomerAddress> customerAddresses =
                uniqueCustomerAddresses.map(new Function<Tuple2<BigInteger, CustomerAddress>, CustomerAddress>() {
                    @Override
                    public CustomerAddress call(Tuple2<BigInteger, CustomerAddress> customerAddressPair) throws
                            Exception {
                        return customerAddressPair._2();
                    }
                });

        return customerAddresses;
    }

    public static JavaRDD<Order> getUniqueOrders(JavaRDD<Order> ordersDataCsv) {
        // Map orders to a JavaPairRDD of (order id => Order)
        JavaPairRDD<String, Order> ordersPairRdd =
                ordersDataCsv.mapToPair((PairFunction<Order, String, Order>) order -> {
                    if (order != null) {
                        return new Tuple2<>(order.getOrderId(), order);
                    } else {
                        return new Tuple2<>(null, null);
                    }
                });

        JavaPairRDD<String, Order> uniqueOrders =
                ordersPairRdd.reduceByKey((Function2<Order, Order, Order>) (order1, order2) -> {
                    if (order1 == null) {
                        return order2;
                    } else if (order2 == null) {
                        return order1;
                    } else if (order1.getCreatedAt() == null) {
                        return order2;
                    } else if (order2.getCreatedAt() == null) {
                        return order1;
                    } else if (order1.getCreatedAt().compareTo(order2.getCreatedAt()) > 0) {
                        return order1;
                    } else {
                        return order2;
                    }
                });

        JavaRDD<Order> orders = uniqueOrders.map((Function<Tuple2<String, Order>, Order>) orderPair -> orderPair._2());

        return orders;
    }

    public static JavaRDD<RFMData> getRfmDataJavaRDD(int applicationId, JavaRDD<Customer> customersDataCsv,
                                                     JavaRDD<CustomerAddress> customerAddressesDataCsv,
                                                     JavaRDD<Order> ordersDataCsv) {
        // Make sure the customers, addresses and orders are unique for this application
        JavaRDD<Customer> uniqueCustomers = getUniqueCustomers(customersDataCsv);
        JavaRDD<CustomerAddress> uniqueCustomerAddresses = getUniqueCustomerAddresses(customerAddressesDataCsv);
        JavaRDD<Order> uniqueOrders = getUniqueOrders(ordersDataCsv);

        if (uniqueOrders == null || uniqueOrders.isEmpty()) {
            logger.info("No unique orders found.");
            return null;
        }

        JavaRDD<Order> uniquePaidOrders = getPaidOrders(uniqueOrders);

        if (uniquePaidOrders == null || uniquePaidOrders.isEmpty()) {
            logger.info("No unique paid orders found.");
            return null;
        }

        // Map customers to a JavaPairRDD of (id (customer id) => Customer)
        JavaPairRDD<BigInteger, Customer> customersPairRdd =
                uniqueCustomers.mapToPair((PairFunction<Customer, BigInteger, Customer>) customer -> {
                    if (customer != null) {
                        return new Tuple2<>(customer.getId(), customer);
                    } else {
                        return new Tuple2<>(null, null);
                    }
                });

        // Map customers to a JavaPairRDD of (customer id => CustomerAddress)
        JavaPairRDD<BigInteger, CustomerAddress> customerAddressesPairRdd =
                uniqueCustomerAddresses.mapToPair(
                        (PairFunction<CustomerAddress, BigInteger, CustomerAddress>) customerAddress -> {
                            if (customerAddress != null) {
                                return new Tuple2<>(customerAddress.getCustomerId(),
                                                    customerAddress);
                            } else {
                                return new Tuple2<>(null, null);
                            }
                        });

        // Map orders to a JavaPairRDD of (customer id => Order)
        JavaPairRDD<BigInteger, Order> ordersPairRdd =
                uniquePaidOrders.mapToPair((PairFunction<Order, BigInteger, Order>) order -> {
                    if (order != null) {
                        return new Tuple2<>(order.getCustomerId(), order);
                    } else {
                        return new Tuple2<>(null, null);
                    }
                });

        // Join the customers and their addresses together
        JavaPairRDD<BigInteger, Tuple2<Customer, Optional<CustomerAddress>>> customersAndAddressesRdd =
                customersPairRdd.leftOuterJoin(customerAddressesPairRdd);

        // Change the mapping of customers and addresses to be (customer id => (Customer, Customer Address)) instead of (id (customer_id) => (Customer, Customer Address))
        // This is to get the dataset ready to join with orders because the order's customer id is different from customers.id (use customers.customer_id instead when joining)
        JavaPairRDD<BigInteger, Tuple2<Customer, Optional<CustomerAddress>>> customersAndAddressesByCustomerIdKeyRdd =
                customersAndAddressesRdd.mapToPair(
                        (PairFunction<Tuple2<BigInteger, Tuple2<Customer, Optional<CustomerAddress>>>, BigInteger, Tuple2<Customer, Optional<CustomerAddress>>>) tuple -> new Tuple2<>(
                                tuple._2._1.getCustomerId(),
                                new Tuple2<>(tuple._2._1, tuple._2._2)));

        // Join the customer and their address with the customer's orders
        JavaPairRDD<BigInteger, Tuple2<Tuple2<Customer, Optional<CustomerAddress>>, Order>>
                customersAndAddressesAndOrdersPairRdd = customersAndAddressesByCustomerIdKeyRdd.join(ordersPairRdd);

        // The aggregated null set for use with aggregating by key
        Tuple11<Customer, CustomerAddress, BigDecimal, Integer, Date, Date, Integer, BigDecimal, BigDecimal, BigDecimal, Date>
                zeroValue =
                new Tuple11<Customer, CustomerAddress, BigDecimal, Integer, Date, Date, Integer, BigDecimal, BigDecimal, BigDecimal, Date>(
                        null, null, null,
                        null, null, null, null, null, null, null, null);

        // Transform the tuple of customer and address with the order tuple to return a tuple with customer, customer address, and order values.
        Function2<Tuple11<Customer, CustomerAddress, BigDecimal, Integer, Date, Date, Integer, BigDecimal, BigDecimal, BigDecimal, Date>, Tuple2<Tuple2<Customer, Optional<CustomerAddress>>, Order>,
                Tuple11<Customer, CustomerAddress, BigDecimal, Integer, Date, Date, Integer, BigDecimal, BigDecimal, BigDecimal, Date>>
                seqFunc =
                (Function2<Tuple11<Customer, CustomerAddress, BigDecimal, Integer, Date, Date, Integer, BigDecimal, BigDecimal, BigDecimal, Date>, Tuple2<Tuple2<Customer, Optional<CustomerAddress>>, Order>, Tuple11<Customer, CustomerAddress, BigDecimal, Integer, Date, Date, Integer, BigDecimal, BigDecimal, BigDecimal, Date>>) (tuple1, tuple2) -> {
                    Order order = tuple2._2;
                    Customer customer2 = tuple2._1._1;
                    Date customerCreatedAtDate = customer2.getCreatedAt();
                    CustomerAddress customerAddress2 =
                            (tuple2._1()._2() != null && tuple2._1()._2() != Optional.<CustomerAddress>absent()) ?
                                    tuple2._1()._2().get() : null;
                    Integer averageDaysBetweenOrders = null;
                    Tuple11<Customer, CustomerAddress, BigDecimal, Integer, Date, Date, Integer, BigDecimal, BigDecimal, BigDecimal, Date>
                            latestTuple = null;

                    // Is this the first time seeing this tuple?
                    if (tuple1 == null || tuple1.equals(zeroValue)) {
                        Integer orderCount = 0;

                        if (order != null) {
                            orderCount = 1;
                        }

                        latestTuple =
                                new Tuple11<>(
                                        customer2, customerAddress2, order.getTotalPrice(), orderCount,
                                        order.getCreatedAt(), order.getCreatedAt(), averageDaysBetweenOrders,
                                        order.getTotalPrice(), order.getTotalPrice(), order.getTotalPrice(),
                                        customerCreatedAtDate);
                    } else {
                        // Aggregate new tuple data with existing tuple data
                        Customer customer1 = tuple1._1();
                        CustomerAddress customerAddress1 = tuple1._2();
                        BigDecimal total1 = tuple1._3();
                        BigDecimal total2 = order.getTotalPrice();
                        BigDecimal totalSum = RFMHelpers.GetTotalSum(total1, total2);
                        Customer latestCustomer = RFMHelpers.GetLatestCustomer(customer1, customer2);
                        CustomerAddress latestCustomerAddress =
                                RFMHelpers.GetLatestCustomerAddress(customerAddress1, customerAddress2);
                        Date createdDate1 = tuple1._5();
                        Date lastCreatedDate = tuple1._6();
                        Date createdDate2 = order.getCreatedAt();
                        Date firstOrderDate = RFMHelpers.GetFirstOrderDate(createdDate1, createdDate2);
                        Date lastOrderDate = RFMHelpers.GetLastOrderDate(lastCreatedDate, createdDate2);
                        BigDecimal tuple1OrderAmount = tuple1._8();
                        BigDecimal tuple2OrderAmount = tuple1._9();
                        BigDecimal firstOrderAmount = RFMHelpers
                                .GetFirstOrderAmount(createdDate1, createdDate2, tuple1OrderAmount,
                                                     order.getTotalPrice());
                        BigDecimal lastOrderAmount = RFMHelpers
                                .GetLastOrderAmount(lastCreatedDate, createdDate2, tuple2OrderAmount,
                                                    order.getTotalPrice());
                        Integer tuple1OrderCount = tuple1._4();
                        BigDecimal averageOrderPrice = null;

                        Integer orderCount = tuple1OrderCount + 1;
                        BigDecimal orderCountBigDecimal = new BigDecimal(orderCount);
                        averageOrderPrice = (totalSum != null) ?
                                totalSum.divide(orderCountBigDecimal, 2, RoundingMode.HALF_UP) : null;
                        averageDaysBetweenOrders =
                                RFMHelpers.GetAverageDaysBetweenOrders(firstOrderDate, lastOrderDate, orderCount);

                        latestTuple =
                                new Tuple11<>(
                                        latestCustomer, latestCustomerAddress, totalSum, orderCount, firstOrderDate,
                                        lastOrderDate, averageDaysBetweenOrders, firstOrderAmount,
                                        lastOrderAmount, averageOrderPrice, customerCreatedAtDate);
                    }

                    return latestTuple;
                };

        Function2<Tuple11<Customer, CustomerAddress, BigDecimal, Integer, Date, Date, Integer, BigDecimal, BigDecimal, BigDecimal, Date>, Tuple11<Customer, CustomerAddress, BigDecimal, Integer, Date, Date, Integer, BigDecimal, BigDecimal, BigDecimal, Date>,
                Tuple11<Customer, CustomerAddress, BigDecimal, Integer, Date, Date, Integer, BigDecimal, BigDecimal, BigDecimal, Date>>
                combFunc =
                (Function2<Tuple11<Customer, CustomerAddress, BigDecimal, Integer, Date, Date, Integer, BigDecimal, BigDecimal, BigDecimal, Date>, Tuple11<Customer, CustomerAddress, BigDecimal, Integer, Date, Date, Integer, BigDecimal, BigDecimal, BigDecimal, Date>, Tuple11<Customer, CustomerAddress, BigDecimal, Integer, Date, Date, Integer, BigDecimal, BigDecimal, BigDecimal, Date>>) (tuple1, tuple2) -> {
                    if (tuple1 != null && !tuple1.equals(zeroValue) && tuple2 != null &&
                        !tuple2.equals(zeroValue)) {
                        // Aggregate new tuple data with existing tuple data
                        Customer customer1 = tuple1._1();
                        Customer customer2 = tuple2._1();
                        Customer latestCustomer = RFMHelpers.GetLatestCustomer(customer1, customer2);
                        CustomerAddress customerAddress1 = tuple1._2();
                        CustomerAddress customerAddress2 = tuple2._2();
                        CustomerAddress latestCustomerAddress =
                                RFMHelpers.GetLatestCustomerAddress(customerAddress1, customerAddress2);
                        BigDecimal total1 = tuple1._3();
                        BigDecimal total2 = tuple2._3();
                        BigDecimal totalSum = RFMHelpers.GetTotalSum(total1, total2);
                        Integer tuple1OrderCount = tuple1._4();
                        Integer tuple2OrderCount = tuple2._4();
                        Integer orderCount = tuple1OrderCount + tuple2OrderCount;
                        Date createdDate1 = tuple1._5();
                        Date createdDate2 = tuple2._5();
                        Date firstOrderDate = RFMHelpers.GetFirstOrderDate(createdDate1, createdDate2);
                        Date lastOrderDate = RFMHelpers.GetLastOrderDate(createdDate1, createdDate2);
                        Integer averageDaysBetweenOrders =
                                RFMHelpers.GetAverageDaysBetweenOrders(firstOrderDate, lastOrderDate, orderCount);
                        BigDecimal tuple1OrderAmount = tuple1._8();
                        BigDecimal tuple2OrderAmount = tuple2._8();
                        BigDecimal firstOrderAmount = RFMHelpers
                                .GetFirstOrderAmount(createdDate1, createdDate2, tuple1OrderAmount,
                                                     tuple2OrderAmount);
                        BigDecimal lastOrderAmount = RFMHelpers
                                .GetLastOrderAmount(createdDate1, createdDate2, tuple1OrderAmount,
                                                    tuple2OrderAmount);
                        BigDecimal orderCountBigDecimal = new BigDecimal(orderCount);
                        BigDecimal averageOrderPrice = (totalSum != null) ?
                                totalSum.divide(orderCountBigDecimal, 2, RoundingMode.HALF_UP) : null;
                        Date tuple1CustomerCreatedAtDate = tuple1._11();
                        Date tuple2CustomerCreatedAtDate = tuple2._11();
                        Date customerCreatedAtDate = RFMHelpers
                                .GetFirstOrderDate(tuple1CustomerCreatedAtDate, tuple2CustomerCreatedAtDate);

                        Tuple11<Customer, CustomerAddress, BigDecimal, Integer, Date, Date, Integer, BigDecimal, BigDecimal, BigDecimal, Date>
                                latestTuple =
                                new Tuple11<>(
                                        latestCustomer,
                                        latestCustomerAddress, totalSum, orderCount, firstOrderDate, lastOrderDate,
                                        averageDaysBetweenOrders, firstOrderAmount, lastOrderAmount,
                                        averageOrderPrice, customerCreatedAtDate);

                        return latestTuple;
                    } else if (tuple1 != null && !tuple1.equals(zeroValue)) {
                        return tuple1;
                    } else if (tuple2 != null && !tuple2.equals(zeroValue)) {
                        return tuple2;
                    } else {
                        return zeroValue;
                    }
                };

        /*
            Example customer, address and their orders:

            164218,469,101279962,null,Allyson,Davis,allysuewho@gmail.com,278757,null,Draper,Utah,United States,469,144562992,101279962,14.37,Mon Oct 29 12:34:28 UTC 2012,Mon Oct 29 11:34:28 UTC 2012
            164218,469,101279962,null,Allyson,Davis,allysuewho@gmail.com,278757,null,Draper,Utah,United States,469,152811720,101279962,15.87,Thu Dec 20 10:16:29 UTC 2012,Mon Oct 29 11:34:28 UTC 2012
            164218,469,101279962,null,Allyson,Davis,allysuewho@gmail.com,278757,null,Draper,Utah,United States,469,243108653,101279962,9.56,Tue Mar 11 18:53:26 UTC 2014,Mon Oct 29 11:34:28 UTC 2012
            164218,469,101279962,null,Allyson,Davis,allysuewho@gmail.com,278757,null,Draper,Utah,United States,469,335301569,101279962,35.21,Fri Sep 12 14:12:04 UTC 2014,Mon Oct 29 11:34:28 UTC 2012
            164218,469,101279962,null,Allyson,Davis,allysuewho@gmail.com,278757,null,Draper,Utah,United States,469,351543273,101279962,35.20,Fri Nov 14 10:59:26 UTC 2014,Mon Oct 29 11:34:28 UTC 2012
            164218,469,101279962,null,Allyson,Davis,allysuewho@gmail.com,278757,null,Draper,Utah,United States,469,355221501,101279962,16.59,Wed Nov 26 14:04:15 UTC 2014,Mon Oct 29 11:34:28 UTC 2012
            164218,469,101279962,null,Allyson,Davis,allysuewho@gmail.com,278757,null,Draper,Utah,United States,469,360649665,101279962,25.57,Mon Dec 08 12:35:36 UTC 2014,Mon Oct 29 11:34:28 UTC 2012
            164218,469,101279962,null,Allyson,Davis,allysuewho@gmail.com,278757,null,Draper,Utah,United States,469,1615139009,101279962,26.66,Mon Sep 21 11:31:10 UTC 2015,Mon Oct 29 11:34:28 UTC 2012
            164218,469,101279962,null,Allyson,Davis,allysuewho@gmail.com,278757,null,Draper,Utah,United States,469,1615139009,101279962,26.66,Mon Sep 21 11:31:10 UTC 2015,Mon Oct 29 11:34:28 UTC 2012
            164218,469,101279962,null,Allyson,Davis,allysuewho@gmail.com,278757,null,Draper,Utah,United States,469,1615232001,101279962,23.45,Mon Sep 21 11:42:37 UTC 2015,Mon Oct 29 11:34:28 UTC 2012
        */

        // Aggregate the customer, address, and order by key (customer id)
        // See: https://spark.apache.org/docs/1.4.0/api/java/org/apache/spark/api/java/JavaPairRDD.html#aggregateByKey(U,%20org.apache.spark.api.java.function.Function2,%20org.apache.spark.api.java.function.Function2)
        JavaPairRDD<BigInteger, Tuple11<Customer, CustomerAddress, BigDecimal, Integer, Date, Date, Integer, BigDecimal, BigDecimal, BigDecimal, Date>>
                customersAndAddressesAndOrdersAggregateRdd =
                customersAndAddressesAndOrdersPairRdd.aggregateByKey(zeroValue, seqFunc, combFunc);

        return customersAndAddressesAndOrdersAggregateRdd
                .map((Function<Tuple2<BigInteger, Tuple11<Customer, CustomerAddress, BigDecimal, Integer, Date, Date, Integer, BigDecimal, BigDecimal, BigDecimal, Date>>, RFMData>) tuple -> {
                    Customer customer = tuple._2._1();
                    CustomerAddress customerAddress =
                            (tuple._2._2() != null) ? tuple._2._2() : new CustomerAddress();
                    BigDecimal subTotalSum = tuple._2._3();
                    Date firstOrderDate = tuple._2._5();
                    Date lastOrderDate = tuple._2._6();
                    Integer orderCount = tuple._2._4();
                    Integer averageDaysBetweenOrders = tuple._2._7();
                    BigDecimal firstOrderAmount = tuple._2._8();
                    BigDecimal lastOrderAmount = tuple._2._9();
                    BigDecimal averageOrderPrice = tuple._2._10();
                    Date customerCreatedAt = tuple._2._11();
                    String firstName = customer.getFirstName();
                    String lastName = customer.getLastName();
                    String fullName = RFMHelpers.GetFormattedFullName(firstName, lastName);

                    RFMData rfmData =
                            new RFMData(applicationId, customer.getId(), fullName, customerAddress.getCompany(),
                                        customer.getCustomerGroup(), customerAddress.getCity(),
                                        customerAddress.getProvince(), customerAddress.getCountry(),
                                        customer.getEmail(), subTotalSum, orderCount,
                                        firstOrderDate, lastOrderDate, averageDaysBetweenOrders, firstOrderAmount,
                                        lastOrderAmount, averageOrderPrice, customerCreatedAt);

                    return rfmData;
                });
    }

    public static JavaRDD<Order> getPaidOrders(JavaRDD<Order> orders) {
        if (orders == null || orders.isEmpty()) {
            return null;
        }

        return orders.filter((Function<Order, Boolean>) order -> {
            if (order.getFinancialStatus() != null && !order.getFinancialStatus().isEmpty()) {
                OrderPaidStatus orderPaidStatus = OrderPaidStatus.getOrderPaidStatus(order.getFinancialStatus());

                if (orderPaidStatus != null) {
                    return true;
                }
            }

            return false;
        });
    }

    public static String getCurrentDateTimePath() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
        Date date = new Date();
        String currentDateTime = dateFormat.format(date);

        return currentDateTime;
    }

    public static JavaRDD<RFMSummaryStatistics> calculateSummaryStatistics(JavaSparkContext sc, int applicationId,
                                                                           JavaRDD<RFMData> customersAndAddressesAndOrdersAggregateRFMDataSubsetRdd,
                                                                           JavaRDD<RFMData> customersAndAddressesAndOrdersAggregateRFMDataRdd) {
        // Calculate total customers
        long totalNumberOfCustomersLong = (customersAndAddressesAndOrdersAggregateRFMDataSubsetRdd != null) ?
                customersAndAddressesAndOrdersAggregateRFMDataSubsetRdd.count() : 0;
        BigDecimal totalNumberOfCustomers = new BigDecimal(totalNumberOfCustomersLong);

        JavaDoubleRDD sumOfAllOrdersRdd =
                customersAndAddressesAndOrdersAggregateRFMDataSubsetRdd.mapToDouble(
                        (DoubleFunction<RFMData>) rfmData -> (rfmData != null && rfmData.getOrdersSubTotal() != null) ?
                                rfmData.getOrdersSubTotal().doubleValue() : 0);

        JavaDoubleRDD grandSumOfAllOrdersRdd =
                customersAndAddressesAndOrdersAggregateRFMDataRdd.mapToDouble(
                        (DoubleFunction<RFMData>) rfmData -> (rfmData != null && rfmData.getOrdersSubTotal() != null) ?
                                rfmData.getOrdersSubTotal().doubleValue() : 0);

        JavaDoubleRDD totalOrderCountRdd =
                customersAndAddressesAndOrdersAggregateRFMDataSubsetRdd.mapToDouble(
                        (DoubleFunction<RFMData>) rfmData -> (rfmData != null && rfmData.getOrderCount() != null) ?
                                rfmData.getOrderCount() : 0);

        JavaDoubleRDD grandTotalOrderCountRdd =
                customersAndAddressesAndOrdersAggregateRFMDataRdd.mapToDouble(
                        (DoubleFunction<RFMData>) rfmData -> (rfmData != null && rfmData.getOrderCount() != null) ?
                                rfmData.getOrderCount() : 0);

        JavaRDD<RFMData> customersAndAddressesAndOrdersAggregateRepeatBuyersRFMDataRdd =
                customersAndAddressesAndOrdersAggregateRFMDataSubsetRdd.filter(
                        (Function<RFMData, Boolean>) rfmData -> rfmData != null && rfmData.getOrderCount() != null &&
                                                                rfmData.getOrderCount() > 1);

        JavaDoubleRDD revenueFromRepeatBuyersRdd = customersAndAddressesAndOrdersAggregateRepeatBuyersRFMDataRdd
                .mapToDouble(
                        (DoubleFunction<RFMData>) rfmData -> (rfmData != null && rfmData.getOrdersSubTotal() != null) ?
                                rfmData.getOrdersSubTotal().doubleValue() : 0);

        JavaDoubleRDD averageDaysBetweenOrdersRdd =
                customersAndAddressesAndOrdersAggregateRFMDataSubsetRdd.mapToDouble(
                        (DoubleFunction<RFMData>) rfmData -> {
                            if (rfmData != null && rfmData.getAverageDaysBetweenOrders() != null) {
                                return rfmData.getAverageDaysBetweenOrders();
                            } else {
                                return 0;
                            }
                        });

        BigDecimal zero = new BigDecimal(0);
        BigDecimal zeroTwoDecimalPoints = zero.setScale(2);
        Double totalNumberOfOrdersDouble = (totalOrderCountRdd != null) ? totalOrderCountRdd.sum() : null;
        BigDecimal totalNumberOfOrders = (totalNumberOfOrdersDouble != null && !totalNumberOfOrdersDouble.isNaN()) ?
                new BigDecimal(totalNumberOfOrdersDouble).setScale(0) : null;
        Double grandTotalNumberOfOrdersDouble =
                (grandTotalOrderCountRdd != null) ? grandTotalOrderCountRdd.sum() : null;
        BigDecimal grandTotalNumberOfOrders =
                (grandTotalNumberOfOrdersDouble != null && !grandTotalNumberOfOrdersDouble.isNaN()) ?
                        new BigDecimal(grandTotalNumberOfOrdersDouble).setScale(0) : null;
        BigDecimal percentageTotalNumberOfOrders =
                (totalNumberOfOrders != null && grandTotalNumberOfOrders.compareTo(zeroTwoDecimalPoints) > 0) ?
                        totalNumberOfOrders.divide(grandTotalNumberOfOrders, 2, BigDecimal.ROUND_HALF_UP) : null;
        Double sumOfAllOrdersDouble = (sumOfAllOrdersRdd != null) ? sumOfAllOrdersRdd.sum() : null;
        BigDecimal sumOfAllOrders = (sumOfAllOrdersDouble != null && !sumOfAllOrdersDouble.isNaN()) ?
                new BigDecimal(sumOfAllOrdersDouble).setScale(2, RoundingMode.HALF_UP) : null;
        Double grandSumOfAllOrdersDouble = (grandSumOfAllOrdersRdd != null) ? grandSumOfAllOrdersRdd.sum() : null;
        BigDecimal grandSumOfAllOrders = (grandSumOfAllOrdersDouble != null && !grandSumOfAllOrdersDouble.isNaN()) ?
                new BigDecimal(grandSumOfAllOrdersDouble).setScale(2, RoundingMode.HALF_UP) : null;
        BigDecimal percentageTotalAmountOfOrders = (sumOfAllOrders != null && grandSumOfAllOrders != null &&
                                                    grandSumOfAllOrders.compareTo(zeroTwoDecimalPoints) > 0) ?
                sumOfAllOrders.divide(grandSumOfAllOrders, 2, BigDecimal.ROUND_HALF_UP) : null;
        BigDecimal averageOrderValue = (sumOfAllOrders != null && totalNumberOfOrders != null &&
                                        totalNumberOfOrders.compareTo(zeroTwoDecimalPoints) > 0) ?
                sumOfAllOrders.divide(totalNumberOfOrders, 2, BigDecimal.ROUND_HALF_UP) : null;
        BigDecimal averageCustomerLifetimeValue = (sumOfAllOrders != null && totalNumberOfCustomers != null &&
                                                   totalNumberOfCustomers.compareTo(zeroTwoDecimalPoints) > 0) ?
                sumOfAllOrders.divide(totalNumberOfCustomers, 2, BigDecimal.ROUND_HALF_UP) : null;
        BigDecimal averageNumberOfOrders = (totalNumberOfOrders != null && totalNumberOfCustomers != null &&
                                            totalNumberOfCustomers.compareTo(zeroTwoDecimalPoints) > 0) ?
                totalNumberOfOrders.divide(totalNumberOfCustomers, 2, BigDecimal.ROUND_HALF_UP) : null;
        Double revenueFromRepeatBuyersDouble =
                (revenueFromRepeatBuyersRdd != null) ? revenueFromRepeatBuyersRdd.sum() : null;
        BigDecimal revenueFromRepeatBuyers =
                (revenueFromRepeatBuyersDouble != null && !revenueFromRepeatBuyersDouble.isNaN()) ?
                        new BigDecimal(revenueFromRepeatBuyersDouble).setScale(2, RoundingMode.HALF_UP) : null;
        BigDecimal percentageRevenueFromRepeatBuyers = (revenueFromRepeatBuyers != null && sumOfAllOrders != null &&
                                                        sumOfAllOrders.compareTo(zeroTwoDecimalPoints) > 0) ?
                revenueFromRepeatBuyers.divide(sumOfAllOrders, 2, BigDecimal.ROUND_HALF_UP) : null;
        long numberOfRepeatBuyersLong = customersAndAddressesAndOrdersAggregateRepeatBuyersRFMDataRdd.count();
        BigDecimal numberOfRepeatBuyers = new BigDecimal(numberOfRepeatBuyersLong).setScale(0);
        BigDecimal percentageOfRepeatBuyers = (numberOfRepeatBuyers != null && totalNumberOfCustomers != null &&
                                               totalNumberOfCustomers.compareTo(zeroTwoDecimalPoints) > 0) ?
                numberOfRepeatBuyers.divide(totalNumberOfCustomers, 2, BigDecimal.ROUND_HALF_UP) : null;
        Double averageDaysBetweenOrdersDouble =
                (averageDaysBetweenOrdersRdd != null) ? averageDaysBetweenOrdersRdd.mean() : null;
        BigDecimal averageDaysSinceLastOrder =
                (averageDaysBetweenOrdersDouble != null && !averageDaysBetweenOrdersDouble.isNaN()) ?
                        new BigDecimal(averageDaysBetweenOrdersDouble).setScale(0, RoundingMode.HALF_UP) : null;

        RFMSummaryStatistics rfmSummaryStatistics =
                new RFMSummaryStatistics(applicationId, totalNumberOfCustomers.intValue(), sumOfAllOrders,
                                         percentageTotalAmountOfOrders, averageOrderValue, averageCustomerLifetimeValue,
                                         totalNumberOfOrders.intValue(),
                                         percentageTotalNumberOfOrders, averageNumberOfOrders, percentageOfRepeatBuyers,
                                         percentageRevenueFromRepeatBuyers, averageDaysSinceLastOrder.intValue());

        ArrayList<RFMSummaryStatistics> rfmSummaryStatisticsList = new ArrayList<RFMSummaryStatistics>();
        rfmSummaryStatisticsList.add(rfmSummaryStatistics);

        JavaRDD<RFMSummaryStatistics> rfmSummaryStatisticsRdd = sc.parallelize(rfmSummaryStatisticsList);

        return rfmSummaryStatisticsRdd;
    }

    public static List<JavaRDD<RFMSummaryStatistics>> calculateEightyTwentySummaryStatistics(JavaSparkContext sc,
                                                                                             int applicationId,
                                                                                             JavaRDD<RFMData> customersAndAddressesAndOrdersAggregateRFMDataRdd) {
        Double onePercent = .01;
        Double fivePercent = .05;
        Double fifteenPercent = .15;
        Double twentyFivePercent = .25;
        Double fiftyPercent = .50;
        Double bottomPercent = .50;

        // Calculate 80/20 summary (1%, 5%, 15%, 25%, 50%, Remaining 50%)
        JavaRDD<RFMSummaryStatistics> topOnePercentRFMSummaryStatisticsJavaRDD =
                calculateSummaryStatisticsTopXPercent(sc, applicationId,
                                                      customersAndAddressesAndOrdersAggregateRFMDataRdd, onePercent);
        JavaRDD<RFMSummaryStatistics> topFivePercentRFMSummaryStatisticsJavaRDD =
                calculateSummaryStatisticsTopXPercent(sc, applicationId,
                                                      customersAndAddressesAndOrdersAggregateRFMDataRdd, fivePercent);
        JavaRDD<RFMSummaryStatistics> topFifteenPercentRFMSummaryStatisticsJavaRDD =
                calculateSummaryStatisticsTopXPercent(sc, applicationId,
                                                      customersAndAddressesAndOrdersAggregateRFMDataRdd,
                                                      fifteenPercent);
        JavaRDD<RFMSummaryStatistics> topTwentyFivePercentRFMSummaryStatisticsJavaRDD =
                calculateSummaryStatisticsTopXPercent(sc, applicationId,
                                                      customersAndAddressesAndOrdersAggregateRFMDataRdd,
                                                      twentyFivePercent);
        JavaRDD<RFMSummaryStatistics> topFiftyPercentRFMSummaryStatisticsJavaRDD =
                calculateSummaryStatisticsTopXPercent(sc, applicationId,
                                                      customersAndAddressesAndOrdersAggregateRFMDataRdd, fiftyPercent);
        JavaRDD<RFMSummaryStatistics> bottomFiftyPercentRFMSummaryStatisticsJavaRDD =
                calculateSummaryStatisticsBottomXPercent(sc, applicationId,
                                                         customersAndAddressesAndOrdersAggregateRFMDataRdd,
                                                         bottomPercent);

        List<JavaRDD<RFMSummaryStatistics>> eightyTwentySummaryStatistics =
                new ArrayList<JavaRDD<RFMSummaryStatistics>>();

        eightyTwentySummaryStatistics.add(topOnePercentRFMSummaryStatisticsJavaRDD);
        eightyTwentySummaryStatistics.add(topFivePercentRFMSummaryStatisticsJavaRDD);
        eightyTwentySummaryStatistics.add(topFifteenPercentRFMSummaryStatisticsJavaRDD);
        eightyTwentySummaryStatistics.add(topTwentyFivePercentRFMSummaryStatisticsJavaRDD);
        eightyTwentySummaryStatistics.add(topFiftyPercentRFMSummaryStatisticsJavaRDD);
        eightyTwentySummaryStatistics.add(bottomFiftyPercentRFMSummaryStatisticsJavaRDD);

        return eightyTwentySummaryStatistics;
    }

    public static JavaRDD<RFMSummaryStatistics> calculateSummaryStatisticsTopXPercent(JavaSparkContext sc,
                                                                                      int applicationId,
                                                                                      JavaRDD<RFMData> customersAndAddressesAndOrdersAggregateRFMDataRdd,
                                                                                      Double xPercent) {
        JavaRDD<RFMData> topCustomersAndAddressesAndOrdersAggregateRFMDataRdd =
                customersAndAddressesAndOrdersAggregateRFMDataRdd.sortBy((Function<RFMData, BigDecimal>) rfmData -> {
                    if (rfmData != null && rfmData.getOrdersSubTotal() != null) {
                        return rfmData.getOrdersSubTotal();
                    } else {
                        return new BigDecimal(0);
                    }
                }, false, 1);

        topCustomersAndAddressesAndOrdersAggregateRFMDataRdd.cache();

        long topCustomerCount = (topCustomersAndAddressesAndOrdersAggregateRFMDataRdd != null) ?
                topCustomersAndAddressesAndOrdersAggregateRFMDataRdd.count() : 0;
        Integer topXPercentOfCustomers = ((Double) (xPercent * topCustomerCount)).intValue();

        List<RFMData> topXPercentOfCustomersList = (topCustomersAndAddressesAndOrdersAggregateRFMDataRdd != null) ?
                topCustomersAndAddressesAndOrdersAggregateRFMDataRdd.take(topXPercentOfCustomers) :
                new ArrayList<>();
        JavaRDD<RFMData> topXPercentOfCustomersRdd = sc.parallelize(topXPercentOfCustomersList);

        return calculateSummaryStatistics(sc, applicationId, topXPercentOfCustomersRdd,
                                          topCustomersAndAddressesAndOrdersAggregateRFMDataRdd);
    }

    public static JavaRDD<RFMSummaryStatistics> calculateSummaryStatisticsBottomXPercent(JavaSparkContext sc,
                                                                                         int applicationId,
                                                                                         JavaRDD<RFMData> customersAndAddressesAndOrdersAggregateRFMDataRdd,
                                                                                         Double xPercent) {
        JavaRDD<RFMData> topCustomersAndAddressesAndOrdersAggregateRFMDataRdd =
                customersAndAddressesAndOrdersAggregateRFMDataRdd.sortBy((Function<RFMData, BigDecimal>) rfmData -> {
                    if (rfmData != null && rfmData.getOrdersSubTotal() != null) {
                        return rfmData.getOrdersSubTotal();
                    } else {
                        return new BigDecimal(0);
                    }
                }, false, 1);

        topCustomersAndAddressesAndOrdersAggregateRFMDataRdd.cache();

        long topCustomerCount = topCustomersAndAddressesAndOrdersAggregateRFMDataRdd.count();

        Double topXPercent = 1 - xPercent;
        Integer topXPercentOfCustomers = ((Double) (topXPercent * topCustomerCount)).intValue();

        ArrayList<RFMData> bottomXPercentOfCustomersList = new ArrayList<RFMData>();

        List<RFMData> topCustomersAndAddressesAndOrdersAggregateRFMDataList =
                topCustomersAndAddressesAndOrdersAggregateRFMDataRdd.collect();

        for (int i = 0; i < topCustomersAndAddressesAndOrdersAggregateRFMDataList.size(); i++) {
            if (i >= topXPercentOfCustomers) {
                bottomXPercentOfCustomersList.add(topCustomersAndAddressesAndOrdersAggregateRFMDataList.get(i));
            }
        }

        JavaRDD<RFMData> bottomXPercentOfCustomersRdd = sc.parallelize(bottomXPercentOfCustomersList);
        return calculateSummaryStatistics(sc, applicationId, bottomXPercentOfCustomersRdd,
                                          topCustomersAndAddressesAndOrdersAggregateRFMDataRdd);
    }
}
