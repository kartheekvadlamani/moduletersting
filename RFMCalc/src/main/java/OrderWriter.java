/**
 * Created by brettcooper on 8/25/16.
 */
import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ProtocolVersion;
import com.datastax.spark.connector.writer.RowWriter;
import scala.collection.JavaConversions;
import scala.collection.immutable.IndexedSeq;
import scala.collection.immutable.Seq;

import java.io.Serializable;
import java.util.Arrays;

public class OrderWriter implements RowWriter<Order>, Serializable {
    // Return the list of column names in the Cassandra table
    public Seq<String> columnNames() {
        return toIndexedSeq(Arrays.asList("id", "application_id", "order_id", "order_number", "status", "financial_status", "customer_is_guest", "customer_id", "first_name", "last_name", "email", "sub_total_price", "total_discounts", "store_credit", "total_price", "currency_code", "source",
                "created_at", "updated_at", "customer_created_at", "is_synced", "billing_address_id", "shipping_address_id", "created", "modified", "consumer_order_id", "previous_status", "is_partial_data"));
    }

    public int estimateSizeInBytes(Order order) {
        return 100;
    }

    public BoundStatement bind(Order order, PreparedStatement statement, ProtocolVersion version) {
        BoundStatement boundStatement = new BoundStatement(statement);

        boundStatement.bind(order.getId(), order.getApplicationId(), order.getOrderId(), order.getOrderNumber(), order.getStatus(), order.getFinancialStatus(), order.getCustomerIsGuest(), order.getCustomerId(), order.getFirstName(), order.getLastName(), order.getEmail(), order.getSubTotalPrice(),
                order.getTotalDiscounts(), order.getStoreCredit(), order.getTotalPrice(), order.getCurrencyCode(), order.getSource(), order.getCreatedAt(), order.getUpdatedAt(), order.getCustomerCreatedAt(), order.getIsSynced(), order.getBillingAddressId(), order.getShippingAddressId(),
                order.getCreated(), order.getModified(), order.getConsumerOrderId(), order.getPreviousStatus(), order.getIsPartialData());

        return boundStatement;
    }

    private <T> IndexedSeq<T> toIndexedSeq(Iterable<T> iterable) {
        return JavaConversions.asScalaIterable(iterable).toIndexedSeq();
    }

    public void readColumnValues(Order order, Object[] buffer) {
        buffer[0] = order.getId();
        buffer[1] = order.getApplicationId();
        buffer[2] = order.getOrderId();
        buffer[3] = order.getOrderNumber();
        buffer[4] = order.getStatus();
        buffer[5] = order.getFinancialStatus();
        buffer[6] = order.getCustomerIsGuest();
        buffer[7] = order.getCustomerId();
        buffer[8] = order.getFirstName();
        buffer[9] = order.getLastName();
        buffer[10] = order.getEmail();
        buffer[11] = order.getSubTotalPrice();
        buffer[12] = order.getTotalDiscounts();
        buffer[13] = order.getStoreCredit();
        buffer[14] = order.getTotalPrice();
        buffer[15] = order.getCurrencyCode();
        buffer[16] = order.getSource();
        buffer[17] = order.getCreatedAt();
        buffer[18] = order.getUpdatedAt();
        buffer[19] = order.getCustomerCreatedAt();
        buffer[20] = order.getIsSynced();
        buffer[21] = order.getBillingAddressId();
        buffer[22] = order.getShippingAddressId();
        buffer[23] = order.getCreated();
        buffer[24] = order.getModified();
        buffer[25] = order.getConsumerOrderId();
        buffer[26] = order.getPreviousStatus();
        buffer[27] = order.getIsPartialData();
    }
}





