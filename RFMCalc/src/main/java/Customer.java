import java.io.Serializable;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by brettcooper on 4/14/16.
 */
public class Customer implements Serializable {
    private BigInteger id;
    private Integer applicationId;
    private BigInteger customerId;
    private String customerGroup;
    private String firstName;
    private String lastName;
    private String email;
    private Boolean optInNewsletter;
    private Date createdAt;
    private Date updatedAt;
    private Boolean isSynced;
    private Date created;
    private Date modified;
    private Integer consumerCustomerId;
    private Boolean isPartialData;

    public Customer(BigInteger id, int applicationId, BigInteger customerId, String customerGroup, String firstName,
                    String lastName, String email, Boolean optInNewsletter, Date createdAt, Date updatedAt,
                    Boolean isSynced, Date created, Date modified, Integer consumerCustomerId, Boolean isPartialData) {
        this.id = id;
        this.applicationId = applicationId;
        this.customerId = customerId;
        this.customerGroup = (customerGroup != null && !customerGroup.isEmpty()) ? customerGroup.replaceAll("\\r", "") :
                customerGroup;
        this.firstName = (firstName != null && !firstName.isEmpty()) ? firstName.replaceAll("\\r", "") : firstName;
        this.lastName = (lastName != null && !lastName.isEmpty()) ? lastName.replaceAll("\\r", "") : firstName;
        this.email = (email != null && !email.isEmpty()) ? email.replaceAll("\\r", "") : firstName;
        this.optInNewsletter = optInNewsletter;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.isSynced = isSynced;
        this.created = created;
        this.modified = modified;
        this.consumerCustomerId = consumerCustomerId;
        this.isPartialData = isPartialData;
    }

    public BigInteger getId() { return id; }

    public Integer getApplicationId() { return applicationId; }

    public BigInteger getCustomerId() { return customerId; }

    public String getCustomerGroup() {
        return customerGroup;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() { return email; }

    public Boolean getOptInNewsletter() { return optInNewsletter; }

    public Date getCreatedAt() {
        return createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public Boolean getIsSynced() {
        return isSynced;
    }

    public Date getCreated() {
        return created;
    }

    public Date getModified() {
        return modified;
    }

    public Integer getConsumerCustomerId() {
        return consumerCustomerId;
    }

    public Boolean getIsPartialData() {
        return isPartialData;
    }

    public String toString() {
        SimpleDateFormat outputDateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        String formattedCreatedAtDate = (createdAt != null) ? outputDateFormatter.format(createdAt) : null;
        String formattedUpdatedAtDate = (updatedAt != null) ? outputDateFormatter.format(updatedAt) : null;
        String formattedCreatedDate = (created != null) ? outputDateFormatter.format(created) : null;
        String formattedModifiedDate = (modified != null) ? outputDateFormatter.format(modified) : null;

        return String.format("%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s", id, RFMCalc.DELIMITER,
                             applicationId, RFMCalc.DELIMITER, customerId, RFMCalc.DELIMITER, customerGroup,
                             RFMCalc.DELIMITER, firstName, RFMCalc.DELIMITER, lastName, RFMCalc.DELIMITER, email,
                             RFMCalc.DELIMITER, optInNewsletter, RFMCalc.DELIMITER, formattedCreatedAtDate,
                             RFMCalc.DELIMITER, formattedUpdatedAtDate, RFMCalc.DELIMITER, isSynced, RFMCalc.DELIMITER,
                             formattedCreatedDate, RFMCalc.DELIMITER, formattedModifiedDate,
                             RFMCalc.DELIMITER, consumerCustomerId, RFMCalc.DELIMITER, isPartialData);
    }
}
