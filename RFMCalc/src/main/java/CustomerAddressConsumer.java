/**
 * Created by brettcooper on 8/24/16.
 */

import kafka.consumer.ConsumerIterator;
import kafka.consumer.KafkaStream;
import kafka.message.MessageAndMetadata;
import org.apache.avro.generic.IndexedRecord;
import org.apache.kafka.common.errors.SerializationException;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.slf4j.Logger;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.datastax.spark.connector.japi.CassandraJavaUtil.javaFunctions;

public class CustomerAddressConsumer implements Runnable {
    private KafkaStream _kafkaStream;
    private JavaSparkContext _sparkContext;
    private Logger _logger;
    private String _cassandraSchemaName;
    private String _cassandraTableName;

    public CustomerAddressConsumer(KafkaStream kafkaStream, JavaSparkContext sparkContext, Logger logger, String cassandraSchemaName, String cassandraTableName) {
        _kafkaStream = kafkaStream;
        _sparkContext = sparkContext;
        _logger = logger;
        _cassandraSchemaName = cassandraSchemaName;
        _cassandraTableName = cassandraTableName;
    }

    public void run() {
        ConsumerIterator it = _kafkaStream.iterator();

        while (it.hasNext()) {
            CustomerAddress customerAddress = parseCustomerAddressFromStream(it);

            if (customerAddress != null) {
                List<CustomerAddress> customerAddressList = new ArrayList<CustomerAddress>();
                customerAddressList.add(customerAddress);

                JavaRDD<CustomerAddress> customerAddressJavaRDD = _sparkContext.parallelize(customerAddressList);

                // Save to Cassandra
                javaFunctions(customerAddressJavaRDD).writerBuilder(_cassandraSchemaName, _cassandraTableName, new CustomerAddressWriterFactory()).saveToCassandra();

                _logger.debug("customerAddress: " + customerAddress.toString());
            }
        }
    }

    private CustomerAddress parseCustomerAddressFromStream(ConsumerIterator iterator) {
        try {
            MessageAndMetadata messageAndMetadata = iterator.next();
            String key = (String) messageAndMetadata.key();
            IndexedRecord value = (IndexedRecord) messageAndMetadata.message();

            Integer applicationId = (value.get(0) != null) ? RFMHelpers.TryParseInteger(value.get(0).toString()) : null;
            Integer id = (value.get(1) != null) ? RFMHelpers.TryParseInteger(value.get(1).toString()) : null;
            BigInteger customerId = (value.get(2) != null) ? RFMHelpers.TryParseBigInteger(value.get(2).toString()) : null;
            String firstName = (value.get(3) != null) ? value.get(3).toString() : null;
            String lastName = (value.get(4) != null) ? value.get(4).toString() : null;
            String company = (value.get(5) != null) ? value.get(5).toString() : null;
            String address1 = (value.get(6) != null) ? value.get(6).toString() : null;
            String address2 = (value.get(7) != null) ? value.get(7).toString() : null;
            String city = (value.get(8) != null) ? value.get(8).toString() : null;
            String province = (value.get(9) != null) ? value.get(9).toString() : null;
            String country = (value.get(10) != null) ? value.get(10).toString() : null;
            String provinceCode = (value.get(11) != null) ? value.get(11).toString() : null;
            String countryCode = (value.get(12) != null) ? value.get(12).toString() : null;
            String addressType = (value.get(13) != null) ? value.get(13).toString() : null;
            String zip = (value.get(14) != null) ? value.get(14).toString() : null;
            String telephone = (value.get(15) != null) ? value.get(15).toString() : null;
            Date created = (value.get(16) != null) ? new Date((Long) value.get(16)) : null;
            Date modified = (value.get(17) != null) ? new Date((Long) value.get(17)) : null;

            CustomerAddress customerAddress = new CustomerAddress(applicationId, id, customerId, firstName, lastName, company, address1, address2, city, province, country, provinceCode, countryCode, addressType, zip, telephone, created, modified);

            return customerAddress;
        }
        catch (SerializationException ex) {
            _logger.error(String.format("SerializationException: %s", ex.getMessage()));
            return null;
        }
    }
}




