import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.TypeCodec;
import com.datastax.spark.connector.cql.CassandraConnector;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

/**
 * Created by brettcooper on 8/4/16.
 */
public class Cassandra {
    private static Logger logger = LoggerFactory.getLogger(Cassandra.class);

    public static void initializeBatchLayerTables(JavaSparkContext javaSparkContext, String cassandraSchemaName,
                                                  String cassandraRFMResultsTableName,
                                                  String cassandraSummaryStatisticsTableName,
                                                  String cassandraTop1PercentSummaryStatisticsTableName,
                                                  String cassandraTop5PercentSummaryStatisticsTableName,
                                                  String cassandraTop15PercentSummaryStatisticsTableName,
                                                  String _cassandraTop25PercentSummaryStatisticsTableName,
                                                  String cassandraTop50PercentSummaryStatisticsTableName,
                                                  String cassandraBottom50PercentSummaryStatisticsTableName,
                                                  String cassandraRfmLastRunTableName) {
        Session session = null;

        try {
            CassandraConnector connector = CassandraConnector.apply(javaSparkContext.getConf());

            session = connector.openSession();

            session.execute(String.format(
                    "CREATE KEYSPACE IF NOT EXISTS %s WITH replication = {'class': 'SimpleStrategy', 'replication_factor': 1}",
                    cassandraSchemaName));
            session.execute(String.format(
                    "CREATE TABLE IF NOT EXISTS %s.%s (application_id INT, customer_id VARINT, customer_name TEXT, company_name TEXT, customer_group TEXT, customer_city TEXT, customer_state TEXT, customer_country TEXT, customer_email TEXT, " +
                    "orders_sub_total DECIMAL, orders_count INT, first_order_date TIMESTAMP, last_order_date TIMESTAMP, average_days_between_orders INT, first_order_amount DECIMAL, last_order_amount DECIMAL, average_order_price DECIMAL, customer_created_at TIMESTAMP, " +
                    "PRIMARY KEY (application_id, customer_id))", cassandraSchemaName, cassandraRFMResultsTableName));
            session.execute(String.format(
                    "CREATE TABLE IF NOT EXISTS %s.%s(application_id INT PRIMARY KEY, total_customers BIGINT, total_amount_of_orders DECIMAL, percentage_total_amount_of_orders DECIMAL, average_order_value DECIMAL, average_customer_lifetime_value DECIMAL, " +
                    "total_number_of_orders INT, percentage_total_number_of_orders DECIMAL, average_number_of_orders DECIMAL, percentage_of_repeat_buyers DECIMAL, percentage_revenue_from_repeat_buyers DECIMAL, average_days_since_last_order INT)",
                    cassandraSchemaName,
                    cassandraSummaryStatisticsTableName));
            session.execute(String.format(
                    "CREATE TABLE IF NOT EXISTS %s.%s(application_id INT PRIMARY KEY, total_customers BIGINT, total_amount_of_orders DECIMAL, percentage_total_amount_of_orders DECIMAL, average_order_value DECIMAL, average_customer_lifetime_value DECIMAL, " +
                    "total_number_of_orders INT, percentage_total_number_of_orders DECIMAL, average_number_of_orders DECIMAL, percentage_of_repeat_buyers DECIMAL, percentage_revenue_from_repeat_buyers DECIMAL, average_days_since_last_order INT)",
                    cassandraSchemaName,
                    cassandraTop1PercentSummaryStatisticsTableName));
            session.execute(String.format(
                    "CREATE TABLE IF NOT EXISTS %s.%s(application_id INT PRIMARY KEY, total_customers BIGINT, total_amount_of_orders DECIMAL, percentage_total_amount_of_orders DECIMAL, average_order_value DECIMAL, average_customer_lifetime_value DECIMAL, " +
                    "total_number_of_orders INT, percentage_total_number_of_orders DECIMAL, average_number_of_orders DECIMAL, percentage_of_repeat_buyers DECIMAL, percentage_revenue_from_repeat_buyers DECIMAL, average_days_since_last_order INT)",
                    cassandraSchemaName,
                    cassandraTop5PercentSummaryStatisticsTableName));
            session.execute(String.format(
                    "CREATE TABLE IF NOT EXISTS %s.%s(application_id INT PRIMARY KEY, total_customers BIGINT, total_amount_of_orders DECIMAL, percentage_total_amount_of_orders DECIMAL, average_order_value DECIMAL, average_customer_lifetime_value DECIMAL, " +
                    "total_number_of_orders INT, percentage_total_number_of_orders DECIMAL, average_number_of_orders DECIMAL, percentage_of_repeat_buyers DECIMAL, percentage_revenue_from_repeat_buyers DECIMAL, average_days_since_last_order INT)",
                    cassandraSchemaName,
                    cassandraTop15PercentSummaryStatisticsTableName));
            session.execute(String.format(
                    "CREATE TABLE IF NOT EXISTS %s.%s(application_id INT PRIMARY KEY, total_customers BIGINT, total_amount_of_orders DECIMAL, percentage_total_amount_of_orders DECIMAL, average_order_value DECIMAL, average_customer_lifetime_value DECIMAL, " +
                    "total_number_of_orders INT, percentage_total_number_of_orders DECIMAL, average_number_of_orders DECIMAL, percentage_of_repeat_buyers DECIMAL, percentage_revenue_from_repeat_buyers DECIMAL, average_days_since_last_order INT)",
                    cassandraSchemaName,
                    _cassandraTop25PercentSummaryStatisticsTableName));
            session.execute(String.format(
                    "CREATE TABLE IF NOT EXISTS %s.%s(application_id INT PRIMARY KEY, total_customers BIGINT, total_amount_of_orders DECIMAL, percentage_total_amount_of_orders DECIMAL, average_order_value DECIMAL, average_customer_lifetime_value DECIMAL, " +
                    "total_number_of_orders INT, percentage_total_number_of_orders DECIMAL, average_number_of_orders DECIMAL, percentage_of_repeat_buyers DECIMAL, percentage_revenue_from_repeat_buyers DECIMAL, average_days_since_last_order INT)",
                    cassandraSchemaName,
                    cassandraTop50PercentSummaryStatisticsTableName));
            session.execute(String.format(
                    "CREATE TABLE IF NOT EXISTS %s.%s(application_id INT PRIMARY KEY, total_customers BIGINT, total_amount_of_orders DECIMAL, percentage_total_amount_of_orders DECIMAL, average_order_value DECIMAL, average_customer_lifetime_value DECIMAL, " +
                    "total_number_of_orders INT, percentage_total_number_of_orders DECIMAL, average_number_of_orders DECIMAL, percentage_of_repeat_buyers DECIMAL, percentage_revenue_from_repeat_buyers DECIMAL, average_days_since_last_order INT)",
                    cassandraSchemaName,
                    cassandraBottom50PercentSummaryStatisticsTableName));
            session.execute(String.format(
                    "CREATE TABLE IF NOT EXISTS %s.%s(id INT PRIMARY KEY, last_run_date TEXT, last_run_customer_id BIGINT, last_run_customer_address_id INT, last_run_order_id INT)",
                    cassandraSchemaName, cassandraRfmLastRunTableName));
        } finally {
            if (session != null && !session.isClosed()) {
                session.close();
            }
        }
    }

    public static void initializeSpeedLayerTables(JavaSparkContext javaSparkContext, Logger logger,
                                                  String cassandraSchemaName,
                                                  String cassandraCustomersTableName,
                                                  String cassandraCustomerAddressesTableName,
                                                  String cassandraOrdersTableName, String cassandraRFMResultsTableName,
                                                  String cassandraSummaryStatisticsSpeedTableName,
                                                  String cassandraTop1PercentSummaryStatisticsSpeedTableName,
                                                  String cassandraTop5PercentSummaryStatisticsSpeedTableName,
                                                  String cassandraTop15PercentSummaryStatisticsSpeedTableName,
                                                  String cassandraTop25PercentSummaryStatisticsSpeedTableName,
                                                  String cassandraTop50PercentSummaryStatisticsSpeedTableName,
                                                  String cassandraBottom50PercentSummaryStatisticsSpeedTableName) {
        logger.info("Initializing speed layer tables");

        Session session = null;

        try {
            CassandraConnector connector = CassandraConnector.apply(javaSparkContext.getConf());

            session = connector.openSession();

            session.execute(String.format(
                    "CREATE KEYSPACE IF NOT EXISTS %s WITH replication = {'class': 'SimpleStrategy', 'replication_factor': 1}",
                    cassandraSchemaName));
            session.execute(String.format(
                    "CREATE TABLE IF NOT EXISTS %s.%s (id BIGINT, application_id INT, customer_id BIGINT, customer_group TEXT, first_name TEXT, last_name TEXT, email TEXT, opt_in_newsletter BOOLEAN, created_at TIMESTAMP, updated_at TIMESTAMP, is_synced BOOLEAN," +
                    "created TIMESTAMP, modified TIMESTAMP, consumer_customer_id INT, is_partial_data BOOLEAN, PRIMARY KEY (id))",
                    cassandraSchemaName, cassandraCustomersTableName));
            session.execute(String.format(
                    "CREATE TABLE IF NOT EXISTS %s.%s (application_id INT, id INT, customer_id BIGINT, first_name TEXT, last_name TEXT, company TEXT, address1 TEXT, address2 TEXT, city TEXT, province TEXT, country TEXT, province_code TEXT," +
                    "country_code TEXT, address_type TEXT, zip TEXT, telephone TEXT, created TIMESTAMP, modified TIMESTAMP, PRIMARY KEY (id))",
                    cassandraSchemaName, cassandraCustomerAddressesTableName));
            session.execute(String.format(
                    "CREATE TABLE IF NOT EXISTS %s.%s (id INT, application_id INT, order_id TEXT, order_number TEXT, status TEXT, financial_status TEXT, customer_is_guest BOOLEAN, customer_id BIGINT, first_name TEXT, last_name TEXT," +
                    "email TEXT, sub_total_price DECIMAL, total_discounts DECIMAL, store_credit DECIMAL, total_price DECIMAL, currency_code TEXT, source TEXT, created_at TIMESTAMP, updated_at TIMESTAMP, customer_created_at TIMESTAMP, is_synced BOOLEAN, billing_address_id TEXT, " +
                    "shipping_address_id TEXT, created TIMESTAMP, modified TIMESTAMP, consumer_order_id INT, previous_status TEXT, is_partial_data BOOLEAN, PRIMARY KEY (id))",
                    cassandraSchemaName, cassandraOrdersTableName));
            session.execute(String.format(
                    "CREATE TABLE IF NOT EXISTS %s.%s (application_id INT, customer_id VARINT, customer_name TEXT, company_name TEXT, customer_group TEXT, customer_city TEXT, customer_state TEXT, customer_country TEXT, customer_email TEXT, " +
                    "orders_sub_total DECIMAL, orders_count INT, first_order_date TIMESTAMP, last_order_date TIMESTAMP, average_days_between_orders INT, first_order_amount DECIMAL, last_order_amount DECIMAL, average_order_price DECIMAL, customer_created_at TIMESTAMP, " +
                    "PRIMARY KEY (application_id, customer_id))", cassandraSchemaName, cassandraRFMResultsTableName));
            session.execute(String.format(
                    "CREATE TABLE IF NOT EXISTS %s.%s(application_id INT PRIMARY KEY, total_customers BIGINT, total_amount_of_orders DECIMAL, percentage_total_amount_of_orders DECIMAL, average_order_value DECIMAL, average_customer_lifetime_value DECIMAL, " +
                    "total_number_of_orders INT, percentage_total_number_of_orders DECIMAL, average_number_of_orders DECIMAL, percentage_of_repeat_buyers DECIMAL, percentage_revenue_from_repeat_buyers DECIMAL, average_days_since_last_order INT)",
                    cassandraSchemaName,
                    cassandraSummaryStatisticsSpeedTableName));
            session.execute(String.format(
                    "CREATE TABLE IF NOT EXISTS %s.%s(application_id INT PRIMARY KEY, total_customers BIGINT, total_amount_of_orders DECIMAL, percentage_total_amount_of_orders DECIMAL, average_order_value DECIMAL, average_customer_lifetime_value DECIMAL, " +
                    "total_number_of_orders INT, percentage_total_number_of_orders DECIMAL, average_number_of_orders DECIMAL, percentage_of_repeat_buyers DECIMAL, percentage_revenue_from_repeat_buyers DECIMAL, average_days_since_last_order INT)",
                    cassandraSchemaName,
                    cassandraTop1PercentSummaryStatisticsSpeedTableName));
            session.execute(String.format(
                    "CREATE TABLE IF NOT EXISTS %s.%s(application_id INT PRIMARY KEY, total_customers BIGINT, total_amount_of_orders DECIMAL, percentage_total_amount_of_orders DECIMAL, average_order_value DECIMAL, average_customer_lifetime_value DECIMAL, " +
                    "total_number_of_orders INT, percentage_total_number_of_orders DECIMAL, average_number_of_orders DECIMAL, percentage_of_repeat_buyers DECIMAL, percentage_revenue_from_repeat_buyers DECIMAL, average_days_since_last_order INT)",
                    cassandraSchemaName,
                    cassandraTop5PercentSummaryStatisticsSpeedTableName));
            session.execute(String.format(
                    "CREATE TABLE IF NOT EXISTS %s.%s(application_id INT PRIMARY KEY, total_customers BIGINT, total_amount_of_orders DECIMAL, percentage_total_amount_of_orders DECIMAL, average_order_value DECIMAL, average_customer_lifetime_value DECIMAL, " +
                    "total_number_of_orders INT, percentage_total_number_of_orders DECIMAL, average_number_of_orders DECIMAL, percentage_of_repeat_buyers DECIMAL, percentage_revenue_from_repeat_buyers DECIMAL, average_days_since_last_order INT)",
                    cassandraSchemaName,
                    cassandraTop15PercentSummaryStatisticsSpeedTableName));
            session.execute(String.format(
                    "CREATE TABLE IF NOT EXISTS %s.%s(application_id INT PRIMARY KEY, total_customers BIGINT, total_amount_of_orders DECIMAL, percentage_total_amount_of_orders DECIMAL, average_order_value DECIMAL, average_customer_lifetime_value DECIMAL, " +
                    "total_number_of_orders INT, percentage_total_number_of_orders DECIMAL, average_number_of_orders DECIMAL, percentage_of_repeat_buyers DECIMAL, percentage_revenue_from_repeat_buyers DECIMAL, average_days_since_last_order INT)",
                    cassandraSchemaName,
                    cassandraTop25PercentSummaryStatisticsSpeedTableName));
            session.execute(String.format(
                    "CREATE TABLE IF NOT EXISTS %s.%s(application_id INT PRIMARY KEY, total_customers BIGINT, total_amount_of_orders DECIMAL, percentage_total_amount_of_orders DECIMAL, average_order_value DECIMAL, average_customer_lifetime_value DECIMAL, " +
                    "total_number_of_orders INT, percentage_total_number_of_orders DECIMAL, average_number_of_orders DECIMAL, percentage_of_repeat_buyers DECIMAL, percentage_revenue_from_repeat_buyers DECIMAL, average_days_since_last_order INT)",
                    cassandraSchemaName,
                    cassandraTop50PercentSummaryStatisticsSpeedTableName));
            session.execute(String.format(
                    "CREATE TABLE IF NOT EXISTS %s.%s(application_id INT PRIMARY KEY, total_customers BIGINT, total_amount_of_orders DECIMAL, percentage_total_amount_of_orders DECIMAL, average_order_value DECIMAL, average_customer_lifetime_value DECIMAL, " +
                    "total_number_of_orders INT, percentage_total_number_of_orders DECIMAL, average_number_of_orders DECIMAL, percentage_of_repeat_buyers DECIMAL, percentage_revenue_from_repeat_buyers DECIMAL, average_days_since_last_order INT)",
                    cassandraSchemaName,
                    cassandraBottom50PercentSummaryStatisticsSpeedTableName));
        } finally {
            if (session != null && !session.isClosed()) {
                session.close();
            }
        }
    }

    public static void truncateSpeedLayerTables(JavaSparkContext javaSparkContext, String cassandraSchemaName,
                                                List<String> tableNames) {
        if (tableNames == null || tableNames.size() == 0) {
            return;
        }

        Session session = null;

        try {
            CassandraConnector connector = CassandraConnector.apply(javaSparkContext.getConf());

            session = connector.openSession();

            for (String tableName : tableNames) {
                session.execute(String.format("TRUNCATE TABLE %s.%s", cassandraSchemaName, tableName));
            }
        } catch (Exception ex) {
            logger.error("Error cleaning up speed layer tables: {}", ex);
        } finally {
            if (session != null && !session.isClosed()) {
                session.close();
            }
        }
    }

    public static List<RFMLastRun> getLastRFMRuns(JavaSparkContext javaSparkContext, String cassandraSchemaName,
                                                  String cassandraRfmLastRunTableName) {
        Session session = null;
        List<RFMLastRun> runLastRunList = new ArrayList<RFMLastRun>();

        try {
            CassandraConnector connector = CassandraConnector.apply(javaSparkContext.getConf());

            session = connector.openSession();

            ResultSet lastRunDateSet = session.execute(String.format(
                    "SELECT id, last_run_date, last_run_customer_id, last_run_customer_address_id, last_run_order_id FROM %s.%s",
                    cassandraSchemaName, cassandraRfmLastRunTableName));

            if (lastRunDateSet != null) {
                List<Row> allDates = lastRunDateSet.all();

                for (Row row : allDates) {
                    Integer id = row.getInt(0);
                    String lastRunDate = row.getString(1);
                    Long lastRunCustomerIdLong = row.get(2, TypeCodec.bigint());
                    BigInteger lastRunCustomerId =
                            (lastRunCustomerIdLong != null) ? BigInteger.valueOf(lastRunCustomerIdLong) : null;
                    Integer lastRunCustomerAddressId = row.getInt(3);
                    Integer lastRunOrderId = row.getInt(4);

                    RFMLastRun rfmLastRun = new RFMLastRun(id, lastRunDate, lastRunCustomerId, lastRunCustomerAddressId,
                                                           lastRunOrderId);

                    runLastRunList.add(rfmLastRun);
                }

                runLastRunList.sort(new Comparator<RFMLastRun>() {
                    @Override
                    public int compare(RFMLastRun rfmLastRun1, RFMLastRun rfmLastRun2) {
                        return rfmLastRun1.getId().compareTo(rfmLastRun2.getId());
                    }
                });
            }
        } finally {
            if (session != null && !session.isClosed()) {
                session.close();
            }
        }

        return runLastRunList;
    }

    public static BigInteger getLastRunCustomerId(JavaSparkContext javaSparkContext, String cassandraSchemaName,
                                                  String cassandraRfmLastRunTableName) {
        List<RFMLastRun> rfmLastRuns =
                getLastRFMRuns(javaSparkContext, cassandraSchemaName, cassandraRfmLastRunTableName);
        BigInteger lastRunCustomerId = null;

        if (rfmLastRuns != null && rfmLastRuns.size() > 0) {
            RFMLastRun rfmLastRun = rfmLastRuns.get(rfmLastRuns.size() - 1);
            lastRunCustomerId = rfmLastRun.getlastRunCustomerId();
        }

        return lastRunCustomerId;
    }

    public static Integer getLastRunCustomerAddressId(JavaSparkContext javaSparkContext, String cassandraSchemaName,
                                                      String cassandraRfmLastRunTableName) {
        List<RFMLastRun> rfmLastRuns =
                getLastRFMRuns(javaSparkContext, cassandraSchemaName, cassandraRfmLastRunTableName);
        Integer lastRunCustomerAddressId = null;

        if (rfmLastRuns != null && rfmLastRuns.size() > 0) {
            RFMLastRun rfmLastRun = rfmLastRuns.get(rfmLastRuns.size() - 1);
            lastRunCustomerAddressId = rfmLastRun.getlastRunCustomerAddressId();
        }

        return lastRunCustomerAddressId;
    }

    public static Integer getLastRunOrderId(JavaSparkContext javaSparkContext, String cassandraSchemaName,
                                            String cassandraRfmLastRunTableName) {
        List<RFMLastRun> rfmLastRuns =
                getLastRFMRuns(javaSparkContext, cassandraSchemaName, cassandraRfmLastRunTableName);
        Integer lastRunOrderId = null;

        if (rfmLastRuns != null && rfmLastRuns.size() > 0) {
            RFMLastRun rfmLastRun = rfmLastRuns.get(rfmLastRuns.size() - 1);
            lastRunOrderId = rfmLastRun.getlastRunOrderId();
        }

        return lastRunOrderId;
    }

    private static List<Customer> getSpeedLayerCustomers(Session session, String cassandraSchemaName,
                                                         String customersTableName) {
        ResultSet customersSet = session.execute(String.format(
                "SELECT id, application_id, customer_id, customer_group, first_name, last_name, email, opt_in_newsletter, created_at, updated_at, is_synced, created, modified, consumer_customer_id, is_partial_data FROM %s.%s",
                cassandraSchemaName, customersTableName));

        List<Customer> customers = new ArrayList<Customer>();

        if (customersSet != null) {
            for (Row customerRow : customersSet.all()) {
                BigInteger id = (customerRow.get(0, TypeCodec.bigint()) != null) ?
                        BigInteger.valueOf(customerRow.get(0, TypeCodec.bigint())) : null;
                Integer applicationId = customerRow.get(1, Integer.class);
                BigInteger customerId = (customerRow.get(2, TypeCodec.bigint()) != null) ?
                        BigInteger.valueOf(customerRow.get(2, TypeCodec.bigint())) : null;
                String customerGroup = customerRow.getString(3);
                String firstName = customerRow.getString(4);
                String lastName = customerRow.getString(5);
                String email = customerRow.getString(6);
                Boolean optInNewsletter = customerRow.get(7, Boolean.class);
                Date createdAt = customerRow.getTimestamp(8);
                Date updatedAt = customerRow.getTimestamp(9);
                Boolean isSynced = customerRow.get(10, Boolean.class);
                Date created = customerRow.getTimestamp(11);
                Date modified = customerRow.getTimestamp(12);
                Integer consumerCustomerId = customerRow.get(13, Integer.class);
                Boolean isPartialData = customerRow.get(14, Boolean.class);

                Customer customer =
                        new Customer(id, applicationId, customerId, customerGroup, firstName, lastName, email,
                                     optInNewsletter, createdAt, updatedAt, isSynced, created, modified,
                                     consumerCustomerId, isPartialData);

                customers.add(customer);
            }
        }

        return customers;
    }

    private static List<CustomerAddress> getSpeedLayerCustomerAddresses(Session session, String cassandraSchemaName,
                                                                        String customerAddressesTableName) {
        ResultSet customerAddressesSet = session.execute(String.format(
                "SELECT application_id, id, customer_id, first_name, last_name, company, address1, address2, city, province, country, province_code, country_code, address_type, zip, telephone, created, modified FROM %s.%s",
                cassandraSchemaName, customerAddressesTableName));

        List<CustomerAddress> customerAddresses = new ArrayList<CustomerAddress>();

        if (customerAddressesSet != null) {
            for (Row customerAddressRow : customerAddressesSet.all()) {
                Integer applicationId = customerAddressRow.get(0, Integer.class);
                Integer id = customerAddressRow.get(1, Integer.class);
                BigInteger customerId = (customerAddressRow.get(2, TypeCodec.bigint()) != null) ?
                        BigInteger.valueOf(customerAddressRow.get(2, TypeCodec.bigint())) : null;
                String firstName = customerAddressRow.getString(3);
                String lastName = customerAddressRow.getString(4);
                String company = customerAddressRow.getString(5);
                String address1 = customerAddressRow.getString(6);
                String address2 = customerAddressRow.getString(7);
                String city = customerAddressRow.getString(8);
                String province = customerAddressRow.getString(9);
                String country = customerAddressRow.getString(10);
                String provinceCode = customerAddressRow.getString(11);
                String countryCode = customerAddressRow.getString(12);
                String addressType = customerAddressRow.getString(13);
                String zip = customerAddressRow.getString(14);
                String telephone = customerAddressRow.getString(15);
                Date created = customerAddressRow.getTimestamp(16);
                Date modified = customerAddressRow.getTimestamp(17);

                CustomerAddress customerAddress =
                        new CustomerAddress(applicationId, id, customerId, firstName, lastName, company, address1,
                                            address2, city, province, country, provinceCode, countryCode, addressType,
                                            zip, telephone, created, modified);

                customerAddresses.add(customerAddress);
            }
        }

        return customerAddresses;

    }

    private static List<Order> getSpeedLayerOrders(Session session, String cassandraSchemaName,
                                                   String ordersTableName) {
        ResultSet ordersSet = session.execute(String.format(
                "SELECT id, application_id, order_id, order_number, status, financial_status, customer_is_guest, customer_id, first_name, last_name, email, sub_total_price, total_discounts, store_credit, total_price, currency_code, source," +
                "created_at, updated_at, customer_created_at, is_synced, billing_address_id, shipping_address_id, created, modified, consumer_order_id, previous_status, is_partial_data FROM %s.%s",
                cassandraSchemaName, ordersTableName));

        List<Order> orders = new ArrayList<Order>();

        if (ordersSet != null) {
            for (Row orderRow : ordersSet.all()) {
                Integer id = orderRow.get(0, Integer.class);
                Integer applicationId = orderRow.get(1, Integer.class);
                String orderId = orderRow.getString(2);
                String orderNumber = orderRow.getString(3);
                String status = orderRow.getString(4);
                String financialStatus = orderRow.getString(5);
                Boolean customerIsGuest = orderRow.get(6, Boolean.class);
                BigInteger customerId = (orderRow.get(7, TypeCodec.bigint()) != null) ?
                        BigInteger.valueOf(orderRow.get(7, TypeCodec.bigint())) : null;
                String firstName = orderRow.getString(8);
                String lastName = orderRow.getString(9);
                String email = orderRow.getString(10);
                BigDecimal subTotalPrice = orderRow.get(11, TypeCodec.decimal());
                BigDecimal totalDiscounts = orderRow.get(12, TypeCodec.decimal());
                BigDecimal storeCredit = orderRow.get(13, TypeCodec.decimal());
                BigDecimal totalPrice = orderRow.get(14, TypeCodec.decimal());
                String currencyCode = orderRow.getString(15);
                String source = orderRow.getString(16);
                Date createdAt = orderRow.getTimestamp(17);
                Date updatedAt = orderRow.getTimestamp(18);
                Date customerCreatedAt = orderRow.getTimestamp(19);
                Boolean isSynced = orderRow.get(20, Boolean.class);
                String billingAddressId = orderRow.getString(21);
                String shippingAddressId = orderRow.getString(22);
                Date created = orderRow.getTimestamp(23);
                Date modified = orderRow.getTimestamp(24);
                Integer consumerOrderId = orderRow.get(25, Integer.class);
                String previousStatus = orderRow.getString(26);
                Boolean isPartialData = orderRow.get(27, Boolean.class);

                Order order =
                        new Order(id, applicationId, orderId, orderNumber, status, financialStatus, customerIsGuest,
                                  customerId, firstName, lastName, email, subTotalPrice, totalDiscounts, storeCredit,
                                  totalPrice, currencyCode, source, createdAt, updatedAt, customerCreatedAt,
                                  isSynced, billingAddressId, shippingAddressId, created, modified, consumerOrderId,
                                  previousStatus, isPartialData);

                orders.add(order);
            }
        }

        return orders;
    }

    public static void cleanupCassandra(JavaSparkContext javaSparkContext, String cassandraSchemaName,
                                        String cassandraRfmLastRunTableName, String currentDateTimePath,
                                        BigInteger lastRunCustomerId, Integer lastRunCustomerAddressId,
                                        Integer lastRunOrderId) {
        Session session = null;

        try {
            CassandraConnector connector = CassandraConnector.apply(javaSparkContext.getConf());

            session = connector.openSession();

            ResultSet lastRunDateSet = session.execute(
                    String.format("SELECT id, last_run_date FROM %s.%s", cassandraSchemaName,
                                  cassandraRfmLastRunTableName));

            if (lastRunDateSet != null) {
                List<Row> allDates = lastRunDateSet.all();
                Integer allDatesSize = allDates.size();

                if (allDatesSize > 0) {
                    // Sort by id, ascending
                    allDates.sort(new Comparator<Row>() {
                        @Override
                        public int compare(Row row1, Row row2) {
                            Integer row1Id = row1.getInt(0);
                            Integer row2Id = row2.getInt(0);

                            return row1Id.compareTo(row2Id);
                        }
                    });

                    Row allDatesLastRow = allDates.get(allDatesSize - 1);
                    Integer lastRunId = allDatesLastRow.getInt(0);
                    String lastRunDate = allDatesLastRow.getString(1);
                    Integer currentRunId = lastRunId + 1;

                    insertIntoRfmLastRun(session, cassandraSchemaName, cassandraRfmLastRunTableName, currentRunId,
                                         currentDateTimePath, lastRunCustomerId, lastRunCustomerAddressId,
                                         lastRunOrderId);

                    for (int i = 0; i < allDatesSize; i++) {
                        Row previousRunRow = allDates.get(i);
                        String previousRunDate = previousRunRow.getString(1);

                        if (previousRunDate != null && !previousRunDate.isEmpty()) {
                            String cassandraLastRunRFMResultsTableName = String.format("rfm_results_%s", lastRunDate);
                            String cassandraLastRunSummaryStatisticsTableName =
                                    String.format("summary_stats_%s", lastRunDate);
                            String cassandraLastRunTop1PercentSummaryStatisticsTableName =
                                    String.format("summary_stats_top_1_%s", lastRunDate);
                            String cassandraLastRunTop5PercentSummaryStatisticsTableName =
                                    String.format("summary_stats_top_5_%s", lastRunDate);
                            String cassandraLastRunTop15PercentSummaryStatisticsTableName =
                                    String.format("summary_stats_top_15_%s", lastRunDate);
                            String cassandraLastRunTop25PercentSummaryStatisticsTableName =
                                    String.format("summary_stats_top_25_%s", lastRunDate);
                            String cassandraLastRunTop50PercentSummaryStatisticsTableName =
                                    String.format("summary_stats_top_50_%s", lastRunDate);
                            String cassandraLastRunBottom50PercentSummaryStatisticsTableName =
                                    String.format("summary_stats_bottom_50_%s", lastRunDate);

                            session.execute(String.format("DROP TABLE IF EXISTS %s.%s", cassandraSchemaName,
                                                          cassandraLastRunTop1PercentSummaryStatisticsTableName));
                            session.execute(String.format("DROP TABLE IF EXISTS %s.%s", cassandraSchemaName,
                                                          cassandraLastRunTop5PercentSummaryStatisticsTableName));
                            session.execute(String.format("DROP TABLE IF EXISTS %s.%s", cassandraSchemaName,
                                                          cassandraLastRunTop15PercentSummaryStatisticsTableName));
                            session.execute(String.format("DROP TABLE IF EXISTS %s.%s", cassandraSchemaName,
                                                          cassandraLastRunTop25PercentSummaryStatisticsTableName));
                            session.execute(String.format("DROP TABLE IF EXISTS %s.%s", cassandraSchemaName,
                                                          cassandraLastRunTop50PercentSummaryStatisticsTableName));
                            session.execute(String.format("DROP TABLE IF EXISTS %s.%s", cassandraSchemaName,
                                                          cassandraLastRunBottom50PercentSummaryStatisticsTableName));
                            session.execute(String.format("DROP TABLE IF EXISTS %s.%s", cassandraSchemaName,
                                                          cassandraLastRunSummaryStatisticsTableName));
                            session.execute(String.format("DROP TABLE IF EXISTS %s.%s", cassandraSchemaName,
                                                          cassandraLastRunRFMResultsTableName));
                        }
                    }
                } else {
                    insertFirstJobRunDate(session, cassandraSchemaName, cassandraRfmLastRunTableName,
                                          currentDateTimePath, lastRunCustomerId, lastRunCustomerAddressId,
                                          lastRunOrderId);
                }
            } else {
                insertFirstJobRunDate(session, cassandraSchemaName, cassandraRfmLastRunTableName, currentDateTimePath,
                                      lastRunCustomerId, lastRunCustomerAddressId, lastRunOrderId);
            }
        } finally {
            if (session != null && !session.isClosed()) {
                session.close();
            }
        }
    }

    public static JavaRDD<Customer> getSpeedLayerFilteredCustomers(JavaSparkContext sparkContext, Session session,
                                                                   String cassandraSchemaName,
                                                                   String cassandraRfmLastRunTableName,
                                                                   String customersTableName) {
        JavaRDD<Customer> filteredCustomersJavaRDD = null;
        BigInteger lastRunCustomerId =
                getLastRunCustomerId(sparkContext, cassandraSchemaName, cassandraRfmLastRunTableName);
        List<Customer> customers = getSpeedLayerCustomers(session, cassandraSchemaName, customersTableName);

        JavaRDD<Customer> customersJavaRDD = sparkContext.parallelize(customers);

        // Filter customers based on the last run customer id from the nightly batch job
        if (lastRunCustomerId != null) {
            filteredCustomersJavaRDD = getFilteredCustomers(customersJavaRDD, lastRunCustomerId, true);
        } else {
            filteredCustomersJavaRDD = customersJavaRDD;
        }

        return filteredCustomersJavaRDD;
    }

    public static JavaRDD<CustomerAddress> getSpeedLayerFilteredCustomerAddresses(JavaSparkContext sparkContext,
                                                                                  Session session,
                                                                                  String cassandraSchemaName,
                                                                                  String cassandraRfmLastRunTableName,
                                                                                  String customerAddressesTableName) {
        JavaRDD<CustomerAddress> filteredCustomerAddressJavaRDD = null;
        Integer lastRunCustomerAddressId =
                getLastRunCustomerAddressId(sparkContext, cassandraSchemaName, cassandraRfmLastRunTableName);
        List<CustomerAddress> customerAddresses =
                getSpeedLayerCustomerAddresses(session, cassandraSchemaName, customerAddressesTableName);

        JavaRDD<CustomerAddress> customerAddressJavaRDD = sparkContext.parallelize(customerAddresses);

        // Filter customer addresses based on the last run customer id from the nightly batch job
        if (lastRunCustomerAddressId != null) {
            filteredCustomerAddressJavaRDD =
                    getFilteredCustomerAddresses(customerAddressJavaRDD, lastRunCustomerAddressId, true);
        } else {
            filteredCustomerAddressJavaRDD = customerAddressJavaRDD;
        }

        return filteredCustomerAddressJavaRDD;
    }

    public static JavaRDD<Order> getSpeedLayerFilteredOrders(JavaSparkContext sparkContext, Session session,
                                                             String cassandraSchemaName,
                                                             String cassandraRfmLastRunTableName,
                                                             String ordersTableName) {
        JavaRDD<Order> filteredOrderJavaRDD = null;
        List<Order> orders = getSpeedLayerOrders(session, cassandraSchemaName, ordersTableName);
        Integer lastRunOrderId = getLastRunOrderId(sparkContext, cassandraSchemaName, cassandraRfmLastRunTableName);

        JavaRDD<Order> orderJavaRDD = sparkContext.parallelize(orders);

        // Filter orders based on the last run customer id from the nightly batch job
        if (lastRunOrderId != null) {
            filteredOrderJavaRDD = getFilteredOrders(orderJavaRDD, lastRunOrderId, true);
        } else {
            filteredOrderJavaRDD = orderJavaRDD;
        }

        return filteredOrderJavaRDD;
    }

    private static void insertIntoRfmLastRun(Session session, String cassandraSchemaName,
                                             String cassandraRfmLastRunTableName, Integer currentRunId,
                                             String currentDateTimePath, BigInteger lastRunCustomerId,
                                             Integer lastRunCustomerAddressId, Integer lastRunOrderId) {
        session.execute(String.format(
                "INSERT INTO %s.%s (id, last_run_date, last_run_customer_id, last_run_customer_address_id, last_run_order_id) values (%s, '%s', %s, %s, %s)",
                cassandraSchemaName, cassandraRfmLastRunTableName, currentRunId,
                currentDateTimePath, lastRunCustomerId, lastRunCustomerAddressId, lastRunOrderId));
    }

    private static void insertFirstJobRunDate(Session session, String cassandraSchemaName,
                                              String cassandraRfmLastRunTableName, String currentDateTimePath,
                                              BigInteger lastRunCustomerId, Integer lastRunCustomerAddressId,
                                              Integer lastRunOrderId) {
        insertIntoRfmLastRun(session, cassandraSchemaName, cassandraRfmLastRunTableName, 1, currentDateTimePath,
                             lastRunCustomerId, lastRunCustomerAddressId, lastRunOrderId);
    }

    private static JavaRDD<Order> getFilteredOrders(JavaRDD<Order> orderJavaRDD, Integer lastRunOrderId,
                                                    Boolean greaterThan) {
        return orderJavaRDD.filter(new Function<Order, Boolean>() {
            @Override
            public Boolean call(Order order) throws Exception {
                if (greaterThan) {
                    return order.getId() > lastRunOrderId;
                } else {
                    return order.getId() <= lastRunOrderId;
                }
            }
        });
    }

    private static JavaRDD<CustomerAddress> getFilteredCustomerAddresses(
            JavaRDD<CustomerAddress> customerAddressJavaRDD, Integer lastRunCustomerAddressId,
            Boolean greaterThan) {
        return customerAddressJavaRDD.filter(new Function<CustomerAddress, Boolean>() {
            @Override
            public Boolean call(CustomerAddress customerAddress) throws Exception {
                if (greaterThan) {
                    return customerAddress.getId() > lastRunCustomerAddressId;
                } else {
                    return customerAddress.getId() <= lastRunCustomerAddressId;
                }
            }
        });
    }

    public static JavaRDD<Customer> getFilteredCustomers(JavaRDD<Customer> customersJavaRDD,
                                                         BigInteger lastRunCustomerId,
                                                         Boolean greaterThan) {
        return customersJavaRDD.filter(new Function<Customer, Boolean>() {
            @Override
            public Boolean call(Customer customer) throws Exception {
                if (greaterThan) {
                    return customer.getId().compareTo(lastRunCustomerId) > 0;
                } else {
                    return customer.getId().compareTo(lastRunCustomerId) <= 0;
                }
            }
        });
    }
}
