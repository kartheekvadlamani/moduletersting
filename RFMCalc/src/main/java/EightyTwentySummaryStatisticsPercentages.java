/**
 * Created by brettcooper on 8/30/16.
 */
public final class EightyTwentySummaryStatisticsPercentages {
    private EightyTwentySummaryStatisticsPercentages() { }

    public static final Double OnePercent = .01;
    public static final Double FivePercent = .05;
    public static final Double FifteenPercent = .15;
    public static final Double TwentyFivePercent = .25;
    public static final Double FiftyPercent = .50;
    public static final Double BottomFiftyPercent = .50;
}
