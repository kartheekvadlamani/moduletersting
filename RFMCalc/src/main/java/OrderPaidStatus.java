import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static java.util.Arrays.stream;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toMap;

/**
 * Created by brettcooper on 2/24/17.
 */
public enum OrderPaidStatus {
    PAID("paid"),
    CHECK("check"),
    CHECK_PD("check-pd"),
    COMPLETE("completed"),
    INVOICE_PAID("invoice-pd"),
    PARTIAL_PAYMENT("partial payment", "partial-payment", "partially_paid");

    private static final Map<List<String>, OrderPaidStatus> STATUSES_BY_NAME =
            stream(values()).collect(toMap(status -> status.statusList, identity()));
    private final List<String> statusList = new ArrayList<>();

    OrderPaidStatus(String... statuses) {
        for (String status : statuses) {
            if (status != null && !status.isEmpty()) {
                statusList.add(status);
            }
        }
    }

    public static OrderPaidStatus getOrderPaidStatus(String orderFinancialStatus) {
        if (orderFinancialStatus == null || orderFinancialStatus.isEmpty()) {
            return null;
        }

        int count = 0;

        for (List<String> names : STATUSES_BY_NAME.keySet()) {
            if (names.contains(orderFinancialStatus.toLowerCase())) {
                return (OrderPaidStatus) STATUSES_BY_NAME.values().toArray()[count];
            }

            count++;
        }

        return null;
    }
}
