import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by brettcooper on 5/11/16.
 */
public class RFMData implements Serializable {
    private Integer applicationId;
    private BigInteger customerId;
    private String customerName;
    private String companyName;
    private String customerGroup;
    private String customerCity;
    private String customerState;
    private String customerCountry;
    private String customerEmail;
    private BigDecimal ordersSubTotal;
    private Integer orderCount;
    private Date firstOrderDate;
    private Date lastOrderDate;
    private Integer averageDaysBetweenOrders;
    private BigDecimal firstOrderAmount;
    private BigDecimal lastOrderAmount;
    private BigDecimal averageOrderPrice;
    private Date customerCreatedAt;

    public RFMData() { }

    public RFMData(Integer applicationId, BigInteger customerId, String customerName, String companyName, String customerGroup, String customerCity, String customerState, String customerCountry, String customerEmail, BigDecimal ordersSubTotal, Integer orderCount, Date firstOrderDate,
                   Date lastOrderDate, Integer averageDaysBetweenOrders, BigDecimal firstOrderAmount, BigDecimal lastOrderAmount, BigDecimal averageOrderPrice, Date customerCreatedAt)
    {
        this.applicationId = applicationId;
        this.customerId = customerId;
        this.customerName = customerName;
        this.companyName = RFMHelpers.ToPascalCase(companyName);
        this.customerGroup = RFMHelpers.ToPascalCase(customerGroup);
        this.customerCity = RFMHelpers.ToPascalCase(customerCity);
        this.customerState = (customerState != null && !customerState.isEmpty()) ? customerState.toUpperCase() : null;
        this.customerCountry = customerCountry;
        this.customerEmail = customerEmail;
        this.ordersSubTotal = ordersSubTotal;
        this.orderCount = orderCount;
        this.firstOrderDate = firstOrderDate;
        this.lastOrderDate = lastOrderDate;
        this.averageDaysBetweenOrders = averageDaysBetweenOrders;
        this.firstOrderAmount = firstOrderAmount;
        this.lastOrderAmount = lastOrderAmount;
        this.averageOrderPrice = averageOrderPrice;
        this.customerCreatedAt = customerCreatedAt;
    }

    public Integer getApplicationId() { return applicationId; }
    public BigInteger getCustomerId() { return customerId; }
    public String getCustomerName() { return customerName; }
    public String getCompanyName() { return companyName; }
    public String getCustomerGroup() { return customerGroup; }
    public String getCustomerCity() { return customerCity; }
    public String getCustomerState() { return customerState; }
    public String getCustomerCountry() { return customerCountry; }
    public String getCustomerEmail() { return customerEmail; }
    public BigDecimal getOrdersSubTotal() { return ordersSubTotal; }
    public Integer getOrderCount() { return orderCount; }
    public Date getFirstOrderDate() { return firstOrderDate; }
    public Date getLastOrderDate() { return lastOrderDate; }
    public Integer getAverageDaysBetweenOrders() { return averageDaysBetweenOrders; }
    public BigDecimal getFirstOrderAmount() { return firstOrderAmount; }
    public BigDecimal getLastOrderAmount() { return lastOrderAmount; }
    public BigDecimal getAverageOrderPrice() { return averageOrderPrice; }
    public Date getCustomerCreatedAt() { return customerCreatedAt; }

    public String toString() {
        SimpleDateFormat outputDateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        String formattedFirstOrderDate = (firstOrderDate != null) ? outputDateFormatter.format(firstOrderDate) : null;
        String formattedLastOrderDate = (lastOrderDate != null) ? outputDateFormatter.format(lastOrderDate) : null;
        String formattedCustomerCreatedAtDate = (customerCreatedAt != null) ? outputDateFormatter.format(customerCreatedAt) : null;

        return String.format("%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s", applicationId, customerId, customerName, companyName, customerGroup, customerCity, customerState, customerCountry, customerEmail, ordersSubTotal, orderCount,
                formattedFirstOrderDate, formattedLastOrderDate, averageDaysBetweenOrders, firstOrderAmount, lastOrderAmount, averageOrderPrice, formattedCustomerCreatedAtDate);
    }
}
