import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by brettcooper on 4/14/16.
 */
public class Order implements Serializable {
    private int id;
    private Integer applicationId;
    private String orderId;
    private String orderNumber;
    private String status;
    private String financialStatus;
    private Boolean customerIsGuest;
    private BigInteger customerId;
    private String firstName;
    private String lastName;
    private String email;
    private BigDecimal subTotalPrice;
    private BigDecimal totalDiscounts;
    private BigDecimal storeCredit;
    private BigDecimal totalPrice;
    private String currencyCode;
    private String source;
    private Date createdAt;
    private Date updatedAt;
    private Date customerCreatedAt;
    private Boolean isSynced;
    private String billingAddressId;
    private String shippingAddressId;
    private Date created;
    private Date modified;
    private Integer consumerOrderId;
    private String previousStatus;
    private Boolean isPartialData;

    public Order(int id, int applicationId, String orderId, String orderNumber, String status, String financialStatus,
                 Boolean customerIsGuest, BigInteger customerId, String firstName, String lastName, String email,
                 BigDecimal subTotalPrice, BigDecimal totalDiscounts, BigDecimal storeCredit,
                 BigDecimal totalPrice, String currencyCode, String source, Date createdAt, Date updatedAt,
                 Date customerCreatedAt, Boolean isSynced, String billingAddressId, String shippingAddressId,
                 Date created, Date modified, Integer consumerOrderId, String previousStatus, Boolean isPartialData) {
        this.id = id;
        this.applicationId = applicationId;
        this.orderId = orderId;
        this.orderNumber = orderNumber;
        this.status = (status != null && !status.isEmpty()) ? status.replaceAll("\\n", "") : status;
        this.financialStatus = financialStatus;
        this.customerIsGuest = customerIsGuest;
        this.customerId = customerId;
        this.firstName =
                (firstName != null && !firstName.isEmpty()) ? firstName.replaceAll("\\\\", "").replaceAll("\\r", "") :
                        firstName;
        this.lastName =
                (lastName != null && !lastName.isEmpty()) ? lastName.replaceAll("\\\\", "").replaceAll("\\r", "") :
                        lastName;
        this.email = email;
        this.subTotalPrice = subTotalPrice;
        this.totalDiscounts = totalDiscounts;
        this.storeCredit = storeCredit;
        this.totalPrice = totalPrice;
        this.currencyCode = currencyCode;
        this.source = source;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.customerCreatedAt = customerCreatedAt;
        this.isSynced = isSynced;
        this.billingAddressId = billingAddressId;
        this.shippingAddressId = shippingAddressId;
        this.created = created;
        this.modified = modified;
        this.consumerOrderId = consumerOrderId;
        this.previousStatus =
                (previousStatus != null && !previousStatus.isEmpty()) ? previousStatus.replaceAll("\\n", "") :
                        previousStatus;
        this.isPartialData = isPartialData;
    }

    public int getId() { return id; }

    public Integer getApplicationId() { return applicationId; }

    public String getOrderId() { return orderId; }

    public String getOrderNumber() { return orderNumber; }

    public String getStatus() { return status; }

    public String getFinancialStatus() { return financialStatus; }

    public Boolean getCustomerIsGuest() { return customerIsGuest; }

    public BigInteger getCustomerId() { return customerId; }

    public String getFirstName() { return firstName; }

    public String getLastName() { return lastName; }

    public String getEmail() { return email; }

    public BigDecimal getSubTotalPrice() { return subTotalPrice; }

    public BigDecimal getTotalDiscounts() { return totalDiscounts; }

    public BigDecimal getStoreCredit() { return storeCredit; }

    public BigDecimal getTotalPrice() { return totalPrice; }

    public String getCurrencyCode() { return currencyCode; }

    public String getSource() { return source; }

    public Date getCreatedAt() { return createdAt; }

    public Date getUpdatedAt() { return updatedAt; }

    public Date getCustomerCreatedAt() { return customerCreatedAt; }

    public Boolean getIsSynced() { return isSynced; }

    public String getBillingAddressId() { return billingAddressId; }

    public String getShippingAddressId() { return shippingAddressId; }

    public Date getCreated() { return created; }

    public Date getModified() { return modified; }

    public Integer getConsumerOrderId() { return consumerOrderId; }

    public String getPreviousStatus() { return previousStatus; }

    public Boolean getIsPartialData() { return isPartialData; }

    public String toString() {
        SimpleDateFormat outputDateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        String formattedCreatedAtDate = (createdAt != null) ? outputDateFormatter.format(createdAt) : null;
        String formattedUpdatedAtDate = (updatedAt != null) ? outputDateFormatter.format(updatedAt) : null;
        String formattedCustomerCreatedAtDate =
                (customerCreatedAt != null) ? outputDateFormatter.format(customerCreatedAt) : null;
        String formattedCreatedDate = (created != null) ? outputDateFormatter.format(created) : null;
        String formattedModifiedDate = (modified != null) ? outputDateFormatter.format(modified) : null;

        return String
                .format("%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s",
                        id, RFMCalc.DELIMITER, applicationId, RFMCalc.DELIMITER, orderId, RFMCalc.DELIMITER,
                        orderNumber, RFMCalc.DELIMITER, status, RFMCalc.DELIMITER, financialStatus, RFMCalc.DELIMITER,
                        customerIsGuest, RFMCalc.DELIMITER, customerId, RFMCalc.DELIMITER,
                        firstName, RFMCalc.DELIMITER, lastName, RFMCalc.DELIMITER, email, RFMCalc.DELIMITER,
                        subTotalPrice, RFMCalc.DELIMITER, totalDiscounts, RFMCalc.DELIMITER, storeCredit,
                        RFMCalc.DELIMITER, totalPrice, RFMCalc.DELIMITER,
                        currencyCode, RFMCalc.DELIMITER, source, RFMCalc.DELIMITER, formattedCreatedAtDate,
                        RFMCalc.DELIMITER, formattedUpdatedAtDate, RFMCalc.DELIMITER,
                        formattedCustomerCreatedAtDate, RFMCalc.DELIMITER, isSynced, RFMCalc.DELIMITER,
                        billingAddressId, RFMCalc.DELIMITER, shippingAddressId, RFMCalc.DELIMITER,
                        formattedCreatedDate, RFMCalc.DELIMITER, formattedModifiedDate, RFMCalc.DELIMITER,
                        consumerOrderId, RFMCalc.DELIMITER, previousStatus, RFMCalc.DELIMITER, isPartialData);
    }
}
