import java.io.Serializable;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by brettcooper on 4/14/16.
 */
public class CustomerAddress implements Serializable {
    private Integer applicationId;
    private int id;
    private BigInteger customerId;
    private String firstName;
    private String lastName;
    private String company;
    private String address1;
    private String address2;
    private String city;
    private String province;
    private String country;
    private String provinceCode;
    private String countryCode;
    private String addressType;
    private String zip;
    private String telephone;
    private Date created;
    private Date modified;

    public CustomerAddress() {}

    public CustomerAddress(int applicationId, int id, BigInteger customerId, String firstName, String lastName, String company, String address1, String address2, String city, String province, String country, String provinceCode, String countryCode, String addressType, String zip,
                           String telephone, Date created, Date modified) {
        this(id, customerId, firstName, lastName, company, address1, address2, city, province, country, provinceCode,
             countryCode, addressType, zip, telephone, created, modified);
        this.applicationId = applicationId;
    }

    public CustomerAddress(int id, BigInteger customerId, String firstName, String lastName, String company,
                           String address1, String address2, String city, String province, String country,
                           String provinceCode, String countryCode, String addressType, String zip, String telephone,
                           Date created,
                           Date modified) {
        this.id = id;
        this.customerId = customerId;
        this.firstName = (firstName != null && !firstName.isEmpty()) ? firstName.replaceAll("\\r", "") : firstName;
        this.lastName = (lastName != null && !lastName.isEmpty()) ? lastName.replaceAll("\\r", "") : lastName;
        this.company = company;
        this.address1 =
                (address1 != null && !address1.isEmpty()) ? address1.split("\\n")[0].replaceAll("\\r", "") : address1;
        this.address2 =
                (address2 != null && !address2.isEmpty()) ? address2.split("\\n")[0].replaceAll("\\r", "") : address2;
        this.city = city;
        this.province = province;
        this.country = country;
        this.provinceCode = provinceCode;
        this.countryCode = countryCode;
        this.addressType = addressType;
        this.zip = (zip != null && !zip.isEmpty()) ? zip.replaceAll("\\r", "") : zip;
        this.telephone = telephone;
        this.created = created;
        this.modified = modified;
    }

    public Integer getApplicationId() { return applicationId; }

    public int getId() { return id; }

    public BigInteger getCustomerId() {
        return customerId;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getCompany() { return company; }

    public String getAddress1() { return address1; }

    public String getAddress2() { return address2; }

    public String getCity() { return city; }

    public String getProvince() { return province; }

    public String getCountry() { return country; }

    public String getProvinceCode() { return provinceCode; }

    public String getCountryCode() { return countryCode; }

    public String getAddressType() { return addressType; }

    public String getZip() { return zip; }

    public String getTelephone() { return telephone; }

    public Date getCreated() { return created; }

    public Date getModified() { return modified; }

    public String toString() {
        SimpleDateFormat outputDateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        String formattedCreatedDate = (created != null) ? outputDateFormatter.format(created) : null;
        String formattedModifiedDate = (modified != null) ? outputDateFormatter.format(modified) : null;

        return String
                .format("%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s", id, RFMCalc.DELIMITER,
                        customerId, RFMCalc.DELIMITER, firstName, RFMCalc.DELIMITER, lastName, RFMCalc.DELIMITER,
                        company, RFMCalc.DELIMITER, address1, RFMCalc.DELIMITER, address2, RFMCalc.DELIMITER, city,
                        RFMCalc.DELIMITER, province, RFMCalc.DELIMITER, country, RFMCalc.DELIMITER, provinceCode,
                        RFMCalc.DELIMITER, countryCode, RFMCalc.DELIMITER, addressType, RFMCalc.DELIMITER, zip,
                        RFMCalc.DELIMITER, telephone, RFMCalc.DELIMITER, formattedCreatedDate, RFMCalc.DELIMITER,
                        formattedModifiedDate);
    }
}
