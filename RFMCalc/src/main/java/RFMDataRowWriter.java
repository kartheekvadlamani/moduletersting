import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ProtocolVersion;
import com.datastax.spark.connector.writer.RowWriter;
import scala.collection.JavaConversions;
import scala.collection.immutable.IndexedSeq;
import scala.collection.immutable.Seq;

import java.io.Serializable;
import java.util.Arrays;

/**
 * Created by brettcooper on 5/12/16.
 */
public class RFMDataRowWriter implements RowWriter<RFMData>, Serializable {
    // Return the list of column names in the Cassandra table
    public Seq<String> columnNames() {
        return toIndexedSeq(Arrays.asList("application_id", "customer_id", "customer_name", "company_name", "customer_group", "customer_city", "customer_state", "customer_country", "customer_email", "orders_sub_total", "orders_count", "first_order_date", "last_order_date",
                "average_days_between_orders", "first_order_amount", "last_order_amount", "average_order_price", "customer_created_at"));
    }

    public int estimateSizeInBytes(RFMData rfmData) {
        return 100;
    }

    public BoundStatement bind(RFMData rfmData, PreparedStatement statement, ProtocolVersion version) {
        BoundStatement boundStatement = new BoundStatement(statement);

        boundStatement.bind(rfmData.getApplicationId(), rfmData.getCustomerId(), rfmData.getCustomerName(), rfmData.getCompanyName(), rfmData.getCustomerGroup(), rfmData.getCustomerCity(), rfmData.getCustomerState(), rfmData.getCustomerCountry(), rfmData.getCustomerEmail(),
                rfmData.getOrdersSubTotal(), rfmData.getOrderCount(), rfmData.getFirstOrderDate(), rfmData.getLastOrderDate(), rfmData.getAverageDaysBetweenOrders(), rfmData.getFirstOrderAmount(), rfmData.getLastOrderAmount(), rfmData.getAverageOrderPrice(), rfmData.getCustomerCreatedAt());

        return boundStatement;
    }

    private <T> IndexedSeq<T> toIndexedSeq(Iterable<T> iterable) {
        return JavaConversions.asScalaIterable(iterable).toIndexedSeq();
    }

    public void readColumnValues(RFMData rfmData, Object[] buffer) {
        buffer[0] = rfmData.getApplicationId();
        buffer[1] = rfmData.getCustomerId();
        buffer[2] = rfmData.getCustomerName();
        buffer[3] = rfmData.getCompanyName();
        buffer[4] = rfmData.getCustomerGroup();
        buffer[5] = rfmData.getCustomerCity();
        buffer[6] = rfmData.getCustomerState();
        buffer[7] = rfmData.getCustomerCountry();
        buffer[8] = rfmData.getCustomerEmail();
        buffer[9] = rfmData.getOrdersSubTotal();
        buffer[10] = rfmData.getOrderCount();
        buffer[11] = rfmData.getFirstOrderDate();
        buffer[12] = rfmData.getLastOrderDate();
        buffer[13] = rfmData.getAverageDaysBetweenOrders();
        buffer[14] = rfmData.getFirstOrderAmount();
        buffer[15] = rfmData.getLastOrderAmount();
        buffer[16] = rfmData.getAverageOrderPrice();
        buffer[17] = rfmData.getCustomerCreatedAt();
    }
}
