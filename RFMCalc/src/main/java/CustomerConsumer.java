/**
 * Created by brettcooper on 8/24/16.
 */

import kafka.consumer.ConsumerIterator;
import kafka.consumer.KafkaStream;
import kafka.message.MessageAndMetadata;
import org.apache.avro.generic.IndexedRecord;
import org.apache.kafka.common.errors.SerializationException;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.slf4j.Logger;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.datastax.spark.connector.japi.CassandraJavaUtil.javaFunctions;

public class CustomerConsumer implements Runnable {
    private KafkaStream _kafkaStream;
    private JavaSparkContext _sparkContext;
    private Logger _logger;
    private String _cassandraSchemaName;
    private String _cassandraTableName;

    public CustomerConsumer(KafkaStream kafkaStream, JavaSparkContext sparkContext, Logger logger, String cassandraSchemaName, String cassandraTableName) {
        _kafkaStream = kafkaStream;
        _sparkContext = sparkContext;
        _logger = logger;
        _cassandraSchemaName = cassandraSchemaName;
        _cassandraTableName = cassandraTableName;
    }

    public void run() {
        ConsumerIterator it = _kafkaStream.iterator();

        while (it.hasNext()) {
            Customer customer = parseCustomerFromStream(it);

            if (customer != null) {
                List<Customer> customers = new ArrayList<Customer>();
                customers.add(customer);

                JavaRDD<Customer> customersRdd = _sparkContext.parallelize(customers);

                // Save to Cassandra
                javaFunctions(customersRdd).writerBuilder(_cassandraSchemaName, _cassandraTableName, new CustomerWriterFactory()).saveToCassandra();

                _logger.debug("customer: " + customer.toString());
            }
        }
    }

    private Customer parseCustomerFromStream(ConsumerIterator iterator) {
        try {
            MessageAndMetadata messageAndMetadata = iterator.next();
            String key = (String) messageAndMetadata.key();
            IndexedRecord value = (IndexedRecord) messageAndMetadata.message();

            BigInteger id = (value.get(0) != null) ? RFMHelpers.TryParseBigInteger(value.get(0).toString()) : null;
            Integer applicationId = (value.get(1) != null) ? RFMHelpers.TryParseInteger(value.get(1).toString()) : null;
            BigInteger customerId = (value.get(2) != null) ? RFMHelpers.TryParseBigInteger(value.get(2).toString()) : null;
            String customerGroup = (value.get(3) != null) ? value.get(3).toString() : null;
            String firstName = (value.get(4) != null) ? value.get(4).toString() : null;
            String lastName = (value.get(5) != null) ? value.get(5).toString() : null;
            String email = (value.get(6) != null) ? value.get(6).toString() : null;
            Boolean optInNewsletter = (value.get(7) != null) ? ((Integer) value.get(7) != 0) : null;
            Date createdAt = (value.get(8) != null) ? new Date((Long) value.get(8)) : null;
            Date updatedAt = (value.get(9) != null) ? new Date((Long) value.get(9)) : null;
            Boolean isSynced = (value.get(10) != null) ? ((Integer) value.get(10) != 0) : null;
            Date created = (value.get(11) != null) ? new Date((Long) value.get(11)) : null;
            Date modified = (value.get(12) != null) ? new Date((Long) value.get(12)) : null;
            Integer consumerCustomerId = (value.get(13) != null) ? RFMHelpers.TryParseInteger(value.get(13).toString()) : null;
            Boolean isPartialData = (value.get(14) != null) ? ((Integer) value.get(14) != 0) : null;

            Customer customer = new Customer(id, applicationId, customerId, customerGroup, firstName, lastName, email,
                    optInNewsletter, createdAt, updatedAt, isSynced, created, modified, consumerCustomerId, isPartialData);

            return customer;
        }
        catch (SerializationException ex) {
            _logger.error(String.format("SerializationException: %s", ex.getMessage()));
            return null;
        }
    }
}



