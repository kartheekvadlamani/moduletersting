import org.apache.commons.lang.StringUtils;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

/**
 * Created by brettcooper on 5/12/16.
 */
public class RFMHelpers {
    public static String GetFormattedDate(Date date) {
        SimpleDateFormat outputDateFormatter = new SimpleDateFormat("MM/dd/yyyy", Locale.US);
        String formattedDate = null;

        if (date != null) {
            formattedDate = outputDateFormatter.format(date);
        }

        return formattedDate;
    }

    public static String GetFormattedFullName(String firstName, String lastName) {
        return String.format("%s %s", ToPascalCase(firstName), ToPascalCase(lastName));
    }

    public static String ToPascalCase(String string) {
        String camelCaseString = null;
        String nullString = new String("null");

        if (string != null && !string.isEmpty() && !string.equals(nullString)) {
            String[] splitString = string.split("\\s+");
            StringBuilder stringBuilder = new StringBuilder();

            for (int i = 0; i < splitString.length; i++) {
                camelCaseString = StringUtils.capitalize(splitString[i].trim().toLowerCase());

                stringBuilder.append(camelCaseString);
                stringBuilder.append(" ");
            }

            return stringBuilder.toString().trim();
        }

        return null;
    }

    public static BigDecimal GetLastOrderAmount(Date createdDate1, Date createdDate2, BigDecimal createdAmount1,
                                                BigDecimal createdAmount2) {
        BigDecimal lastOrderAmount = null;

        if (createdDate1 != null && createdDate2 != null) {
            if (createdDate1.compareTo(createdDate2) > 0) {
                lastOrderAmount = createdAmount1;
            } else {
                lastOrderAmount = createdAmount2;
            }
        }

        return lastOrderAmount;
    }

    public static BigDecimal GetFirstOrderAmount(Date createdDate1, Date createdDate2, BigDecimal createdAmount1,
                                                 BigDecimal createdAmount2) {
        BigDecimal firstOrderAmount = null;

        if (createdDate1 != null && createdDate2 != null) {
            if (createdDate1.compareTo(createdDate2) < 0) {
                firstOrderAmount = createdAmount1;
            } else {
                firstOrderAmount = createdAmount2;
            }
        }

        return firstOrderAmount;
    }

    public static Integer GetAverageDaysBetweenOrders(Date firstOrderDate, Date lastOrderDate, Integer orderCount) {
        Integer averageDaysBetweenOrders = null;

        if (firstOrderDate != null && lastOrderDate != null && orderCount != null && orderCount > 1) {
            // (last order date - first order date) / (number of orders - 1)
            long daysBetweenOrders = getDifferenceDays(firstOrderDate, lastOrderDate);
            averageDaysBetweenOrders = (int) daysBetweenOrders / (orderCount - 1);
        }

        return averageDaysBetweenOrders;
    }

    private static long getDifferenceDays(Date d1, Date d2) {
        long diff = Math.abs(d2.getTime() - d1.getTime());
        return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
    }

    public static Date GetLastOrderDate(Date tuple1OrderCreatedAtDate, Date tuple2OrderCreatedAtDate) {
        Date lastOrderDate = null;

        if (tuple1OrderCreatedAtDate != null && tuple2OrderCreatedAtDate != null) {
            if (tuple1OrderCreatedAtDate.compareTo(tuple2OrderCreatedAtDate) > 0) {
                lastOrderDate = tuple1OrderCreatedAtDate;
            } else {
                lastOrderDate = tuple2OrderCreatedAtDate;
            }
        } else if (tuple1OrderCreatedAtDate != null) {
            lastOrderDate = tuple1OrderCreatedAtDate;
        } else {
            lastOrderDate = tuple2OrderCreatedAtDate;
        }

        return lastOrderDate;
    }

    public static Date GetFirstOrderDate(Date date1, Date date2) {
        Date firstOrderDate = null;

        if (date1 != null && date2 != null) {
            if (date1.compareTo(date2) < 0) {
                firstOrderDate = date1;
            } else {
                firstOrderDate = date2;
            }
        } else if (date1 != null) {
            firstOrderDate = date1;
        } else {
            firstOrderDate = date2;
        }

        return firstOrderDate;
    }

    public static BigDecimal GetTotalSum(BigDecimal total1, BigDecimal total2) {
        BigDecimal totalSum = null;

        if (total1 != null && total2 != null) {
            totalSum = total1.add(total2);
        } else if (total1 != null) {
            totalSum = total1;
        } else {
            totalSum = total2;
        }

        return totalSum;
    }

    public static CustomerAddress GetLatestCustomerAddress(CustomerAddress customerAddress1,
                                                           CustomerAddress customerAddress2) {
        CustomerAddress latestCustomerAddress = null;

        if (customerAddress1 != null && customerAddress2 != null) {
            Date customerAddress1CreatedDate = customerAddress1.getCreated();
            Date customerAddress2CreatedDate = customerAddress2.getCreated();

            if (customerAddress1CreatedDate != null && customerAddress2CreatedDate != null) {
                if (customerAddress1CreatedDate.compareTo(customerAddress2CreatedDate) > 0) {
                    latestCustomerAddress = customerAddress1;
                } else {
                    latestCustomerAddress = customerAddress2;
                }
            } else if (customerAddress1CreatedDate != null) {
                latestCustomerAddress = customerAddress1;
            } else {
                latestCustomerAddress = customerAddress2;
            }
        } else if (customerAddress1 != null) {
            latestCustomerAddress = customerAddress1;
        } else if (customerAddress2 != null) {
            latestCustomerAddress = customerAddress2;
        }

        return latestCustomerAddress;
    }

    public static Customer GetLatestCustomer(Customer customer1, Customer customer2) {
        Customer latestCustomer = null;
        Date tuple1CustomerCreatedDate = customer1.getCreated();
        Date tuple2CustomerCreatedDate = customer2.getCreated();

        if (tuple1CustomerCreatedDate != null && tuple2CustomerCreatedDate != null) {
            if (tuple1CustomerCreatedDate.compareTo(tuple2CustomerCreatedDate) > 0) {
                latestCustomer = customer1;
            } else {
                latestCustomer = customer2;
            }
        } else if (tuple1CustomerCreatedDate != null) {
            latestCustomer = customer1;
        } else {
            latestCustomer = customer2;
        }

        return latestCustomer;
    }

    public static String RemoveEscapeSequences(String line, String delimiter) {
        // This is the escape sequence used to output the data (by default backslash escape commas in the string)
        // e.g. - sqoop --escaped-by \\ ...
        String escapeSequence = String.format("\\%s", delimiter);
        String lineWithoutEscapeSequences = line;

        if (line != null && !line.isEmpty() && line.contains(escapeSequence)) {
            String regexEscapeSequence = String.format("\\\\\\%s", delimiter);

            lineWithoutEscapeSequences =
                    Pattern.compile(regexEscapeSequence).matcher(lineWithoutEscapeSequences).replaceAll("");
        }

        return lineWithoutEscapeSequences;
    }

    public static BigDecimal TryParseBigDecimal(String decimalString) {
        BigDecimal decimal = null;
        String nullString = new String("null");

        if (decimalString != null && !decimalString.isEmpty() && !decimalString.equals(nullString)) {
            try {
                decimal = new BigDecimal(decimalString);
            } catch (NumberFormatException ex) {
            }
        }

        return decimal;
    }

    public static Integer TryParseInteger(String integerValue) {
        Integer integer = null;
        String nullString = new String("null");

        if (integerValue != null && !integerValue.isEmpty() && !integerValue.equals(nullString)) {
            try {
                integer = Integer.parseInt(integerValue);
            } catch (NumberFormatException ex) {
            }
        }

        return integer;
    }

    public static BigInteger TryParseBigInteger(String bigIntValue) {
        BigInteger bigInteger = null;
        String nullString = new String("null");

        if (bigIntValue != null && !bigIntValue.isEmpty() && !bigIntValue.equals(nullString)) {
            try {
                bigInteger = new BigInteger(bigIntValue);
            } catch (Exception ex) {
            }
        }

        return bigInteger;
    }

    public static Boolean TryParseBoolean(String boolValue) {
        Boolean bool = null;
        String nullString = new String("null");


        if (boolValue != null && !boolValue.isEmpty() && !boolValue.equals(nullString)) {
            try {
                bool = Boolean.parseBoolean(boolValue);
            } catch (Exception ex) {
            }
        }

        return bool;
    }

    public static Date TryParseDate(String dateValue, SimpleDateFormat dateFormat) {
        Date date = null;
        String nullString = new String("null");

        if (dateValue != null && !dateValue.isEmpty() && !dateValue.equals(nullString)) {
            try {
                date = dateFormat.parse(dateValue);
            } catch (ParseException ex) {
            }
        }

        return date;
    }

    public static BigDecimal AvroHeapByteBufferToBigDecimal(Object object) {
        if (object != null &&
            (object.getClass() == ByteBuffer.class || object.getClass().getSuperclass() == ByteBuffer.class)) {
            ByteBuffer buffer = (ByteBuffer) object;
            byte[] bytes = buffer.array();
            BigDecimal bigDecimal = new BigDecimal(new BigInteger(bytes), 2);

            return bigDecimal;
        }

        return null;
    }
}
