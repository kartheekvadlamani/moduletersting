import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ProtocolVersion;
import com.datastax.spark.connector.writer.RowWriter;
import scala.collection.JavaConversions;
import scala.collection.immutable.IndexedSeq;
import scala.collection.immutable.Seq;

import java.io.Serializable;
import java.util.Arrays;

/**
 * Created by brettcooper on 8/25/16.
 */

public class CustomerAddressWriter implements RowWriter<CustomerAddress>, Serializable {
    // Return the list of column names in the Cassandra table
    public Seq<String> columnNames() {
        return toIndexedSeq(Arrays.asList("application_id", "id", "customer_id", "first_name", "last_name", "company", "address1", "address2", "city", "province", "country", "province_code", "country_code", "address_type", "zip", "telephone", "created", "modified"));
    }

    public int estimateSizeInBytes(CustomerAddress customerAddress) {
        return 100;
    }

    public BoundStatement bind(CustomerAddress customerAddress, PreparedStatement statement, ProtocolVersion version) {
        BoundStatement boundStatement = new BoundStatement(statement);

        boundStatement.bind(customerAddress.getApplicationId(), customerAddress.getId(), customerAddress.getCustomerId(), customerAddress.getFirstName(), customerAddress.getLastName(), customerAddress.getCompany(), customerAddress.getAddress1(), customerAddress.getAddress2(),
                customerAddress.getCity(), customerAddress.getProvince(), customerAddress.getCountry(), customerAddress.getProvinceCode(), customerAddress.getCountryCode(), customerAddress.getAddressType(), customerAddress.getZip(), customerAddress.getTelephone(), customerAddress.getCreated(),
                customerAddress.getModified());

        return boundStatement;
    }

    private <T> IndexedSeq<T> toIndexedSeq(Iterable<T> iterable) {
        return JavaConversions.asScalaIterable(iterable).toIndexedSeq();
    }

    public void readColumnValues(CustomerAddress customerAddress, Object[] buffer) {
        buffer[0] = customerAddress.getApplicationId();
        buffer[1] = customerAddress.getId();
        buffer[2] = customerAddress.getCustomerId();
        buffer[3] = customerAddress.getFirstName();
        buffer[4] = customerAddress.getLastName();
        buffer[5] = customerAddress.getCompany();
        buffer[6] = customerAddress.getAddress1();
        buffer[7] = customerAddress.getAddress2();
        buffer[8] = customerAddress.getCity();
        buffer[9] = customerAddress.getProvince();
        buffer[10] = customerAddress.getCountry();
        buffer[11] = customerAddress.getProvinceCode();
        buffer[12] = customerAddress.getCountryCode();
        buffer[13] = customerAddress.getAddressType();
        buffer[14] = customerAddress.getZip();
        buffer[15] = customerAddress.getTelephone();
        buffer[16] = customerAddress.getCreated();
        buffer[17] = customerAddress.getModified();
    }
}





