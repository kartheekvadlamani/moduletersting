import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocalFileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hdfs.DistributedFileSystem;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.slf4j.Logger;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * Created by brettcooper on 8/5/16.
 */
public class Hadoop {
    public static void DeleteNewData(String path, String hdfsPath) throws IOException {
        Configuration conf = GetConfiguration();
        FileSystem hdfs = FileSystem.get(URI.create(hdfsPath), conf);

        if (path.endsWith("*")) {
            path = path.substring(0, path.length() - 2);
        }

        if (hdfs.exists(new Path(path))) {
            hdfs.delete(new Path(path), true);
        }
    }

    public static Boolean DoesPathExist(String path, String hdfsPath, Logger logger) {
        Configuration conf = GetConfiguration();

        try {
            FileSystem hdfs = FileSystem.get(URI.create(hdfsPath), conf);

            if (path.endsWith("*")) {
                path = path.substring(0, path.length() - 2);
            }

            return hdfs.exists(new Path(path));
        }
        catch (IOException ex) {
            logger.error(ex.getMessage());
            return false;
        }
    }

    public static Configuration GetConfiguration() {
        Configuration conf = new Configuration();

        conf.set("fs.hdfs.impl", DistributedFileSystem.class.getName());
        conf.set("fs.file.impl", LocalFileSystem.class.getName());

        return conf;
    }

    public static ArrayList<Integer> GetApplicationIds(String masterDataPath, String hdfsPath, Logger logger) throws IOException {
        ArrayList<Integer> applicationIds = new ArrayList<Integer>();

        try {
            String customersPath = String.format("%s/customers", masterDataPath);

            if (DoesPathExist(customersPath, hdfsPath, logger)) {
                Configuration conf = GetConfiguration();
                FileSystem hdfs = FileSystem.get(URI.create(hdfsPath), conf);
                FileStatus[] status = hdfs.listStatus(new Path(customersPath));

                for (int i = 0; i < status.length; i++) {
                    String directoryName = status[i].getPath().getName();

                    Integer applicationId = RFMHelpers.TryParseInteger(directoryName);

                    if (applicationId != null) {
                        String customerAddressesPathByAppId = String.format("%s/customers_addresses/%s", masterDataPath, applicationId);
                        String ordersPathByAppId = String.format("%s/orders/%s", masterDataPath, applicationId);

                        if (DoesPathExist(customerAddressesPathByAppId, hdfsPath, logger) &&
                            DoesPathExist(ordersPathByAppId, hdfsPath, logger)) {
                            applicationIds.add(applicationId);
                        }
                    }
                }
            }
        }
        catch (IOException ex){
            logger.error(String.format("Getting application id's failed: %s.", ex.getMessage()));
            throw ex;
        }

        return applicationIds;
    }

    public static JavaRDD<Order> ParseOrders(String ordersPath, JavaSparkContext sc, final Logger logger) {
        return sc.textFile(ordersPath).map(
            new Function<String, Order>() {
                public Order call(String line) {
                    try {
                        String lineWithoutEscapeCharacters = RFMHelpers.RemoveEscapeSequences(line, RFMCalc.DELIMITER);
                        String[] fields = lineWithoutEscapeCharacters.split("\\" + RFMCalc.DELIMITER);
                        SimpleDateFormat inputDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);

                        if (fields.length == 28) {
                            int id = Integer.parseInt(fields[0]);
                            int applicationId = Integer.parseInt(fields[1]);
                            String orderId = (fields[2] != null && !fields[2].isEmpty()) ? fields[2].trim() : null;
                            String orderNumber = (fields[3] != null && !fields[3].isEmpty()) ? fields[3].trim() : null;
                            String status = (fields[4] != null && !fields[4].isEmpty()) ? fields[4].trim() : null;
                            String financialStatus = (fields[5] != null && !fields[5].isEmpty()) ? fields[5].trim() : null;
                            Boolean customerIsGuest = RFMHelpers.TryParseBoolean(fields[6]);
                            BigInteger customerId = RFMHelpers.TryParseBigInteger(fields[7]);
                            String firstName = (fields[8] != null && !fields[8].isEmpty()) ? fields[8].trim() : null;
                            String lastName = (fields[9] != null && !fields[9].isEmpty()) ? fields[9].trim() : null;
                            String email = (fields[10] != null && !fields[10].isEmpty()) ? fields[10].trim() : null;
                            BigDecimal subTotalPrice = RFMHelpers.TryParseBigDecimal(fields[11]);
                            BigDecimal totalDiscounts = RFMHelpers.TryParseBigDecimal(fields[12]);
                            BigDecimal storeCredit = RFMHelpers.TryParseBigDecimal(fields[13]);
                            BigDecimal totalPrice = RFMHelpers.TryParseBigDecimal(fields[14]);
                            String currencyCode = (fields[15] != null && !fields[15].isEmpty()) ? fields[15].trim() : null;
                            String source = (fields[16] != null && !fields[16].isEmpty()) ? fields[16].trim() : null;
                            Date createdAt = RFMHelpers.TryParseDate(fields[17], inputDateFormat);
                            Date updatedAt = RFMHelpers.TryParseDate(fields[18], inputDateFormat);
                            Date customerCreatedAt = RFMHelpers.TryParseDate(fields[19], inputDateFormat);
                            Boolean isSynced = RFMHelpers.TryParseBoolean(fields[20]);
                            String billingAddressId = (fields[21] != null && !fields[21].isEmpty()) ? fields[21].trim() : null;
                            String shippingAddressId = (fields[22] != null && !fields[22].isEmpty()) ? fields[22].trim() : null;
                            Date created = RFMHelpers.TryParseDate(fields[23], inputDateFormat);
                            Date modified = RFMHelpers.TryParseDate(fields[24], inputDateFormat);
                            Integer consumerOrderId = RFMHelpers.TryParseInteger(fields[25]);
                            String previousStatus = (fields[26] != null && !fields[26].isEmpty()) ? fields[26].trim() : null;
                            Boolean isPartialData = RFMHelpers.TryParseBoolean(fields[27]);

                            Order order = new Order(id, applicationId, orderId, orderNumber, status, financialStatus, customerIsGuest, customerId, firstName, lastName, email, subTotalPrice, totalDiscounts, storeCredit, totalPrice, currencyCode, source, createdAt, updatedAt, customerCreatedAt, isSynced,
                                    billingAddressId, shippingAddressId, created, modified, consumerOrderId, previousStatus, isPartialData);

                            return order;
                        }
                        else {
                            String errorMessage = String.format("Order '%s' cannot be parsed due to invalid length of %s.", line, fields.length);
                            logger.error(errorMessage);
                            return null;
                        }
                    }
                    catch (Exception ex) {
                        logger.error("Failed to parse order: " + line);
                        return null;
                    }
                }
            }
        );
    }

    public static JavaRDD<CustomerAddress> ParseCustomerAddresses(String customerAddressesPath, JavaSparkContext sc, final Logger logger) {
        return sc.textFile(customerAddressesPath).map(
            new Function<String, CustomerAddress>() {
                public CustomerAddress call(String line) throws Exception {
                    try {
                        String lineWithoutEscapeCharacters = RFMHelpers.RemoveEscapeSequences(line, RFMCalc.DELIMITER);
                        String[] fields = lineWithoutEscapeCharacters.split("\\" + RFMCalc.DELIMITER);
                        SimpleDateFormat inputDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);

                        if (fields.length == 17) {
                            int id = Integer.parseInt(fields[0]);
                            BigInteger customerId = RFMHelpers.TryParseBigInteger(fields[1]);
                            String firstName = (fields[2] != null && !fields[2].isEmpty()) ? fields[2].trim() : null;
                            String lastName = (fields[3] != null && !fields[3].isEmpty()) ? fields[3].trim() : null;
                            String company = (fields[4] != null && !fields[4].isEmpty()) ? fields[4].trim() : null;
                            String address1 = (fields[5] != null && !fields[5].isEmpty()) ? fields[5].trim() : null;
                            String address2 = (fields[6] != null && !fields[6].isEmpty()) ? fields[6].trim() : null;
                            String city = (fields[7] != null && !fields[7].isEmpty()) ? fields[7].trim() : null;
                            String province = (fields[8] != null && !fields[8].isEmpty()) ? fields[8].trim() : null;
                            String country = (fields[9] != null && !fields[9].isEmpty()) ? fields[9].trim() : null;
                            String provinceCode =
                                    (fields[10] != null && !fields[10].isEmpty()) ? fields[10].trim().toUpperCase() :
                                            null;
                            String countryCode =
                                    (fields[11] != null && !fields[11].isEmpty()) ? fields[11].trim() : null;
                            String addressType =
                                    (fields[12] != null && !fields[12].isEmpty()) ? fields[12].trim() : null;
                            String zip = (fields[13] != null && !fields[13].isEmpty()) ? fields[13].trim() : null;
                            String telephone = (fields[14] != null && !fields[14].isEmpty()) ? fields[14].trim() : null;
                            Date created = RFMHelpers.TryParseDate(fields[15], inputDateFormat);
                            Date modified = RFMHelpers.TryParseDate(fields[16], inputDateFormat);

                            return new CustomerAddress(id, customerId, firstName, lastName, company, address1, address2,
                                                       city, province, country, provinceCode, countryCode, addressType,
                                                       zip, telephone, created, modified);
                        } else if (fields.length == 18) {
                            int applicationId = Integer.parseInt(fields[0]);
                            int id = Integer.parseInt(fields[1]);
                            BigInteger customerId = RFMHelpers.TryParseBigInteger(fields[2]);
                            String firstName = (fields[3] != null && !fields[3].isEmpty()) ? fields[3].trim() : null;
                            String lastName = (fields[4] != null && !fields[4].isEmpty()) ? fields[4].trim() : null;
                            String company = (fields[5] != null && !fields[5].isEmpty()) ? fields[5].trim() : null;
                            String address1 = (fields[6] != null && !fields[6].isEmpty()) ? fields[6].trim() : null;
                            String address2 = (fields[7] != null && !fields[7].isEmpty()) ? fields[7].trim() : null;
                            String city = (fields[8] != null && !fields[8].isEmpty()) ? fields[8].trim() : null;
                            String province = (fields[9] != null && !fields[9].isEmpty()) ? fields[9].trim() : null;
                            String country = (fields[10] != null && !fields[10].isEmpty()) ? fields[10].trim() : null;
                            String provinceCode = (fields[11] != null && !fields[11].isEmpty()) ? fields[11].trim().toUpperCase() : null;
                            String countryCode = (fields[12] != null && !fields[12].isEmpty()) ? fields[12].trim() : null;
                            String addressType = (fields[13] != null && !fields[13].isEmpty()) ? fields[13].trim() : null;
                            String zip = (fields[14] != null && !fields[14].isEmpty()) ? fields[14].trim() : null;
                            String telephone = (fields[15] != null && !fields[15].isEmpty()) ? fields[15].trim() : null;
                            Date created = RFMHelpers.TryParseDate(fields[16], inputDateFormat);
                            Date modified = RFMHelpers.TryParseDate(fields[17], inputDateFormat);

                            return new CustomerAddress(applicationId, id, customerId, firstName, lastName, company,
                                                       address1, address2, city, province, country, provinceCode,
                                                       countryCode, addressType, zip, telephone, created, modified);
                        }
                        else {
                            String errorMessage = String.format("Address '%s' cannot be parsed due to invalid length of %s.", lineWithoutEscapeCharacters, fields.length);
                            logger.error(errorMessage);
                            return null;
                        }
                    }
                    catch (Exception ex) {
                        logger.error(String.format("Failed to parse customer address: '%s', error message: %s.", line, ex.getMessage()));
                        return null;
                    }
                }
            }
        );
    }

    public static JavaRDD<Customer> ParseCustomerData(String customersPath, JavaSparkContext sc, final Logger logger) {
        return sc.textFile(customersPath).map(
            new Function<String, Customer>() {
                public Customer call(String line) throws Exception {
                    try {
                        String lineWithoutEscapeCharacters = RFMHelpers.RemoveEscapeSequences(line, RFMCalc.DELIMITER);
                        String[] fields = lineWithoutEscapeCharacters.split("\\" + RFMCalc.DELIMITER);
                        SimpleDateFormat inputDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);

                        if (fields.length == 15) {
                            BigInteger id = RFMHelpers.TryParseBigInteger(fields[0]);
                            int applicationId = Integer.parseInt(fields[1]);
                            BigInteger customerId = RFMHelpers.TryParseBigInteger(fields[2]);
                            String customerGroup = (fields[3] != null && !fields[3].isEmpty()) ? fields[3].trim() : null;
                            String firstName = (fields[4] != null && !fields[4].isEmpty()) ? fields[4].trim() : null;
                            String lastName = (fields[5] != null && !fields[5].isEmpty()) ? fields[5].trim() : null;
                            String email = (fields[6] != null && !fields[6].isEmpty()) ? fields[6].trim() : null;
                            Boolean optInNewsletter = RFMHelpers.TryParseBoolean(fields[7]);
                            Date createdAt = RFMHelpers.TryParseDate(fields[8], inputDateFormat);
                            Date updatedAt = RFMHelpers.TryParseDate(fields[9], inputDateFormat);
                            Boolean isSynced = RFMHelpers.TryParseBoolean(fields[10]);
                            Date created = RFMHelpers.TryParseDate(fields[11], inputDateFormat);
                            Date modified = RFMHelpers.TryParseDate(fields[12], inputDateFormat);
                            Integer consumerCustomerId = RFMHelpers.TryParseInteger(fields[13]);
                            Boolean isPartialData = RFMHelpers.TryParseBoolean(fields[14]);

                            Customer customer = new Customer(id, applicationId, customerId, customerGroup, firstName, lastName, email, optInNewsletter, createdAt, updatedAt, isSynced, created, modified, consumerCustomerId, isPartialData);

                            return customer;
                        }
                        else {
                            String errorMessage = String.format("Customer '%s' cannot be parsed due to invalid length of %s.", lineWithoutEscapeCharacters, fields.length);
                            logger.error(errorMessage);
                            return null;
                        }
                    }
                    catch (Exception ex) {
                        logger.error("Failed to parse customer: " + line);
                        return null;
                    }
                }
            }
        );
    }
}
