import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * Created by brettcooper on 2/24/17.
 */
public class OrderPaidStatusTest {
    @Test
    public void orderPaidTest() {
        String orderPaidStatus = "paid";

        OrderPaidStatus orderPaid = OrderPaidStatus.getOrderPaidStatus(orderPaidStatus);

        assertEquals(OrderPaidStatus.PAID, orderPaid);
    }

    @Test
    public void orderPaidIgnoreCaseTest() {
        String orderPaidStatus = "Paid";

        OrderPaidStatus orderPaid = OrderPaidStatus.getOrderPaidStatus(orderPaidStatus);

        assertEquals(OrderPaidStatus.PAID, orderPaid);
    }

    @Test
    public void orderStatusNullTest() {
        String orderPaidStatus = null;

        OrderPaidStatus orderPaid = OrderPaidStatus.getOrderPaidStatus(orderPaidStatus);

        assertNull(orderPaid);
    }

    @Test
    public void orderPartialPaymentTest() {
        String orderPaidStatus = "partial payment";

        OrderPaidStatus orderPaid = OrderPaidStatus.getOrderPaidStatus(orderPaidStatus);

        assertEquals(OrderPaidStatus.PARTIAL_PAYMENT, orderPaid);
    }

    @Test
    public void orderPartialPaymentWithHyphenTest() {
        String orderPaidStatus = "partial-payment";

        OrderPaidStatus orderPaid = OrderPaidStatus.getOrderPaidStatus(orderPaidStatus);

        assertEquals(OrderPaidStatus.PARTIAL_PAYMENT, orderPaid);
    }

    @Test
    public void orderPartiallyPaidTest() {
        String orderPaidStatus = "partially_paid";

        OrderPaidStatus orderPaid = OrderPaidStatus.getOrderPaidStatus(orderPaidStatus);

        assertEquals(OrderPaidStatus.PARTIAL_PAYMENT, orderPaid);
    }
}
