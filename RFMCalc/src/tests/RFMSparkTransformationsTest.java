import org.apache.commons.lang.time.DateUtils;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.io.File;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;

/**
 * Created by brettcooper on 7/27/16.
 */
public class RFMSparkTransformationsTest {
    private JavaSparkContext _sc;
    private String _masterDataPath = ".";

    public static boolean deleteDir(File dir) {
        if (dir.isDirectory()) {
            String[] children = dir.list();

            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));

                if (!success) {
                    return false;
                }
            }
        }

        return dir.delete();
    }

    @Before
    public void setUp() throws Exception {
        String sparkMaster = "local[1]";
        String appName = getClass().getName();

        SparkConf sparkConf = new SparkConf()
                .setMaster(sparkMaster)
                .setAppName(appName);

        _sc = new JavaSparkContext(sparkConf);
    }

    @After
    public void tearDown() throws Exception {
        String customersDirectory = String.format("%s/customers", _masterDataPath);
        Path customersPath = Paths.get(customersDirectory);

        if (Files.exists(customersPath)) {
            deleteDir(new File(customersDirectory));
        }

        String customerAddressesDirectory = String.format("%s/customers_addresses", _masterDataPath);
        Path customerAddressesPath = Paths.get(customerAddressesDirectory);

        if (Files.exists(customerAddressesPath)) {
            deleteDir(new File(customerAddressesDirectory));
        }

        String ordersDirectory = String.format("%s/orders", _masterDataPath);
        Path ordersPath = Paths.get(ordersDirectory);

        if (Files.exists(ordersPath)) {
            deleteDir(new File(ordersDirectory));
        }

        if (_sc != null) {
            _sc.stop();
        }
    }

    @Test
    public void testPartitionCustomerAddressesBySingleApplication() throws Exception {
        // Arrange
        int applicationId = 1000;
        int expectedAppsCount = 1;
        Date today = Calendar.getInstance().getTime();

        List<CustomerAddress> customerAddressList = new ArrayList<CustomerAddress>();
        customerAddressList.add(new CustomerAddress(applicationId, 1, new BigInteger("1"), "john", "doe", "John's Plumbing", "123 Plumbing St.", null, "Cleveland", "OH", "US", "OH", "US", "shipping", "44125", "4404445555", today, today));
        customerAddressList.add(new CustomerAddress(applicationId, 2, new BigInteger("2"), "Jane", "doe", "Jane's Business", "456 Another St.", null, "Atlanta", "GA", "US", "GA", "US", "shipping", "55555", "8005882300", today, today));
        customerAddressList.add(new CustomerAddress(applicationId, 3, new BigInteger("3"), "timmy", "doe", "unemployed", "89 Parkway Ave.", null, "New York", "NY", "US", "NY", "US", "shipping", "44125", "8003338888", today, today));

        JavaRDD<CustomerAddress> customerAddressesRdd = _sc.parallelize(customerAddressList);
        String currentDateTime = RFMSparkTransformations.getCurrentDateTimePath();

        // Act
        int keysCount = RFMSparkTransformations
                .partitionCustomerAddressesByApplication(_sc, customerAddressesRdd, currentDateTime, _masterDataPath);

        // Assert
        assertEquals(expectedAppsCount, keysCount);
    }

    @Test
    public void testPartitionCustomerAddressesByMultipleApplications() throws Exception {
        // Arrange
        int applicationId1 = 1000;
        int applicationId2 = 1001;
        int expectedAppsCount = 2;
        Date today = Calendar.getInstance().getTime();

        List<CustomerAddress> customerAddressList = new ArrayList<CustomerAddress>();
        customerAddressList.add(new CustomerAddress(applicationId1, 1, new BigInteger("1"), "john", "doe", "John's Plumbing", "123 Plumbing St.", null, "Cleveland", "OH", "US", "OH", "US", "shipping", "44125", "4404445555", today, today));
        customerAddressList.add(new CustomerAddress(applicationId1, 2, new BigInteger("2"), "Jane", "doe", "Jane's Business", "456 Another St.", null, "Atlanta", "GA", "US", "GA", "US", "shipping", "55555", "8005882300", today, today));
        customerAddressList.add(new CustomerAddress(applicationId2, 3, new BigInteger("3"), "timmy", "doe", "unemployed", "89 Parkway Ave.", null, "New York", "NY", "US", "NY", "US", "shipping", "44125", "8003338888", today, today));

        JavaRDD<CustomerAddress> customerAddressesRdd = _sc.parallelize(customerAddressList);
        String currentDateTime = RFMSparkTransformations.getCurrentDateTimePath();
        String masterDataPath = ".";

        // Act
        int keysCount = RFMSparkTransformations
                .partitionCustomerAddressesByApplication(_sc, customerAddressesRdd, currentDateTime, _masterDataPath);

        // Assert
        assertEquals(expectedAppsCount, keysCount);
    }

    @Test
    public void testPartitionCustomersBySingleApplication() throws Exception {
        // Arrange
        int applicationId = 1000;
        int expectedAppsCount = 1;
        Date today = Calendar.getInstance().getTime();

        List<Customer> customers = new ArrayList<Customer>();
        customers.add(new Customer(new BigInteger("1"), applicationId, new BigInteger("1"), null, "Test", "Customer", "test.customer@gmail.com", false, today, today, true, today, today, 1, false));
        customers.add(new Customer(new BigInteger("2"), applicationId, new BigInteger("2"), null, "Test2", "Customer2", "test.customer2@gmail.com", false, today, today, true, today, today, 2, false));
        customers.add(new Customer(new BigInteger("3"), applicationId, new BigInteger("3"), null, "Test3", "Customer3", "test.customer3@gmail.com", false, today, today, true, today, today, 3, false));

        JavaRDD<Customer> customersRdd = _sc.parallelize(customers);
        String currentDateTime = RFMSparkTransformations.getCurrentDateTimePath();
        String masterDataPath = ".";

        // Act
        int keysCount = RFMSparkTransformations
                .partitionCustomersByApplication(_sc, customersRdd, currentDateTime, _masterDataPath);

        // Assert
        assertEquals(expectedAppsCount, keysCount);
    }

    @Test
    public void testPartitionCustomersByMultipleApplications() throws Exception {
        // Arrange
        int applicationId1 = 1000;
        int applicationId2 = 1001;
        int expectedAppsCount = 2;
        Date today = Calendar.getInstance().getTime();

        List<Customer> customers = new ArrayList<Customer>();
        customers.add(new Customer(new BigInteger("1"), applicationId1, new BigInteger("1"), null, "Test", "Customer", "test.customer@gmail.com", false, today, today, true, today, today, 1, false));
        customers.add(new Customer(new BigInteger("2"), applicationId2, new BigInteger("2"), null, "Test2", "Customer2", "test.customer2@gmail.com", false, today, today, true, today, today, 2, false));
        customers.add(new Customer(new BigInteger("3"), applicationId1, new BigInteger("3"), null, "Test3", "Customer3", "test.customer3@gmail.com", false, today, today, true, today, today, 3, false));

        JavaRDD<Customer> customersRdd = _sc.parallelize(customers);
        String currentDateTime = RFMSparkTransformations.getCurrentDateTimePath();
        String masterDataPath = ".";

        // Act
        int keysCount = RFMSparkTransformations
                .partitionCustomersByApplication(_sc, customersRdd, currentDateTime, _masterDataPath);

        // Assert
        assertEquals(expectedAppsCount, keysCount);
    }

    @Test
    public void testPartitionOrdersBySingleApplication() throws Exception {
        // Arrange
        int applicationId = 1000;
        int expectedAppsCount = 1;
        Date today = Calendar.getInstance().getTime();

        List<Order> orders = new ArrayList<Order>();
        orders.add(new Order(1, applicationId, "1", "1", "new", null, false, new BigInteger("1"), "Test", "Customer", "test.customer@gmail.com", new BigDecimal("10"), null, null, new BigDecimal("10"), "USD", null, today, today, today, true, null, null, today, today, 1, null, false));
        orders.add(new Order(2, applicationId, "2", "1", "new", null, false, new BigInteger("2"), "Test", "Customer", "test.customer@gmail.com", new BigDecimal("10"), null, null, new BigDecimal("10"), "USD", null, today, today, today, true, null, null, today, today, 2, null, false));
        orders.add(new Order(3, applicationId, "3", "1", "new", null, false, new BigInteger("3"), "Test", "Customer", "test.customer@gmail.com", new BigDecimal("10"), null, null, new BigDecimal("10"), "USD", null, today, today, today, true, null, null, today, today, 3, null, false));

        JavaRDD<Order> ordersRdd = _sc.parallelize(orders);
        String currentDateTime = RFMSparkTransformations.getCurrentDateTimePath();
        String masterDataPath = ".";

        // Act
        int keysCount =
                RFMSparkTransformations.partitionOrdersByApplication(_sc, ordersRdd, currentDateTime, _masterDataPath);

        // Assert
        assertEquals(expectedAppsCount, keysCount);
    }

    @Test
    public void testPartitionOrdersByMultipleApplications() throws Exception {
        // Arrange
        int applicationId1 = 1000;
        int applicationId2 = 1001;
        int expectedAppsCount = 2;
        Date today = Calendar.getInstance().getTime();

        List<Order> orders = new ArrayList<Order>();
        orders.add(new Order(1, applicationId1, "1", "1", "new", null, false, new BigInteger("1"), "Test", "Customer", "test.customer@gmail.com", new BigDecimal("10"), null, null, new BigDecimal("10"), "USD", null, today, today, today, true, null, null, today, today, 1, null, false));
        orders.add(new Order(2, applicationId2, "2", "1", "new", null, false, new BigInteger("2"), "Test", "Customer", "test.customer@gmail.com", new BigDecimal("10"), null, null, new BigDecimal("10"), "USD", null, today, today, today, true, null, null, today, today, 2, null, false));
        orders.add(new Order(3, applicationId1, "3", "1", "new", null, false, new BigInteger("3"), "Test", "Customer", "test.customer@gmail.com", new BigDecimal("10"), null, null, new BigDecimal("10"), "USD", null, today, today, today, true, null, null, today, today, 3, null, false));

        JavaRDD<Order> ordersRdd = _sc.parallelize(orders);
        String currentDateTime = RFMSparkTransformations.getCurrentDateTimePath();
        String masterDataPath = ".";

        // Act
        int keysCount =
                RFMSparkTransformations.partitionOrdersByApplication(_sc, ordersRdd, currentDateTime, _masterDataPath);

        // Assert
        assertEquals(expectedAppsCount, keysCount);
    }

    @Test
    public void testGetRfmDataJavaRDDSingleApplication() throws Exception {
        // Arrange
        int applicationId = 1000;
        Date today = Calendar.getInstance().getTime();
        Date yesterday = DateUtils.addDays(today, -1);
        List<Customer> customers = new ArrayList<Customer>();

        customers.add(new Customer(new BigInteger("1"), applicationId, new BigInteger("1"), null, "Test", "Customer", "test.customer@gmail.com", false, today, today, true, today, today, 1, false));
        customers.add(new Customer(new BigInteger("2"), applicationId, new BigInteger("2"), null, "Test2", "Customer2", "test.customer2@gmail.com", false, today, today, true, today, today, 2, false));
        customers.add(new Customer(new BigInteger("3"), applicationId, new BigInteger("3"), null, "Test3", "Customer3", "test.customer3@gmail.com", false, today, today, true, today, today, 3, false));

        List<CustomerAddress> customerAddresses = new ArrayList<CustomerAddress>();

        customerAddresses.add(new CustomerAddress(applicationId, 1, new BigInteger("1"), "Test", "Customer", "John's Plumbing", "123 Plumbing St.", null, "Cleveland", "OH", "US", "OH", "US", "shipping", "44125", "4404445555", today, today));
        customerAddresses.add(new CustomerAddress(applicationId, 2, new BigInteger("2"), "Test2", "Customer2", "Jane's Business", "456 Another St.", null, "Atlanta", "GA", "US", "GA", "US", "shipping", "55555", "8005882300", today, today));
        customerAddresses.add(new CustomerAddress(applicationId, 3, new BigInteger("3"), "Test3", "Customer3", "unemployed", "89 Parkway Ave.", null, "New York", "NY", "US", "NY", "US", "shipping", "44125", "8003338888", today, today));

        List<Order> orders = new ArrayList<Order>();

        orders.add(new Order(1, applicationId, "1", "1", "new", "paid", false, new BigInteger("1"), "Test", "Customer",
                             "test.customer@gmail.com", new BigDecimal("10"), null, null, new BigDecimal("10"), "USD",
                             null, yesterday, yesterday, yesterday, true, null, null, yesterday, yesterday, 1, null,
                             false));
        orders.add(
                new Order(2, applicationId, "2", "1", "new", "paid", false, new BigInteger("2"), "Test2", "Customer2",
                          "test.customer@gmail.com", new BigDecimal("20"), null, null, new BigDecimal("20"), "USD",
                          null, today, today, today, true, null, null, today, today, 2, null, false));
        orders.add(
                new Order(3, applicationId, "3", "1", "new", "paid", false, new BigInteger("3"), "Test3", "Customer3",
                          "test.customer@gmail.com", new BigDecimal("30"), null, null, new BigDecimal("30"), "USD",
                          null, today, today, today, true, null, null, today, today, 3, null, false));

        JavaRDD<Customer> customersData = _sc.parallelize(customers);
        JavaRDD<CustomerAddress> customerAddressesData = _sc.parallelize(customerAddresses);
        JavaRDD<Order> ordersData = _sc.parallelize(orders);

        // Act
        JavaRDD<RFMData> rfmDataJavaRDD = RFMSparkTransformations
                .getRfmDataJavaRDD(applicationId, customersData, customerAddressesData, ordersData);
        List<RFMData> rfmData = rfmDataJavaRDD.collect();

        // Assert
        Customer customer1 = customers.get(0);
        Customer customer2 = customers.get(1);
        Customer customer3 = customers.get(2);
        CustomerAddress customerAddress1 = customerAddresses.get(0);
        CustomerAddress customerAddress2 = customerAddresses.get(1);
        CustomerAddress customerAddress3 = customerAddresses.get(2);
        Order order1 = orders.get(0);
        Order order2 = orders.get(1);
        Order order3 = orders.get(2);
        RFMData customer1rfmData = rfmData.get(0);
        RFMData customer2rfmData = rfmData.get(2);
        RFMData customer3rfmData = rfmData.get(1);

        List<Order> orders1List = new ArrayList<Order>();
        orders1List.add(order1);

        List<Order> orders2List = new ArrayList<Order>();
        orders2List.add(order2);

        List<Order> orders3List = new ArrayList<Order>();
        orders3List.add(order3);

        verifyRfmData(customer1, customerAddress1, orders1List, customer1rfmData);
        verifyRfmData(customer2, customerAddress2, orders2List, customer2rfmData);
        verifyRfmData(customer3, customerAddress3, orders3List, customer3rfmData);
    }

    @Test
    public void testGetRfmDataJavaRDDSingleApplicationMultipleOrders() throws Exception {
        // Arrange
        int applicationId = 1000;
        Date today = Calendar.getInstance().getTime();
        Date yesterday = DateUtils.addDays(today, -1);
        Date twoDaysAgo = DateUtils.addDays(today, -1);
        Date threeDaysAgo = DateUtils.addDays(today, -1);
        List<Customer> customers = new ArrayList<Customer>();

        customers.add(new Customer(new BigInteger("1"), applicationId, new BigInteger("1"), null, "Test", "Customer", "test.customer@gmail.com", false, today, today, true, today, today, 1, false));
        customers.add(new Customer(new BigInteger("2"), applicationId, new BigInteger("2"), null, "Test2", "Customer2", "test.customer2@gmail.com", false, today, today, true, today, today, 2, false));
        customers.add(new Customer(new BigInteger("3"), applicationId, new BigInteger("3"), null, "Test3", "Customer3", "test.customer3@gmail.com", false, today, today, true, today, today, 3, false));

        List<CustomerAddress> customerAddresses = new ArrayList<CustomerAddress>();

        customerAddresses.add(new CustomerAddress(applicationId, 1, new BigInteger("1"), "john", "doe", "John's Plumbing", "123 Plumbing St.", null, "Cleveland", "OH", "US", "OH", "US", "shipping", "44125", "4404445555", today, today));
        customerAddresses.add(new CustomerAddress(applicationId, 2, new BigInteger("2"), "Jane", "doe", "Jane's Business", "456 Another St.", null, "Atlanta", "GA", "US", "GA", "US", "shipping", "55555", "8005882300", today, today));
        customerAddresses.add(new CustomerAddress(applicationId, 3, new BigInteger("3"), "timmy", "doe", "unemployed", "89 Parkway Ave.", null, "New York", "NY", "US", "NY", "US", "shipping", "44125", "8003338888", today, today));

        List<Order> orders = new ArrayList<Order>();

        orders.add(new Order(1, applicationId, "1", "1", "new", "paid", false, new BigInteger("1"), "Test", "Customer",
                             "test.customer@gmail.com", new BigDecimal("10"), null, null, new BigDecimal("10"), "USD",
                             null, today, today, today, true, null, null, today, today, 1, null, false));
        orders.add(new Order(2, applicationId, "2", "2", "new", "paid", false, new BigInteger("1"), "Test", "Customer",
                             "test.customer@gmail.com", new BigDecimal("15"), null, null, new BigDecimal("15"), "USD",
                             null, yesterday, yesterday, yesterday, true, null, null, yesterday, yesterday, 1, null,
                             false));
        orders.add(new Order(3, applicationId, "3", "3", "new", "paid", false, new BigInteger("1"), "Test", "Customer",
                             "test.customer@gmail.com", new BigDecimal("20"), null, null, new BigDecimal("20"), "USD",
                             null, twoDaysAgo, twoDaysAgo, twoDaysAgo, true, null, null, twoDaysAgo, twoDaysAgo, 1,
                             null, false));

        orders.add(
                new Order(4, applicationId, "4", "4", "new", "paid", false, new BigInteger("2"), "Test2", "Customer2",
                          "test.customer2@gmail.com", new BigDecimal("100"), null, null, new BigDecimal("100"), "USD",
                          null, today, today, today, true, null, null, today, today, 2, null, false));
        orders.add(
                new Order(5, applicationId, "5", "5", "new", "paid", false, new BigInteger("2"), "Test2", "Customer2",
                          "test.customer2@gmail.com", new BigDecimal("200"), null, null, new BigDecimal("200"), "USD",
                          null, twoDaysAgo, twoDaysAgo, twoDaysAgo, true, null, null, twoDaysAgo, twoDaysAgo, 2, null,
                          false));
        orders.add(
                new Order(6, applicationId, "6", "6", "new", "paid", false, new BigInteger("2"), "Test2", "Customer2",
                          "test.customer2@gmail.com", new BigDecimal("300"), null, null, new BigDecimal("300"), "USD",
                          null, threeDaysAgo, threeDaysAgo, threeDaysAgo, true, null, null, threeDaysAgo, threeDaysAgo,
                          2, null, false));

        orders.add(
                new Order(7, applicationId, "7", "7", "new", "paid", false, new BigInteger("3"), "Test3", "Customer3",
                          "test.customer3@gmail.com", new BigDecimal("30"), null, null, new BigDecimal("30"), "USD",
                          null, today, today, today, true, null, null, today, today, 3, null, false));
        orders.add(
                new Order(8, applicationId, "8", "8", "new", "paid", false, new BigInteger("3"), "Test3", "Customer3",
                          "test.customer3@gmail.com", new BigDecimal("30"), null, null, new BigDecimal("30"), "USD",
                          null, today, today, today, true, null, null, today, today, 3, null, false));
        orders.add(
                new Order(9, applicationId, "9", "9", "new", "paid", false, new BigInteger("3"), "Test3", "Customer3",
                          "test.customer3@gmail.com", new BigDecimal("30"), null, null, new BigDecimal("30"), "USD",
                          null, today, today, today, true, null, null, today, today, 3, null, false));

        JavaRDD<Customer> customersData = _sc.parallelize(customers);
        JavaRDD<CustomerAddress> customerAddressesData = _sc.parallelize(customerAddresses);
        JavaRDD<Order> ordersData = _sc.parallelize(orders);

        // Act
        JavaRDD<RFMData> rfmDataJavaRDD = RFMSparkTransformations
                .getRfmDataJavaRDD(applicationId, customersData, customerAddressesData, ordersData);
        List<RFMData> rfmData = rfmDataJavaRDD.collect();

        // Assert
        Customer customer1 = customers.get(0);
        Customer customer2 = customers.get(1);
        Customer customer3 = customers.get(2);
        CustomerAddress customerAddress1 = customerAddresses.get(0);
        CustomerAddress customerAddress2 = customerAddresses.get(1);
        CustomerAddress customerAddress3 = customerAddresses.get(2);
        Order order1 = orders.get(0);
        Order order2 = orders.get(1);
        Order order3 = orders.get(2);
        RFMData customer1rfmData = rfmData.get(0);
        RFMData customer2rfmData = rfmData.get(2);
        RFMData customer3rfmData = rfmData.get(1);

        verifyRfmData(customer1, customerAddress1, orders.subList(0, 3), customer1rfmData);
        verifyRfmData(customer2, customerAddress2, orders.subList(3, 6), customer2rfmData);
        verifyRfmData(customer3, customerAddress3, orders.subList(6, 9), customer3rfmData);
    }

    @Test
    public void testCalculateSummaryStatistics() {
        // Arrange
        Integer applicationId = 1000;
        Date today = Calendar.getInstance().getTime();
        Date yesterday = DateUtils.addDays(today, -1);
        Date twoDaysAgo = DateUtils.addDays(today, -1);
        Date threeDaysAgo = DateUtils.addDays(today, -1);
        Date aWeekAgo = DateUtils.addDays(today, -7);
        List<RFMData> rfmDataList = new ArrayList<RFMData>();
        JavaRDD<RFMData> rfmDataJavaRDD = null;

        rfmDataList.add(new RFMData(applicationId, new BigInteger("1"), "Test Customer", "Test Company", null, "Cleveland", "OH", "US", "test.customer@gmail.com", new BigDecimal("100"), 1, today, today, 0, new BigDecimal("100"), new BigDecimal("100"), new BigDecimal("100"), today));
        rfmDataList.add(new RFMData(applicationId, new BigInteger("2"), "Test Customer2", "Test Company2", null, "Cleveland", "OH", "US", "test2.customer@gmail.com", new BigDecimal("500"), 5, aWeekAgo, yesterday, 5, new BigDecimal("5"), new BigDecimal("50"), new BigDecimal("100"), aWeekAgo));
        rfmDataList.add(new RFMData(applicationId, new BigInteger("3"), "Test Customer3", "Test Company3", null, "Cleveland", "OH", "US", "test3.customer@gmail.com", new BigDecimal("23.95"), 2, threeDaysAgo, twoDaysAgo, 2, new BigDecimal("19.95"), new BigDecimal("21.00"), new BigDecimal("35.33"), threeDaysAgo));
        rfmDataList.add(new RFMData(applicationId, new BigInteger("4"), "Test Customer4", "Test Company4", null, "Cleveland", "OH", "US", "test4.customer@gmail.com", new BigDecimal("5.00"), 1, today, today, 0, new BigDecimal("5.00"), new BigDecimal("5.00"), new BigDecimal("5.00"), today));
        rfmDataList.add(new RFMData(applicationId, new BigInteger("5"), "Test Customer5", "Test Company5", null, "Cleveland", "OH", "US", "test5.customer@gmail.com", new BigDecimal("1000"), 10, aWeekAgo, threeDaysAgo, 3, new BigDecimal("100"), new BigDecimal("3.33"), new BigDecimal("100"), aWeekAgo));

        rfmDataJavaRDD = _sc.parallelize(rfmDataList);

        // Act
        JavaRDD<RFMSummaryStatistics> rfmSummaryStatisticsJavaRDD =
                RFMSparkTransformations.calculateSummaryStatistics(_sc, applicationId, rfmDataJavaRDD, rfmDataJavaRDD);

        // Assert
        List<RFMSummaryStatistics> rfmSummaryStatisticsList = rfmSummaryStatisticsJavaRDD.collect();
        RFMSummaryStatistics rfmSummaryStatistics = rfmSummaryStatisticsList.get(0);

        verifySummaryStatistics(applicationId, rfmDataList, rfmSummaryStatistics, rfmSummaryStatistics);
    }

    @Test
    public void testCalculateSummaryStatisticsTop25Percent() {
        // Arrange
        Double top25Percent = .25;
        Integer applicationId = 1000;
        Date today = Calendar.getInstance().getTime();
        Date yesterday = DateUtils.addDays(today, -1);
        Date twoDaysAgo = DateUtils.addDays(today, -1);
        Date threeDaysAgo = DateUtils.addDays(today, -1);
        Date aWeekAgo = DateUtils.addDays(today, -7);
        List<RFMData> rfmDataList = new ArrayList<RFMData>();
        JavaRDD<RFMData> rfmDataJavaRDD = null;

        rfmDataList.add(new RFMData(applicationId, new BigInteger("1"), "Test Customer", "Test Company", null, "Cleveland", "OH", "US", "test.customer@gmail.com", new BigDecimal("100"), 1, today, today, 0, new BigDecimal("100"), new BigDecimal("100"), new BigDecimal("100"), today));
        rfmDataList.add(new RFMData(applicationId, new BigInteger("2"), "Test Customer2", "Test Company2", null, "Cleveland", "OH", "US", "test2.customer@gmail.com", new BigDecimal("500"), 5, aWeekAgo, yesterday, 5, new BigDecimal("5"), new BigDecimal("50"), new BigDecimal("100"), aWeekAgo));
        rfmDataList.add(new RFMData(applicationId, new BigInteger("3"), "Test Customer3", "Test Company3", null, "Cleveland", "OH", "US", "test3.customer@gmail.com", new BigDecimal("23.95"), 2, threeDaysAgo, twoDaysAgo, 2, new BigDecimal("19.95"), new BigDecimal("21.00"), new BigDecimal("35.33"), threeDaysAgo));
        rfmDataList.add(new RFMData(applicationId, new BigInteger("4"), "Test Customer4", "Test Company4", null, "Cleveland", "OH", "US", "test4.customer@gmail.com", new BigDecimal("5.00"), 1, today, today, 0, new BigDecimal("5.00"), new BigDecimal("5.00"), new BigDecimal("5.00"), today));
        rfmDataList.add(new RFMData(applicationId, new BigInteger("5"), "Test Customer5", "Test Company5", null, "Cleveland", "OH", "US", "test5.customer@gmail.com", new BigDecimal("1000"), 10, aWeekAgo, threeDaysAgo, 3, new BigDecimal("100"), new BigDecimal("3.33"), new BigDecimal("100"), aWeekAgo));

        rfmDataJavaRDD = _sc.parallelize(rfmDataList);

        JavaRDD<RFMSummaryStatistics> rfmSummaryStatisticsJavaRDD =
                RFMSparkTransformations.calculateSummaryStatistics(_sc, applicationId, rfmDataJavaRDD, rfmDataJavaRDD);
        JavaRDD<RFMSummaryStatistics> rfmSummaryStatisticsTop25PercentJavaRDD = RFMSparkTransformations
                .calculateSummaryStatisticsTopXPercent(_sc, applicationId, rfmDataJavaRDD, top25Percent);
        RFMSummaryStatistics rfmTop25PercentSummaryStatistics1 = rfmSummaryStatisticsTop25PercentJavaRDD.first();
        RFMSummaryStatistics rfmSummaryStatistics = rfmSummaryStatisticsJavaRDD.first();

        rfmDataList.sort(new Comparator<RFMData>() {
            @Override
            public int compare(RFMData o1, RFMData o2) {
                return o2.getOrdersSubTotal().compareTo(o1.getOrdersSubTotal());
            }
        });

        Integer finalIndex = new BigDecimal(rfmDataList.size()).multiply(new BigDecimal(top25Percent)).intValue();
        List<RFMData> rfmDataTopXPercentList = rfmDataList.subList(0, finalIndex);

        verifySummaryStatistics(applicationId, rfmDataTopXPercentList, rfmTop25PercentSummaryStatistics1, rfmSummaryStatistics);
    }

    @Test
    public void testCalculateSummaryStatisticsBottom50Percent() {
        // Arrange
        Double bottom50Percent = .50;
        Integer applicationId = 1000;
        Date today = Calendar.getInstance().getTime();
        Date yesterday = DateUtils.addDays(today, -1);
        Date twoDaysAgo = DateUtils.addDays(today, -1);
        Date threeDaysAgo = DateUtils.addDays(today, -1);
        Date aWeekAgo = DateUtils.addDays(today, -7);
        List<RFMData> rfmDataList = new ArrayList<RFMData>();
        JavaRDD<RFMData> rfmDataJavaRDD = null;

        rfmDataList.add(new RFMData(applicationId, new BigInteger("1"), "Test Customer", "Test Company", null, "Cleveland", "OH", "US", "test.customer@gmail.com", new BigDecimal("100"), 1, today, today, 0, new BigDecimal("100"), new BigDecimal("100"), new BigDecimal("100"), today));
        rfmDataList.add(new RFMData(applicationId, new BigInteger("2"), "Test Customer2", "Test Company2", null, "Cleveland", "OH", "US", "test2.customer@gmail.com", new BigDecimal("500"), 5, aWeekAgo, yesterday, 5, new BigDecimal("5"), new BigDecimal("50"), new BigDecimal("100"), aWeekAgo));
        rfmDataList.add(new RFMData(applicationId, new BigInteger("3"), "Test Customer3", "Test Company3", null, "Cleveland", "OH", "US", "test3.customer@gmail.com", new BigDecimal("23.95"), 2, threeDaysAgo, twoDaysAgo, 2, new BigDecimal("19.95"), new BigDecimal("21.00"), new BigDecimal("35.33"), threeDaysAgo));
        rfmDataList.add(new RFMData(applicationId, new BigInteger("4"), "Test Customer4", "Test Company4", null, "Cleveland", "OH", "US", "test4.customer@gmail.com", new BigDecimal("5.00"), 1, today, today, 0, new BigDecimal("5.00"), new BigDecimal("5.00"), new BigDecimal("5.00"), today));
        rfmDataList.add(new RFMData(applicationId, new BigInteger("5"), "Test Customer5", "Test Company5", null, "Cleveland", "OH", "US", "test5.customer@gmail.com", new BigDecimal("1000"), 10, aWeekAgo, threeDaysAgo, 3, new BigDecimal("100"), new BigDecimal("3.33"), new BigDecimal("100"), aWeekAgo));

        rfmDataJavaRDD = _sc.parallelize(rfmDataList);

        JavaRDD<RFMSummaryStatistics> rfmSummaryStatisticsJavaRDD =
                RFMSparkTransformations.calculateSummaryStatistics(_sc, applicationId, rfmDataJavaRDD, rfmDataJavaRDD);
        JavaRDD<RFMSummaryStatistics> rfmSummaryStatisticsBottom50PercentJavaRDD = RFMSparkTransformations
                .calculateSummaryStatisticsBottomXPercent(_sc, applicationId, rfmDataJavaRDD, bottom50Percent);
        RFMSummaryStatistics rfmTop25PercentSummaryStatistics1 = rfmSummaryStatisticsBottom50PercentJavaRDD.first();
        RFMSummaryStatistics rfmSummaryStatistics = rfmSummaryStatisticsJavaRDD.first();

        rfmDataList.sort(new Comparator<RFMData>() {
            @Override
            public int compare(RFMData o1, RFMData o2) {
                return o2.getOrdersSubTotal().compareTo(o1.getOrdersSubTotal());
            }
        });

        Integer finalIndex = ((Double)(rfmDataList.size() * bottom50Percent)).intValue();
        //final Integer topXPercentOfCustomers = ((Double) (topXPercent * topCustomerCount)).intValue();
        List<RFMData> rfmDataTopXPercentList = rfmDataList.subList(finalIndex, rfmDataList.size());

        verifySummaryStatistics(applicationId, rfmDataTopXPercentList, rfmTop25PercentSummaryStatistics1, rfmSummaryStatistics);
    }

    private void verifySummaryStatistics(Integer applicationId, List<RFMData> rfmDataList, RFMSummaryStatistics rfmSummaryStatistics1, RFMSummaryStatistics rfmSummaryStatistics2) {
        long totalCustomers = rfmDataList.size();
        BigDecimal totalAmountOfOrders = new BigDecimal("0").setScale(2);
        Integer orderCount = 0;
        Integer totalAverageDaysBetweenOrders = 0;
        Integer numberOfRepeatBuyers = 0;
        BigDecimal revenueFromRepeatBuyers = new BigDecimal("0");

        for (RFMData rfmData : rfmDataList) {
            totalAmountOfOrders = totalAmountOfOrders.add(rfmData.getOrdersSubTotal());
            orderCount += rfmData.getOrderCount();

            if (rfmData.getOrderCount() > 1) {
                numberOfRepeatBuyers++;
                revenueFromRepeatBuyers = revenueFromRepeatBuyers.add(rfmData.getOrdersSubTotal());
            }

            totalAverageDaysBetweenOrders += rfmData.getAverageDaysBetweenOrders();
        }

        BigDecimal averageOrderValue = totalAmountOfOrders.divide(new BigDecimal(orderCount), 2, BigDecimal.ROUND_HALF_UP);
        BigDecimal averageCustomerLifetimeValue = totalAmountOfOrders.divide(new BigDecimal(totalCustomers), 2, BigDecimal.ROUND_HALF_UP);
        BigDecimal averageNumberOfOrders = new BigDecimal(orderCount).divide(new BigDecimal(totalCustomers), 2, BigDecimal.ROUND_HALF_UP);
        Integer averageDaysSinceLastOrder = new BigDecimal(totalAverageDaysBetweenOrders).divide(new BigDecimal(totalCustomers), 0, BigDecimal.ROUND_HALF_UP).intValue();
        BigDecimal percentageOfRepeatBuyers = new BigDecimal(numberOfRepeatBuyers).divide(new BigDecimal(totalCustomers), 2, BigDecimal.ROUND_HALF_UP);
        BigDecimal percentageRevenueFromRepeatBuyers = revenueFromRepeatBuyers.divide(totalAmountOfOrders, 2, BigDecimal.ROUND_HALF_UP);
        BigDecimal percentageTotalAmountOfOrders = null;

        if (rfmSummaryStatistics1.getTotalAmountOfOrders().compareTo(new BigDecimal(0)) > 0) {
            percentageTotalAmountOfOrders = totalAmountOfOrders.divide(rfmSummaryStatistics2.getTotalAmountOfOrders(), 2, BigDecimal.ROUND_HALF_UP);
        }

        BigDecimal percentageTotalNumberOfOrders = null;

        if (rfmSummaryStatistics1.getTotalNumberOfOrders() > 0) {
            percentageTotalNumberOfOrders = new BigDecimal(orderCount).divide(new BigDecimal(rfmSummaryStatistics1.getTotalNumberOfOrders()), 2, BigDecimal.ROUND_HALF_UP);
        }

        assertEquals(applicationId, rfmSummaryStatistics1.getApplicationId());
        assertEquals(totalCustomers, rfmSummaryStatistics1.getTotalCustomers());
        assertEquals(totalAmountOfOrders, rfmSummaryStatistics1.getTotalAmountOfOrders());
        assertEquals(percentageTotalAmountOfOrders, rfmSummaryStatistics1.getPercentageTotalAmountOfOrders());
        assertEquals(averageOrderValue, rfmSummaryStatistics1.getAverageOrderValue());
        assertEquals(averageCustomerLifetimeValue, rfmSummaryStatistics1.getAverageCustomerLifetimeValue());
        assertEquals(orderCount, rfmSummaryStatistics1.getTotalNumberOfOrders());
        assertEquals(percentageTotalNumberOfOrders, rfmSummaryStatistics2.getPercentageTotalNumberOfOrders());
        assertEquals(averageNumberOfOrders, rfmSummaryStatistics1.getAverageNumberOfOrders());
        assertEquals(percentageOfRepeatBuyers, rfmSummaryStatistics1.getPercentageOfRepeatBuyers());
        assertEquals(percentageRevenueFromRepeatBuyers, rfmSummaryStatistics1.getPercentageRevenueFromRepeatBuyers());
        assertEquals(averageDaysSinceLastOrder, rfmSummaryStatistics1.getAverageDaysSinceLastOrder());
    }

    private void verifyRfmData(Customer customer, CustomerAddress customerAddress, List<Order> orders, RFMData customerRfmData) {
        BigDecimal sumOfAllOrders = new BigDecimal("0");
        Date firstOrderDate = null;
        Date lastOrderDate = null;
        Integer orderCount = 0;
        BigDecimal firstOrderAmount = null;
        BigDecimal lastOrderAmount = null;

        for (Order order : orders) {
            sumOfAllOrders = sumOfAllOrders.add(order.getTotalPrice());

            if (firstOrderDate == null) {
                firstOrderDate = order.getCreated();
                firstOrderAmount = order.getTotalPrice();
            }
            else if (order.getCreated().compareTo(firstOrderDate) <= 0) {
                firstOrderDate = order.getCreated();
                firstOrderAmount = order.getTotalPrice();
            }

            if (lastOrderDate == null) {
                lastOrderDate = order.getCreated();
                lastOrderAmount = order.getTotalPrice();
            }
            else if (order.getCreated().compareTo(lastOrderDate) >= 0) {
                lastOrderDate = order.getCreated();
                lastOrderAmount = order.getTotalPrice();
            }

            orderCount++;
        }

        long firstAndLastOrderDiff = 0;
        long daysBetweenFirstAndLastOrder = 0;

        if (lastOrderDate != null && firstOrderDate != null) {
            firstAndLastOrderDiff = Math.abs(lastOrderDate.getTime() - firstOrderDate.getTime());
            daysBetweenFirstAndLastOrder = TimeUnit.DAYS.convert(firstAndLastOrderDiff, TimeUnit.MILLISECONDS);
        }

        Integer averageDaysBetweenOrders = null;

        if (orderCount - 1 > 0) {
            averageDaysBetweenOrders = (int) daysBetweenFirstAndLastOrder / (orderCount - 1);
        }

        BigDecimal averageOrderPrice = null;

        if (orderCount > 0) {
            averageOrderPrice = sumOfAllOrders.divide(new BigDecimal(orderCount), 2, RoundingMode.HALF_UP);
        }

        assertEquals(customer.getApplicationId(), customerRfmData.getApplicationId());
        assertEquals(customer.getCustomerId(), customerRfmData.getCustomerId());
        assertEquals(String.format("%s %s", customer.getFirstName(), customer.getLastName()), customerRfmData.getCustomerName());
        assertEquals(RFMHelpers.ToPascalCase(customerAddress.getCompany()), customerRfmData.getCompanyName());
        assertEquals(customer.getCustomerGroup(), customerRfmData.getCustomerGroup());
        assertEquals(customerAddress.getCity(), customerRfmData.getCustomerCity());
        assertEquals(customerAddress.getProvince(), customerRfmData.getCustomerState());
        assertEquals(customerAddress.getCountry(), customerRfmData.getCustomerCountry());
        assertEquals(customer.getEmail(), customerRfmData.getCustomerEmail());
        assertEquals(sumOfAllOrders, customerRfmData.getOrdersSubTotal());
        assertEquals(orderCount, customerRfmData.getOrderCount());
        assertEquals(firstOrderDate, customerRfmData.getFirstOrderDate());
        assertEquals(lastOrderDate, customerRfmData.getLastOrderDate());
        assertEquals(averageDaysBetweenOrders, customerRfmData.getAverageDaysBetweenOrders());
        assertEquals(firstOrderAmount, customerRfmData.getFirstOrderAmount());
        assertEquals(lastOrderAmount, customerRfmData.getLastOrderAmount());
        assertEquals(averageOrderPrice, customerRfmData.getAverageOrderPrice().setScale(2));
        assertEquals(customer.getCreatedAt(), customerRfmData.getCustomerCreatedAt());
    }
}