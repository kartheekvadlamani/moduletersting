#!/bin/bash

export CLASSPATH=/etc/kafka-connect-jdbc/mysql-connector-java-5.1.39-bin.jar

echo "Stopping services"
sudo zookeeper-server-stop
sudo kafka-server-stop
sudo schema-registry-stop

sleep 10

echo "Starting Zookeeper"
sudo zookeeper-server-start /etc/kafka/zookeeper.properties &
sleep 5
echo "Starting Kafka"
sudo kafka-server-start /etc/kafka/server.properties &
sleep 5
echo "Starting Schema Registry"
sudo schema-registry-start /etc/schema-registry/schema-registry.properties &
