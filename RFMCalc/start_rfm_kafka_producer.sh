#!/bin/bash

CLASSPATH="/etc/kafka-connect-jdbc/mysql-connector-java-5.1.39-bin.jar"
export CLASSPATH

connect-standalone /etc/schema-registry/connect-avro-standalone.properties rfm_kafka_import_customers.properties rfm_kafka_import_customer_addresses.properties rfm_kafka_import_orders.properties
