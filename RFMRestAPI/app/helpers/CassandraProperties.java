package helpers;

import com.typesafe.config.Config;
import play.api.Play;

/**
 * Created by brettcooper on 7/7/16.
 */
public class CassandraProperties {
    private final String cassandraHostName;
    private final String cassandraKeyspace;
    private final String cassandraUsername;
    private final String cassandraPassword;

    public CassandraProperties() {
        Config config = Play.current().configuration().underlying();

        cassandraHostName = config.getString("cassandra.hostname");
        cassandraKeyspace = config.getString("cassandra.keyspace");
        cassandraUsername = config.getString("cassandra.username");
        cassandraPassword = config.getString("cassandra.password");
    }

    public String getCassandraHostName() {
        return cassandraHostName;
    }

    public String getCassandraKeyspace() {
        return cassandraKeyspace;
    }

    public String getCassandraUsername() {
        return cassandraUsername;
    }

    public String getCassandraPassword() {
        return cassandraPassword;
    }
}
