package helpers;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.exceptions.InvalidQueryException;
import com.fasterxml.jackson.databind.node.ObjectNode;
import entities.RFMSummaryStatistics;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.libs.Json;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Objects;

import static helpers.RFMLogic.getLastRunDate;

/**
 * Created by brettcooper on 2/27/17.
 */
public final class RFMSummaryStatisticsLogic {

    private static final Logger logger = LoggerFactory.getLogger(RFMSummaryStatisticsLogic.class);

    private RFMSummaryStatisticsLogic() {}

    public static ObjectNode getSummaryStatistics(Integer applicationId) {
        RFMSummaryStatistics rfmSummaryStatisticsMergedResult = getRfmMergedSummaryStatistics(applicationId);

        // Turn the summary statistics object into JSON
        return getRfmSummaryStatisticsJson(rfmSummaryStatisticsMergedResult);
    }

    public static RFMSummaryStatistics merge(RFMSummaryStatistics rfmBatchSummaryStatistics,
                                             RFMSummaryStatistics rfmSpeedSummaryStatistics) {
        return merge(rfmBatchSummaryStatistics, rfmSpeedSummaryStatistics, null);
    }

    public static RFMSummaryStatistics merge(RFMSummaryStatistics rfmBatchSummaryStatistics,
                                             RFMSummaryStatistics rfmSpeedSummaryStatistics,
                                             RFMSummaryStatistics rfmSummaryStatistics) {
        if (rfmBatchSummaryStatistics != null && rfmSpeedSummaryStatistics != null &&
            Objects.equals(rfmSpeedSummaryStatistics.getApplicationId(),
                           rfmBatchSummaryStatistics.getApplicationId())) {

            // Get the application id
            Integer applicationId = rfmSpeedSummaryStatistics.getApplicationId();

            // merge total number of customers together by adding the number of customers from the batch and speed layers together
            Long totalNumberOfCustomersSpeed = rfmSpeedSummaryStatistics.getTotalCustomers();
            Long totalNumberOfCustomersBatch = rfmBatchSummaryStatistics.getTotalCustomers();
            Long totalNumberOfCustomers =
                    mergeTotalNumberOfCustomers(totalNumberOfCustomersSpeed, totalNumberOfCustomersBatch);
            BigDecimal totalNumberOfCustomersBigDecimal = (totalNumberOfCustomers != null) ?
                    new BigDecimal(totalNumberOfCustomers).setScale(2, RoundingMode.HALF_UP) : null;

            // merge total number of orders together by adding the number of orders from the batch and speed layers together
            Integer totalNumberOfOrdersSpeed = rfmSpeedSummaryStatistics.getTotalNumberOfOrders();
            Integer totalNumberOfOrdersBatch = rfmBatchSummaryStatistics.getTotalNumberOfOrders();
            Integer totalNumberOfOrders =
                    mergeTotalNumberOfOrders(totalNumberOfOrdersSpeed, totalNumberOfOrdersBatch);
            BigDecimal totalNumberOfOrdersBigDecimal = (totalNumberOfOrders != null) ?
                    new BigDecimal(totalNumberOfOrders).setScale(2, RoundingMode.HALF_UP) : null;

            // merge total amount of orders together by adding the total amount of orders from the batch and speed layers together
            BigDecimal totalAmountOfOrdersSpeed = rfmSpeedSummaryStatistics.getTotalAmountOfOrders();
            BigDecimal totalAmountOfOrdersBatch = rfmBatchSummaryStatistics.getTotalAmountOfOrders();
            BigDecimal totalAmountOfOrders =
                    mergeTotalAmountOfOrders(totalAmountOfOrdersSpeed, totalAmountOfOrdersBatch);

            BigDecimal grandTotalAmountOfOrders;

            if (rfmSummaryStatistics != null) {
                grandTotalAmountOfOrders = rfmSummaryStatistics.getTotalAmountOfOrders();
            } else {
                grandTotalAmountOfOrders = totalAmountOfOrders;
            }

            // Recalculate percentage total amount of order together
            BigDecimal percentageTotalAmountOfOrders =
                    mergePercentageTotalAmountOfOrders(totalAmountOfOrders, grandTotalAmountOfOrders);

            // Recalculate the average order value by dividing the total amount of all orders by the total number of orders
            BigDecimal zero = new BigDecimal(0).setScale(2, RoundingMode.HALF_UP);
            BigDecimal averageOrderValue =
                    (totalAmountOfOrders != null && totalNumberOfOrdersBigDecimal != null &&
                     totalNumberOfOrdersBigDecimal.compareTo(zero) > 0) ?
                            totalAmountOfOrders.divide(totalNumberOfOrdersBigDecimal, 2, RoundingMode.HALF_UP) :
                            null;

            // Recalculate the average customer lifetime value by dividing the total amount of all orders by the total number of customers
            BigDecimal averageCustomerLifetimeValue =
                    (totalAmountOfOrders != null && totalNumberOfCustomersBigDecimal != null &&
                     totalNumberOfCustomersBigDecimal.compareTo(zero) > 0) ?
                            totalAmountOfOrders.divide(totalNumberOfCustomersBigDecimal, 2, RoundingMode.HALF_UP) :
                            null;

            Integer grandTotalNumberOfOrders;

            if (rfmSummaryStatistics != null) {
                grandTotalNumberOfOrders = rfmSummaryStatistics.getTotalNumberOfOrders();
            } else {
                grandTotalNumberOfOrders = totalNumberOfOrders;
            }

            // Recalculate the percentage total number of orders together
            BigDecimal percentageTotalNumberOfOrders = mergePercentageTotalNumberOfOrders(totalNumberOfOrders,
                                                                                          grandTotalNumberOfOrders);

            // Recalculate the average number of orders by dividing the total number of orders by the total number of customers
            BigDecimal averageNumberOfOrders =
                    (totalNumberOfOrdersBigDecimal != null && totalNumberOfCustomersBigDecimal != null &&
                     totalNumberOfCustomersBigDecimal.compareTo(zero) > 0) ? totalNumberOfOrdersBigDecimal
                            .divide(totalNumberOfCustomersBigDecimal, 2, RoundingMode.HALF_UP) : null;

            BigDecimal percentageOfRepeatBuyersSpeed = rfmSpeedSummaryStatistics.getPercentageOfRepeatBuyers();
            BigDecimal percentageOfRepeatBuyersBatch = rfmBatchSummaryStatistics.getPercentageOfRepeatBuyers();
            BigDecimal percentageRevenueOfRepeatBuyersSpeed =
                    rfmSpeedSummaryStatistics.getPercentageRevenueFromRepeatBuyers();
            BigDecimal percentageRevenueOfRepeatBuyersBatch =
                    rfmBatchSummaryStatistics.getPercentageRevenueFromRepeatBuyers();

            // Recalculate the percentage of repeat buyers together
            BigDecimal percentageOfRepeatBuyers =
                    getMergedPercentageOfRepeatBuyers(totalNumberOfCustomersSpeed, totalNumberOfCustomersBatch,
                                                      percentageOfRepeatBuyersSpeed, percentageOfRepeatBuyersBatch,
                                                      totalNumberOfCustomersBigDecimal);

            // Recalculate the percentage of revenue from repeat buyers
            BigDecimal percentageRevenueFromRepeatBuyers =
                    getMergedPercentageRevenueOfRepeatBuyers(totalAmountOfOrdersSpeed, totalAmountOfOrdersBatch,
                                                             percentageRevenueOfRepeatBuyersSpeed,
                                                             percentageRevenueOfRepeatBuyersBatch, totalAmountOfOrders);

            // merge the average days since last order together by approximating
            Integer averageDaysSinceLastOrderSpeed = rfmSpeedSummaryStatistics.getAverageDaysSinceLastOrder();
            Integer averageDaysSinceLastOrderBatch = rfmBatchSummaryStatistics.getAverageDaysSinceLastOrder();
            int totalCustomersSpeed = totalNumberOfCustomersSpeed.intValue();
            int totalCustomersBatch = totalNumberOfCustomersBatch.intValue();
            Integer averageDaysSinceLastOrder =
                    mergeAverageDaysSinceLastOrder(averageDaysSinceLastOrderSpeed, averageDaysSinceLastOrderBatch,
                                                   totalCustomersSpeed, totalCustomersBatch);

            return new RFMSummaryStatistics(applicationId, totalNumberOfCustomers, totalAmountOfOrders,
                                            percentageTotalAmountOfOrders, averageOrderValue,
                                            averageCustomerLifetimeValue, totalNumberOfOrders,
                                            percentageTotalNumberOfOrders, averageNumberOfOrders,
                                            percentageOfRepeatBuyers, percentageRevenueFromRepeatBuyers,
                                            averageDaysSinceLastOrder);
        } else if (rfmBatchSummaryStatistics != null) {
            return rfmBatchSummaryStatistics;
        } else {
            return rfmSpeedSummaryStatistics;
        }
    }

    public static RFMSummaryStatistics getRfmMergedSummaryStatistics(Integer applicationId) {
        CassandraProperties cassandraProperties = new CassandraProperties();

        // Connect to the cluster and keyspace
        try (Cluster cluster = Cluster.builder().addContactPoint(cassandraProperties.getCassandraHostName())
                .withCredentials(cassandraProperties.getCassandraUsername(), cassandraProperties.getCassandraPassword())
                .build()) {
            Session session = cluster.connect(cassandraProperties.getCassandraKeyspace());

            String rfmSummaryStatisticsTableName = "summary_stats";
            RFMSummaryStatistics rfmBatchSummaryStatistics =
                    getSummaryStatisticsBatchResults(applicationId, session, rfmSummaryStatisticsTableName);
            RFMSummaryStatistics rfmSpeedSummaryStatistics =
                    getSummaryStatisticsSpeedResults(applicationId, session, rfmSummaryStatisticsTableName);
            RFMSummaryStatistics rfmSummaryStatisticsMergedResult;

            // merge the batch and speed results together
            if (rfmBatchSummaryStatistics != null && rfmSpeedSummaryStatistics != null) {
                rfmSummaryStatisticsMergedResult = merge(rfmBatchSummaryStatistics, rfmSpeedSummaryStatistics);
            } else if (rfmBatchSummaryStatistics != null) {
                rfmSummaryStatisticsMergedResult = rfmBatchSummaryStatistics;
            } else {
                rfmSummaryStatisticsMergedResult = rfmSpeedSummaryStatistics;
            }

            return rfmSummaryStatisticsMergedResult;
        }
    }

    private static BigDecimal getMergedPercentageOfRepeatBuyers(Long totalNumberOfCustomersSpeed,
                                                                Long totalNumberOfCustomersBatch,
                                                                BigDecimal percentageOfRepeatBuyersSpeed,
                                                                BigDecimal percentageOfRepeatBuyersBatch,
                                                                BigDecimal totalNumberOfCustomers) {
        BigDecimal numberOfRepeatBuyersSpeed = null;

        if (percentageOfRepeatBuyersSpeed != null) {
            BigDecimal totalNumberOfCustomersSpeedBigDecimal = new BigDecimal(totalNumberOfCustomersSpeed);
            numberOfRepeatBuyersSpeed = percentageOfRepeatBuyersSpeed.multiply(totalNumberOfCustomersSpeedBigDecimal);
        }

        BigDecimal numberOfRepeatBuyersBatch = null;

        if (percentageOfRepeatBuyersBatch != null) {
            BigDecimal totalNumberOfCustomersBatchBigDecimal = new BigDecimal(totalNumberOfCustomersBatch);
            numberOfRepeatBuyersBatch = percentageOfRepeatBuyersBatch.multiply(totalNumberOfCustomersBatchBigDecimal);
        }

        BigDecimal totalNumberOfRepeatBuyers;

        if (numberOfRepeatBuyersSpeed != null && numberOfRepeatBuyersBatch != null) {
            totalNumberOfRepeatBuyers = numberOfRepeatBuyersSpeed.add(numberOfRepeatBuyersBatch);
        } else if (numberOfRepeatBuyersBatch != null) {
            totalNumberOfRepeatBuyers = numberOfRepeatBuyersBatch;
        } else {
            totalNumberOfRepeatBuyers = numberOfRepeatBuyersSpeed;
        }

        BigDecimal percentageOfRepeatBuyers = null;
        if (totalNumberOfRepeatBuyers != null && totalNumberOfCustomers != null &&
            totalNumberOfCustomers.compareTo(new BigDecimal(0)) > 0) {
            percentageOfRepeatBuyers =
                    totalNumberOfRepeatBuyers.divide(totalNumberOfCustomers, 2, RoundingMode.HALF_UP);
        }

        return percentageOfRepeatBuyers;
    }

    private static Integer mergeAverageDaysSinceLastOrder(Integer averageDaysSinceLastOrderSpeed,
                                                          Integer averageDaysSinceLastOrderBatch,
                                                          Integer totalCustomersSpeed, Integer totalCustomersBatch) {
        Integer averageDaysSinceLastOrderInt = null;

        BigDecimal averageDaysSinceLastOrderSpeedBigDecimal = new BigDecimal(averageDaysSinceLastOrderSpeed);
        BigDecimal averageDaysSinceLastOrderBatchBigDecimal = new BigDecimal(averageDaysSinceLastOrderBatch);
        BigDecimal totalCustomersSpeedBigDecimal = new BigDecimal(totalCustomersSpeed);
        BigDecimal totalCustomersBatchBigDecimal = new BigDecimal(totalCustomersBatch);

        BigDecimal totalCustomers = totalCustomersBatchBigDecimal.add(totalCustomersSpeedBigDecimal);

        if (totalCustomers != null && totalCustomers.compareTo(new BigDecimal(0)) > 0) {
            BigDecimal sumOfAllAverageDaysSinceLastOrderSpeed =
                    totalCustomersSpeedBigDecimal.multiply(averageDaysSinceLastOrderSpeedBigDecimal);
            BigDecimal sumOfAllAverageDaysSinceLastOrderBatch =
                    totalCustomersBatchBigDecimal.multiply(averageDaysSinceLastOrderBatchBigDecimal);
            BigDecimal sumOfAllAverageDaysSinceLastOrder =
                    sumOfAllAverageDaysSinceLastOrderBatch.add(sumOfAllAverageDaysSinceLastOrderSpeed);
            BigDecimal averageDaysSinceLastOrder =
                    sumOfAllAverageDaysSinceLastOrder.divide(totalCustomers, 2, RoundingMode.HALF_UP);
            averageDaysSinceLastOrderInt = averageDaysSinceLastOrder.intValue();
        }

        return averageDaysSinceLastOrderInt;
    }

    private static BigDecimal mergePercentageTotalNumberOfOrders(Integer totalNumberOfOrders,
                                                                 Integer grandTotalNumberOfOrders) {
        BigDecimal percentageTotalNumberOfOrders = null;

        if (totalNumberOfOrders != null && grandTotalNumberOfOrders != null && grandTotalNumberOfOrders != 0) {
            BigDecimal totalNumberOfOrdersBigDecimal = new BigDecimal(totalNumberOfOrders);
            BigDecimal grandTotalNumberOfOrdersBigDecimal = new BigDecimal(grandTotalNumberOfOrders);

            percentageTotalNumberOfOrders = totalNumberOfOrdersBigDecimal
                    .divide(grandTotalNumberOfOrdersBigDecimal, 2, RoundingMode.HALF_UP);
        }

        return percentageTotalNumberOfOrders;
    }

    private static BigDecimal mergePercentageTotalAmountOfOrders(BigDecimal totalAmountOfOrders,
                                                                 BigDecimal grandTotalAmountOfOrders) {
        BigDecimal percentageTotalAmountOfOrders = null;

        if (totalAmountOfOrders != null && grandTotalAmountOfOrders != null &&
            grandTotalAmountOfOrders.compareTo(new BigDecimal(0)) > 0) {
            percentageTotalAmountOfOrders =
                    totalAmountOfOrders.divide(grandTotalAmountOfOrders, 2, RoundingMode.HALF_UP);
        }

        return percentageTotalAmountOfOrders;
    }

    private static BigDecimal mergeTotalAmountOfOrders(BigDecimal totalAmountOfOrders1,
                                                       BigDecimal totalAmountOfOrders2) {
        BigDecimal totalAmountOfOrders;

        if (totalAmountOfOrders1 != null && totalAmountOfOrders2 != null) {
            totalAmountOfOrders = totalAmountOfOrders1.add(totalAmountOfOrders2);
        } else if (totalAmountOfOrders1 != null) {
            totalAmountOfOrders = totalAmountOfOrders1;
        } else {
            totalAmountOfOrders = totalAmountOfOrders2;
        }

        return totalAmountOfOrders;
    }

    private static Integer mergeTotalNumberOfOrders(Integer totalNumberOfOrders1, Integer totalNumberOfOrders2) {
        Integer totalNumberOfOrders;

        if (totalNumberOfOrders1 != null && totalNumberOfOrders2 != null) {
            totalNumberOfOrders = totalNumberOfOrders1 + totalNumberOfOrders2;
        } else if (totalNumberOfOrders1 != null) {
            totalNumberOfOrders = totalNumberOfOrders1;
        } else {
            totalNumberOfOrders = totalNumberOfOrders2;
        }

        return totalNumberOfOrders;
    }

    private static Long mergeTotalNumberOfCustomers(Long totalNumberOfCustomers1, Long totalNumberOfCustomers2) {
        Long totalNumberOfCustomers;

        if (totalNumberOfCustomers1 != null && totalNumberOfCustomers2 != null) {
            totalNumberOfCustomers = totalNumberOfCustomers1 + totalNumberOfCustomers2;
        } else if (totalNumberOfCustomers1 != null) {
            totalNumberOfCustomers = totalNumberOfCustomers1;
        } else {
            totalNumberOfCustomers = totalNumberOfCustomers2;
        }

        return totalNumberOfCustomers;
    }

    private static BigDecimal getMergedPercentageRevenueOfRepeatBuyers(BigDecimal totalAmountOfOrdersSpeed,
                                                                       BigDecimal totalAmountOfOrdersBatch,
                                                                       BigDecimal percentageRevenueOfRepeatBuyersSpeed,
                                                                       BigDecimal percentageRevenueOfRepeatBuyersBatch,
                                                                       BigDecimal totalAmountOfOrders) {
        // Revenue from repeat buyers / sum of all orders
        BigDecimal totalRevenueOfRepeatBuyersSpeed = null;

        if (percentageRevenueOfRepeatBuyersSpeed != null) {
            totalRevenueOfRepeatBuyersSpeed = percentageRevenueOfRepeatBuyersSpeed.multiply(totalAmountOfOrdersSpeed);
        }

        BigDecimal totalRevenueOfRepeatBuyersBatch = null;

        if (percentageRevenueOfRepeatBuyersBatch != null) {
            totalRevenueOfRepeatBuyersBatch = percentageRevenueOfRepeatBuyersBatch.multiply(totalAmountOfOrdersBatch);
        }

        BigDecimal totalRevenueOfRepeatBuyers;

        if (totalRevenueOfRepeatBuyersSpeed != null && totalRevenueOfRepeatBuyersBatch != null) {
            totalRevenueOfRepeatBuyers = totalRevenueOfRepeatBuyersSpeed.add(totalRevenueOfRepeatBuyersBatch);
        } else if (totalRevenueOfRepeatBuyersBatch != null) {
            totalRevenueOfRepeatBuyers = totalRevenueOfRepeatBuyersBatch;
        } else {
            totalRevenueOfRepeatBuyers = totalRevenueOfRepeatBuyersSpeed;
        }

        BigDecimal percentageRevenueOfRepeatBuyers = null;
        if (totalRevenueOfRepeatBuyers != null && totalAmountOfOrders != null &&
            totalAmountOfOrders.compareTo(new BigDecimal(0)) > 0) {
            percentageRevenueOfRepeatBuyers =
                    totalRevenueOfRepeatBuyers.divide(totalAmountOfOrders, 2, RoundingMode.HALF_UP);
        }

        return percentageRevenueOfRepeatBuyers;
    }

    private static RFMSummaryStatistics getSummaryStatisticsSpeedResults(Integer applicationId, Session session,
                                                                         String rfmSummaryStatisticsTableName) {
        return getRfmSummaryStatistics(applicationId, session, rfmSummaryStatisticsTableName, null);
    }

    private static RFMSummaryStatistics getSummaryStatisticsBatchResults(Integer applicationId, Session session,
                                                                         String rfmSummaryStatisticsTableName) {
        String lastRunDate = getLastRunDate(session);

        return getRfmSummaryStatistics(applicationId, session, rfmSummaryStatisticsTableName, lastRunDate);
    }

    private static RFMSummaryStatistics getRfmSummaryStatistics(Integer applicationId, Session session,
                                                                String rfmSummaryStatisticsTableName,
                                                                String lastRunDate) {
        String cqlGetSummaryStatistics;

        if (lastRunDate != null && !lastRunDate.isEmpty()) {
            cqlGetSummaryStatistics =
                    String.format("select * from %s_%s where application_id = %s", rfmSummaryStatisticsTableName,
                                  lastRunDate, applicationId);
        } else {
            cqlGetSummaryStatistics =
                    String.format("select * from %s where application_id = %s", rfmSummaryStatisticsTableName,
                                  applicationId);
        }

        RFMSummaryStatistics rfmSummaryStatistics = null;

        try {
            // Use select to get the user we just entered
            ResultSet results = session.execute(cqlGetSummaryStatistics);

            List<Row> allBatchResults = results.all();

            if (!allBatchResults.isEmpty()) {
                Row firstRowResult = allBatchResults.get(0);
                rfmSummaryStatistics = getRfmSummaryStatistics(firstRowResult);
            }
        } catch (InvalidQueryException ex) {
            logger.error("Error getting RFM summary stats: {}", ex);
        }

        return rfmSummaryStatistics;
    }

    public static ObjectNode getRfmSummaryStatisticsJson(RFMSummaryStatistics rfmSummaryStatistics) {
        ObjectNode rfmSummaryStatisticsJson = Json.newObject();

        if (rfmSummaryStatistics != null) {
            rfmSummaryStatisticsJson.put("application_id", rfmSummaryStatistics.getApplicationId());
            rfmSummaryStatisticsJson
                    .put("average_customer_lifetime_value", rfmSummaryStatistics.getAverageCustomerLifetimeValue());
            rfmSummaryStatisticsJson
                    .put("average_days_since_last_order", rfmSummaryStatistics.getAverageDaysSinceLastOrder());
            rfmSummaryStatisticsJson.put("average_number_of_orders", rfmSummaryStatistics.getAverageNumberOfOrders());
            rfmSummaryStatisticsJson.put("average_order_value", rfmSummaryStatistics.getAverageOrderValue());
            rfmSummaryStatisticsJson
                    .put("percentage_of_repeat_buyers", rfmSummaryStatistics.getPercentageOfRepeatBuyers());
            rfmSummaryStatisticsJson.put("percentage_revenue_from_repeat_buyers",
                                         rfmSummaryStatistics.getPercentageRevenueFromRepeatBuyers());
            rfmSummaryStatisticsJson
                    .put("percentage_total_amount_of_orders", rfmSummaryStatistics.getPercentageTotalAmountOfOrders());
            rfmSummaryStatisticsJson
                    .put("percentage_total_number_of_orders", rfmSummaryStatistics.getPercentageTotalNumberOfOrders());
            rfmSummaryStatisticsJson.put("total_amount_of_orders", rfmSummaryStatistics.getTotalAmountOfOrders());
            rfmSummaryStatisticsJson.put("total_customers", rfmSummaryStatistics.getTotalCustomers());
            rfmSummaryStatisticsJson.put("total_number_of_orders", rfmSummaryStatistics.getTotalNumberOfOrders());
        }

        return rfmSummaryStatisticsJson;
    }

    public static RFMSummaryStatistics getRfmSummaryStatistics(Row row) {
        if (row != null) {
            int applicationId = row.getInt("application_id");
            BigDecimal averageCustomerLifetimeValue = row.getDecimal("average_customer_lifetime_value");
            int averageDaysSinceLastOrder = row.getInt("average_days_since_last_order");
            BigDecimal averageNumberOfOrders = row.getDecimal("average_number_of_orders");
            BigDecimal averageOrderValue = row.getDecimal("average_order_value");
            BigDecimal percentageOfRepeatBuyers = row.getDecimal("percentage_of_repeat_buyers");
            BigDecimal percentageRevenueFromRepeatBuyers = row.getDecimal("percentage_revenue_from_repeat_buyers");
            BigDecimal percentageTotalAmountOfOrders = row.getDecimal("percentage_total_amount_of_orders");
            BigDecimal percentageTotalNumberOfOrders = row.getDecimal("percentage_total_number_of_orders");
            BigDecimal totalAmountOfOrders = row.getDecimal("total_amount_of_orders");
            long totalCustomers = row.getLong("total_customers");
            int numberOfOrders = row.getInt("total_number_of_orders");

            return new RFMSummaryStatistics(applicationId, totalCustomers, totalAmountOfOrders,
                                            percentageTotalAmountOfOrders, averageOrderValue,
                                            averageCustomerLifetimeValue, numberOfOrders,
                                            percentageTotalNumberOfOrders, averageNumberOfOrders,
                                            percentageOfRepeatBuyers, percentageRevenueFromRepeatBuyers,
                                            averageDaysSinceLastOrder);
        }

        return null;
    }
}
