package helpers;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.exceptions.InvalidQueryException;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import entities.RFMSummaryStatistics;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.libs.Json;
import java.util.ArrayList;
import java.util.List;

import static helpers.RFMSummaryStatisticsLogic.*;

/**
 * Created by brettcooper on 2/27/17.
 */
public final class RFMEightyTwentyLogic {

    private static final Logger logger = LoggerFactory.getLogger(RFMEightyTwentyLogic.class);

    private RFMEightyTwentyLogic() {}

    public static ArrayNode getEightyTwentySummaryStatistics(Integer applicationId) {
        CassandraProperties cassandraProperties = new CassandraProperties();

        // Connect to the cluster and keyspace
        try (Cluster cluster = Cluster.builder().addContactPoint(cassandraProperties.getCassandraHostName())
                .withCredentials(cassandraProperties.getCassandraUsername(), cassandraProperties.getCassandraPassword())
                .build()) {
            Session session = cluster.connect(cassandraProperties.getCassandraKeyspace());

            String rfmSummaryStatisticsTop1PercentTableName = "summary_stats_top_1";
            String rfmSummaryStatisticsTop5PercentTableName = "summary_stats_top_5";
            String rfmSummaryStatisticsTop15PercentTableName = "summary_stats_top_15";
            String rfmSummaryStatisticsTop25PercentTableName = "summary_stats_top_25";
            String rfmSummaryStatisticsTop50PercentTableName = "summary_stats_top_50";
            String rfmSummaryStatisticsBottom50PercentTableName = "summary_stats_bottom_50";

            List<RFMSummaryStatistics> eightyTwentyBatchResults =
                    getEightyTwentyBatchResults(session, applicationId, rfmSummaryStatisticsTop1PercentTableName,
                                                rfmSummaryStatisticsTop5PercentTableName,
                                                rfmSummaryStatisticsTop15PercentTableName,
                                                rfmSummaryStatisticsTop25PercentTableName,
                                                rfmSummaryStatisticsTop50PercentTableName,
                                                rfmSummaryStatisticsBottom50PercentTableName);

            List<RFMSummaryStatistics> eightyTwentySpeedResults =
                    getEightyTwentySpeedResults(session, applicationId, rfmSummaryStatisticsTop1PercentTableName,
                                                rfmSummaryStatisticsTop5PercentTableName,
                                                rfmSummaryStatisticsTop15PercentTableName,
                                                rfmSummaryStatisticsTop25PercentTableName,
                                                rfmSummaryStatisticsTop50PercentTableName,
                                                rfmSummaryStatisticsBottom50PercentTableName);
            List<RFMSummaryStatistics> eightyTwentyMergedResults;

            RFMSummaryStatistics summaryStatistics = getRfmMergedSummaryStatistics(applicationId);

            if (eightyTwentyBatchResults != null && eightyTwentySpeedResults != null) {
                eightyTwentyMergedResults = new ArrayList<>(eightyTwentyBatchResults.size());

                RFMSummaryStatistics rfmSummaryStatisticsTop1PercentMergedResult =
                        merge(eightyTwentyBatchResults.get(0), eightyTwentySpeedResults.get(0), summaryStatistics);
                RFMSummaryStatistics rfmSummaryStatisticsTop5PercentMergedResult =
                        merge(eightyTwentyBatchResults.get(1), eightyTwentySpeedResults.get(1), summaryStatistics);
                RFMSummaryStatistics rfmSummaryStatisticsTop15PercentMergedResult =
                        merge(eightyTwentyBatchResults.get(2), eightyTwentySpeedResults.get(2), summaryStatistics);
                RFMSummaryStatistics rfmSummaryStatisticsTop25PercentMergedResult =
                        merge(eightyTwentyBatchResults.get(3), eightyTwentySpeedResults.get(3), summaryStatistics);
                RFMSummaryStatistics rfmSummaryStatisticsTop50PercentMergedResult =
                        merge(eightyTwentyBatchResults.get(4), eightyTwentySpeedResults.get(4), summaryStatistics);
                RFMSummaryStatistics rfmSummaryStatisticsBottom50PercentMergedResult =
                        merge(eightyTwentyBatchResults.get(5), eightyTwentySpeedResults.get(5), summaryStatistics);

                eightyTwentyMergedResults.add(rfmSummaryStatisticsTop1PercentMergedResult);
                eightyTwentyMergedResults.add(rfmSummaryStatisticsTop5PercentMergedResult);
                eightyTwentyMergedResults.add(rfmSummaryStatisticsTop15PercentMergedResult);
                eightyTwentyMergedResults.add(rfmSummaryStatisticsTop25PercentMergedResult);
                eightyTwentyMergedResults.add(rfmSummaryStatisticsTop50PercentMergedResult);
                eightyTwentyMergedResults.add(rfmSummaryStatisticsBottom50PercentMergedResult);
            } else if (eightyTwentyBatchResults != null) {
                eightyTwentyMergedResults = eightyTwentyBatchResults;
            } else {
                eightyTwentyMergedResults = eightyTwentySpeedResults;
            }

            RFMSummaryStatistics rfmTop1PercentSummaryStatistics = eightyTwentyMergedResults.get(0);
            RFMSummaryStatistics rfmTop5PercentSummaryStatistics = eightyTwentyMergedResults.get(1);
            RFMSummaryStatistics rfmTop15PercentSummaryStatistics = eightyTwentyMergedResults.get(2);
            RFMSummaryStatistics rfmTop25PercentSummaryStatistics = eightyTwentyMergedResults.get(3);
            RFMSummaryStatistics rfmTop50PercentSummaryStatistics = eightyTwentyMergedResults.get(4);
            RFMSummaryStatistics rfmBottom50PercentSummaryStatistics = eightyTwentyMergedResults.get(5);

            ObjectNode rfmSummaryStatisticsTop1PercentJson =
                    getRfmSummaryStatisticsJson(rfmTop1PercentSummaryStatistics);
            ObjectNode rfmSummaryStatisticsTop5PercentJson =
                    getRfmSummaryStatisticsJson(rfmTop5PercentSummaryStatistics);
            ObjectNode rfmSummaryStatisticsTop15PercentJson =
                    getRfmSummaryStatisticsJson(rfmTop15PercentSummaryStatistics);
            ObjectNode rfmSummaryStatisticsTop25PercentJson =
                    getRfmSummaryStatisticsJson(rfmTop25PercentSummaryStatistics);
            ObjectNode rfmSummaryStatisticsTop50PercentJson =
                    getRfmSummaryStatisticsJson(rfmTop50PercentSummaryStatistics);
            ObjectNode rfmSummaryStatisticsBottom50PercentJson =
                    getRfmSummaryStatisticsJson(rfmBottom50PercentSummaryStatistics);

            ArrayNode jsonNodes = Json.newArray();

            jsonNodes.add(rfmSummaryStatisticsTop1PercentJson);
            jsonNodes.add(rfmSummaryStatisticsTop5PercentJson);
            jsonNodes.add(rfmSummaryStatisticsTop15PercentJson);
            jsonNodes.add(rfmSummaryStatisticsTop25PercentJson);
            jsonNodes.add(rfmSummaryStatisticsTop50PercentJson);
            jsonNodes.add(rfmSummaryStatisticsBottom50PercentJson);

            return jsonNodes;
        }
    }

    private static List<RFMSummaryStatistics> getEightyTwentyBatchResults(Session session, Integer applicationId,
                                                                          String rfmSummaryStatisticsTop1PercentTableName,
                                                                          String rfmSummaryStatisticsTop5PercentTableName,
                                                                          String rfmSummaryStatisticsTop15PercentTableName,
                                                                          String rfmSummaryStatisticsTop25PercentTableName,
                                                                          String rfmSummaryStatisticsTop50PercentTableName,
                                                                          String rfmSummaryStatisticsBottom50PercentTableName) {
        String lastRunDate = RFMLogic.getLastRunDate(session);

        return getEightyTwentyResults(session, applicationId, rfmSummaryStatisticsTop1PercentTableName,
                                      rfmSummaryStatisticsTop5PercentTableName,
                                      rfmSummaryStatisticsTop15PercentTableName,
                                      rfmSummaryStatisticsTop25PercentTableName,
                                      rfmSummaryStatisticsTop50PercentTableName,
                                      rfmSummaryStatisticsBottom50PercentTableName, lastRunDate);
    }

    private static List<RFMSummaryStatistics> getEightyTwentySpeedResults(Session session, Integer applicationId,
                                                                          String rfmSummaryStatisticsTop1PercentTableName,
                                                                          String rfmSummaryStatisticsTop5PercentTableName,
                                                                          String rfmSummaryStatisticsTop15PercentTableName,
                                                                          String rfmSummaryStatisticsTop25PercentTableName,
                                                                          String rfmSummaryStatisticsTop50PercentTableName,
                                                                          String rfmSummaryStatisticsBottom50PercentTableName) {

        return getEightyTwentyResults(session, applicationId, rfmSummaryStatisticsTop1PercentTableName,
                                      rfmSummaryStatisticsTop5PercentTableName,
                                      rfmSummaryStatisticsTop15PercentTableName,
                                      rfmSummaryStatisticsTop25PercentTableName,
                                      rfmSummaryStatisticsTop50PercentTableName,
                                      rfmSummaryStatisticsBottom50PercentTableName, null);
    }

    private static List<RFMSummaryStatistics> getEightyTwentyResults(Session session, Integer applicationId,
                                                                     String rfmSummaryStatisticsTop1PercentTableName,
                                                                     String rfmSummaryStatisticsTop5PercentTableName,
                                                                     String rfmSummaryStatisticsTop15PercentTableName,
                                                                     String rfmSummaryStatisticsTop25PercentTableName,
                                                                     String rfmSummaryStatisticsTop50PercentTableName,
                                                                     String rfmSummaryStatisticsBottom50PercentTableName,
                                                                     String lastRunDate) {
        String cqlRfmSummaryStatisticsTop1Percent;
        String cqlRfmSummaryStatisticsTop5Percent;
        String cqlRfmSummaryStatisticsTop15Percent;
        String cqlRfmSummaryStatisticsTop25Percent;
        String cqlRfmSummaryStatisticsTop50Percent;
        String cqlRfmSummaryStatisticsBottom50Percent;

        if (lastRunDate != null && !lastRunDate.isEmpty()) {
            cqlRfmSummaryStatisticsTop1Percent = String.format("select * from %s_%s where application_id = %s",
                                                               rfmSummaryStatisticsTop1PercentTableName, lastRunDate,
                                                               applicationId);
            cqlRfmSummaryStatisticsTop5Percent = String.format("select * from %s_%s where application_id = %s",
                                                               rfmSummaryStatisticsTop5PercentTableName, lastRunDate,
                                                               applicationId);
            cqlRfmSummaryStatisticsTop15Percent = String.format("select * from %s_%s where application_id = %s",
                                                                rfmSummaryStatisticsTop15PercentTableName, lastRunDate,
                                                                applicationId);
            cqlRfmSummaryStatisticsTop25Percent = String.format("select * from %s_%s where application_id = %s",
                                                                rfmSummaryStatisticsTop25PercentTableName, lastRunDate,
                                                                applicationId);
            cqlRfmSummaryStatisticsTop50Percent = String.format("select * from %s_%s where application_id = %s",
                                                                rfmSummaryStatisticsTop50PercentTableName, lastRunDate,
                                                                applicationId);
            cqlRfmSummaryStatisticsBottom50Percent = String.format("select * from %s_%s where application_id = %s",
                                                                   rfmSummaryStatisticsBottom50PercentTableName,
                                                                   lastRunDate, applicationId);
        } else {
            cqlRfmSummaryStatisticsTop1Percent = String.format("select * from %s where application_id = %s",
                                                               rfmSummaryStatisticsTop1PercentTableName, applicationId);
            cqlRfmSummaryStatisticsTop5Percent = String.format("select * from %s where application_id = %s",
                                                               rfmSummaryStatisticsTop5PercentTableName, applicationId);
            cqlRfmSummaryStatisticsTop15Percent = String.format("select * from %s where application_id = %s",
                                                                rfmSummaryStatisticsTop15PercentTableName,
                                                                applicationId);
            cqlRfmSummaryStatisticsTop25Percent = String.format("select * from %s where application_id = %s",
                                                                rfmSummaryStatisticsTop25PercentTableName,
                                                                applicationId);
            cqlRfmSummaryStatisticsTop50Percent = String.format("select * from %s where application_id = %s",
                                                                rfmSummaryStatisticsTop50PercentTableName,
                                                                applicationId);
            cqlRfmSummaryStatisticsBottom50Percent = String.format("select * from %s where application_id = %s",
                                                                   rfmSummaryStatisticsBottom50PercentTableName,
                                                                   applicationId);
        }

        List<RFMSummaryStatistics> eightyTwentySummaryStatisticsList = null;

        try {
            // Use select to get the user we just entered
            ResultSet resultsTop1Percent = session.execute(cqlRfmSummaryStatisticsTop1Percent);
            ResultSet resultsTop5Percent = session.execute(cqlRfmSummaryStatisticsTop5Percent);
            ResultSet resultsTop15Percent = session.execute(cqlRfmSummaryStatisticsTop15Percent);
            ResultSet resultsTop25Percent = session.execute(cqlRfmSummaryStatisticsTop25Percent);
            ResultSet resultsTop50Percent = session.execute(cqlRfmSummaryStatisticsTop50Percent);
            ResultSet resultsBottom50Percent = session.execute(cqlRfmSummaryStatisticsBottom50Percent);

            Row top1PercentFirstRowResult = resultsTop1Percent.one();
            Row top5PercentFirstRowResult = resultsTop5Percent.one();
            Row top15PercentFirstRowResult = resultsTop15Percent.one();
            Row top25PercentFirstRowResult = resultsTop25Percent.one();
            Row top50PercentFirstRowResult = resultsTop50Percent.one();
            Row bottom50PercentFirstRowResult = resultsBottom50Percent.one();

            RFMSummaryStatistics rfmTop1PercentSummaryStatistics = getRfmSummaryStatistics(top1PercentFirstRowResult);
            RFMSummaryStatistics rfmTop5PercentSummaryStatistics = getRfmSummaryStatistics(top5PercentFirstRowResult);
            RFMSummaryStatistics rfmTop15PercentSummaryStatistics = getRfmSummaryStatistics(top15PercentFirstRowResult);
            RFMSummaryStatistics rfmTop25PercentSummaryStatistics = getRfmSummaryStatistics(top25PercentFirstRowResult);
            RFMSummaryStatistics rfmTop50PercentSummaryStatistics = getRfmSummaryStatistics(top50PercentFirstRowResult);
            RFMSummaryStatistics rfmBottom50PercentSummaryStatistics =
                    getRfmSummaryStatistics(bottom50PercentFirstRowResult);

            eightyTwentySummaryStatisticsList = new ArrayList<>();

            eightyTwentySummaryStatisticsList.add(rfmTop1PercentSummaryStatistics);
            eightyTwentySummaryStatisticsList.add(rfmTop5PercentSummaryStatistics);
            eightyTwentySummaryStatisticsList.add(rfmTop15PercentSummaryStatistics);
            eightyTwentySummaryStatisticsList.add(rfmTop25PercentSummaryStatistics);
            eightyTwentySummaryStatisticsList.add(rfmTop50PercentSummaryStatistics);
            eightyTwentySummaryStatisticsList.add(rfmBottom50PercentSummaryStatistics);
        } catch (InvalidQueryException ex) {
            logger.error("Error getting 80/20 stats: {}", ex);
        }

        return eightyTwentySummaryStatisticsList;
    }
}
