package helpers;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.exceptions.InvalidQueryException;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import entities.RFMResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.libs.Json;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static helpers.RFMLogic.getLastRunDate;

/**
 * Created by brettcooper on 2/27/17.
 */
public final class RFMResultsLogic {

    private static final Logger logger = LoggerFactory.getLogger(RFMResultsLogic.class);

    private RFMResultsLogic() {}

    public static ArrayNode getRfmResults(Integer applicationId) {
        List<RFMResult> rfmMergedResults = getRfmMergedResults(applicationId);

        // Turn the RFM results object into JSON

        return getRfmResultsJson(rfmMergedResults);
    }

    private static List<RFMResult> getCqlRfmResults(Integer applicationId, String rfmResultsTableName,
                                                    Session session, String lastRunDate) {
        String cqlGetRfmResults;

        if (lastRunDate != null && !lastRunDate.isEmpty()) {
            cqlGetRfmResults =
                    String.format("select * from %s_%s where application_id = %s", rfmResultsTableName, lastRunDate,
                                  applicationId);
        } else {
            cqlGetRfmResults =
                    String.format("select * from %s where application_id = %s", rfmResultsTableName, applicationId);
        }

        List<RFMResult> rfmResults = new ArrayList<>();

        try {
            // Use select to get the user we just entered
            ResultSet rfmResultsSet = session.execute(cqlGetRfmResults);

            List<Row> allResults = rfmResultsSet.all();

            if (allResults != null && !allResults.isEmpty()) {
                Row firstRowResult = allResults.get(0);
                RFMResult rfmResult = getRfmResult(firstRowResult);

                if (rfmResult != null) {
                    rfmResults.add(rfmResult);
                }
            }
        } catch (InvalidQueryException ex) {
            logger.error("Error getting RFM results: {}", ex);
        }

        return rfmResults;
    }

    private static RFMResult getRfmResult(Row row) {
        if (row != null) {
            int applicationId = row.getInt("application_id");
            BigInteger customerId = BigInteger.valueOf(row.getInt("customer_id"));
            BigDecimal averageDaysBetweenOrders = row.getDecimal("average_days_between_orders");
            BigDecimal averageOrderPrice = row.getDecimal("average_order_price");
            String companyName = row.getString("company_name");
            String customerCity = row.getString("customer_city");
            String customerCountry = row.getString("customer_country");
            ZonedDateTime customerCreatedAt =
                    row.getDate("customer_created_at").toInstant().atZone(ZoneId.systemDefault());
            String customerEmail = row.getString("customer_email");
            String customerGroup = row.getString("customer_group");
            String customerName = row.getString("customer_name");
            String customerState = row.getString("customer_state");
            BigDecimal firstOrderAmount = row.getDecimal("first_order_amount");
            ZonedDateTime firstOrderDate = row.getDate("first_order_date").toInstant().atZone(ZoneId.systemDefault());
            BigDecimal lastOrderAmount = row.getDecimal("last_order_amount");
            ZonedDateTime lastOrderDate = row.getDate("last_order_date").toInstant().atZone(ZoneId.systemDefault());
            int ordersCount = row.getInt("orders_count");
            BigDecimal ordersSubTotal = row.getDecimal("orders_sub_total");

            return new RFMResult(applicationId, customerId, averageDaysBetweenOrders, averageOrderPrice, companyName,
                                 customerCity, customerCountry, customerCreatedAt, customerEmail, customerGroup,
                                 customerName, customerState, firstOrderAmount, firstOrderDate, lastOrderAmount,
                                 lastOrderDate, ordersCount, ordersSubTotal);
        }

        return null;
    }

    private static ArrayNode getRfmResultsJson(List<RFMResult> rfmResults) {
        ArrayNode jsonNodes = Json.newArray();

        if (rfmResults == null || rfmResults.isEmpty()) {
            return jsonNodes;
        }

        for (RFMResult rfmResult : rfmResults) {
            ObjectNode rfmResultsJson = Json.newObject();

            if (rfmResult != null) {
                rfmResultsJson.put("application_id", rfmResult.getApplicationId());
                rfmResultsJson.put("customer_id", String.valueOf(rfmResult.getCustomerId()));
                rfmResultsJson.put("average_days_between_orders", rfmResult.getAverageDaysBetweenOrders());
                rfmResultsJson.put("average_order_price", rfmResult.getAverageOrderPrice());
                rfmResultsJson.put("company_name", rfmResult.getCompanyName());
                rfmResultsJson.put("customer_city", rfmResult.getCustomerCity());
                rfmResultsJson.put("customer_country", rfmResult.getCustomerCountry());
                rfmResultsJson.put("customer_created_at", String.valueOf(rfmResult.getCustomerCreatedAt()));
                rfmResultsJson.put("customer_email", rfmResult.getCustomerEmail());
                rfmResultsJson.put("customer_group", rfmResult.getCustomerGroup());
                rfmResultsJson.put("customer_name", rfmResult.getCustomerName());
                rfmResultsJson.put("customer_state", rfmResult.getCustomerState());
                rfmResultsJson.put("first_order_amount", rfmResult.getFirstOrderAmount());
                rfmResultsJson.put("first_order_date", String.valueOf(rfmResult.getFirstOrderDate()));
                rfmResultsJson.put("last_order_amount", rfmResult.getLastOrderAmount());
                rfmResultsJson.put("last_order_date", String.valueOf(rfmResult.getLastOrderDate()));
                rfmResultsJson.put("orders_count", rfmResult.getOrdersCount());
                rfmResultsJson.put("orders_sub_total", rfmResult.getOrdersSubTotal());
            }

            jsonNodes.add(rfmResultsJson);
        }

        return jsonNodes;
    }

    private static List<RFMResult> getRfmMergedResults(Integer applicationId) {
        CassandraProperties cassandraProperties = new CassandraProperties();

        // Connect to the cluster and keyspace
        try (Cluster cluster = Cluster.builder().addContactPoint(cassandraProperties.getCassandraHostName())
                .withCredentials(cassandraProperties.getCassandraUsername(), cassandraProperties.getCassandraPassword())
                .build()) {
            Session session = cluster.connect(cassandraProperties.getCassandraKeyspace());

            String rfmResultsTableName = "rfm_results";
            List<RFMResult> rfmBatchResults = getRfmBatchResults(applicationId, session, rfmResultsTableName);
            List<RFMResult> rfmSpeedResults = getRfmSpeedResults(applicationId, session, rfmResultsTableName);

            if (rfmBatchResults == null || rfmBatchResults.isEmpty()) {
                return rfmSpeedResults;
            }

            if (rfmSpeedResults == null || rfmSpeedResults.isEmpty()) {
                return rfmBatchResults;
            }

            List<RFMResult> rfmMergedResults = new ArrayList<>();
            for (RFMResult rfmBatchResult : rfmBatchResults) {
                // Find same customer in batch and speed results
                Optional<RFMResult> rfmOptionalSpeedResult =
                        rfmSpeedResults.stream().filter(customer -> Objects
                                .equals(customer.getCustomerId(), rfmBatchResult.getCustomerId()))
                                .findFirst();

                RFMResult rfmSpeedResult = (rfmOptionalSpeedResult != null && rfmOptionalSpeedResult.isPresent()) ?
                        rfmOptionalSpeedResult.get() : null;

                if (rfmSpeedResult == null) {
                    if (rfmBatchResult != null) {
                        rfmMergedResults.add(rfmBatchResult);
                    }
                } else if (rfmBatchResult == null) {
                    rfmMergedResults.add(rfmSpeedResult);
                } else {
                    // Merge the batch and speed results together
                    RFMResult rfmMergedResult = merge(rfmBatchResult, rfmSpeedResult);
                    rfmMergedResults.add(rfmMergedResult);
                }
            }

            return rfmMergedResults;
        }
    }

    private static BigDecimal mergeAverageDaysBetweenOrders(BigDecimal averageDaysBetweenOrdersBatch,
                                                            BigDecimal averageDaysBetweenOrdersSpeed) {
        if (averageDaysBetweenOrdersBatch == null) {
            return averageDaysBetweenOrdersSpeed;
        }

        if (averageDaysBetweenOrdersSpeed == null) {
            return averageDaysBetweenOrdersBatch;
        }

        BigDecimal sumOfBatchAndSpeedAverageDaysBetweenOrders = averageDaysBetweenOrdersBatch
                .add(averageDaysBetweenOrdersSpeed);

        return sumOfBatchAndSpeedAverageDaysBetweenOrders.divide(BigDecimal.valueOf(2), 2, RoundingMode.HALF_UP)
                .setScale(2, RoundingMode.HALF_UP);
    }

    private static BigDecimal mergeAverageOrderPrice(BigDecimal averageOrderPriceBatch,
                                                     BigDecimal averageOrderPriceSpeed) {
        if (averageOrderPriceBatch == null) {
            return averageOrderPriceSpeed;
        }

        if (averageOrderPriceSpeed == null) {
            return averageOrderPriceBatch;
        }

        BigDecimal sumOfBatchAndSpeedResults = averageOrderPriceBatch.add(averageOrderPriceSpeed);

        return sumOfBatchAndSpeedResults.divide(BigDecimal.valueOf(2), 2, RoundingMode.HALF_UP)
                .setScale(2, RoundingMode.HALF_UP);
    }

    private static BigDecimal mergeLastOrderAmount(BigDecimal lastOrderAmountBatch, BigDecimal lastOrderAmountSpeed) {
        if (lastOrderAmountBatch == null) {
            return lastOrderAmountSpeed;
        }

        if (lastOrderAmountSpeed == null) {
            return lastOrderAmountBatch;
        }

        BigDecimal sumOfBatchAndSpeedResults = lastOrderAmountBatch.add(lastOrderAmountSpeed);

        return sumOfBatchAndSpeedResults.divide(BigDecimal.valueOf(2), 2, RoundingMode.HALF_UP)
                .setScale(2, RoundingMode.HALF_UP);
    }

    private static Integer mergeOrdersCount(Integer ordersCountBatch, Integer ordersCountSpeed) {
        if (ordersCountBatch == null) {
            return ordersCountSpeed;
        }

        if (ordersCountSpeed == null) {
            return ordersCountBatch;
        }

        return ordersCountBatch + ordersCountSpeed;
    }

    private static BigDecimal mergedOrdersSubTotal(BigDecimal ordersSubTotalBatch, BigDecimal ordersSubTotalSpeed) {
        if (ordersSubTotalBatch == null) {
            return ordersSubTotalSpeed;
        }

        if (ordersSubTotalSpeed == null) {
            return ordersSubTotalBatch;
        }

        return ordersSubTotalBatch.add(ordersSubTotalSpeed);
    }

    private static List<RFMResult> getRfmSpeedResults(Integer applicationId, Session session,
                                                      String rfmResultsTableName) {

        return getCqlRfmResults(applicationId, rfmResultsTableName, session, null);
    }

    private static List<RFMResult> getRfmBatchResults(Integer applicationId, Session session,
                                                      String rfmResultsTableName) {
        String lastRunDate = getLastRunDate(session);

        return getCqlRfmResults(applicationId, rfmResultsTableName, session, lastRunDate);
    }

    public static RFMResult merge(RFMResult rfmBatchResult, RFMResult rfmSpeedResult) {
        if (rfmBatchResult == null) {
            return rfmSpeedResult;
        }

        if (rfmSpeedResult == null) {
            return rfmBatchResult;
        }

        Integer applicationId = rfmSpeedResult.getApplicationId();
        BigInteger customerId = rfmSpeedResult.getCustomerId();

        // merge total number of average days between orders by adding the number of average days between orders from the batch and speed layers.
        BigDecimal numberOfAverageDaysBetweenOrdersBatch = rfmBatchResult.getAverageDaysBetweenOrders();
        BigDecimal numberOfAverageDaysBetweenOrdersSpeed = rfmSpeedResult.getAverageDaysBetweenOrders();
        BigDecimal totalAverageNumberOfDaysBetweenOrders =
                mergeAverageDaysBetweenOrders(numberOfAverageDaysBetweenOrdersBatch,
                                              numberOfAverageDaysBetweenOrdersSpeed);

        BigDecimal averageOrderPriceBatch = rfmBatchResult.getAverageOrderPrice();
        BigDecimal averageOrderPriceSpeed = rfmSpeedResult.getAverageOrderPrice();
        BigDecimal totalAverageOrdersPrice =
                mergeAverageOrderPrice(averageOrderPriceBatch, averageOrderPriceSpeed);

        String companyName = rfmBatchResult.getCompanyName();
        String customerCity = rfmBatchResult.getCustomerCity();
        String customerCountry = rfmBatchResult.getCustomerCountry();

        ZonedDateTime customerCreatedAtBatch = rfmBatchResult.getCustomerCreatedAt();
        ZonedDateTime customerCreatedAtSpeed = rfmSpeedResult.getCustomerCreatedAt();
        ZonedDateTime customerCreatedAt;
        BigDecimal firstOrderAmount;

        if (customerCreatedAtBatch == null) {
            customerCreatedAt = customerCreatedAtSpeed;
            firstOrderAmount = rfmSpeedResult.getFirstOrderAmount();
        } else if (customerCreatedAtSpeed == null) {
            customerCreatedAt = customerCreatedAtBatch;
            firstOrderAmount = rfmBatchResult.getFirstOrderAmount();
        } else {
            if (customerCreatedAtBatch.compareTo(customerCreatedAtSpeed) <= 0) {
                customerCreatedAt = customerCreatedAtBatch;
                firstOrderAmount = rfmBatchResult.getFirstOrderAmount();
            } else {
                customerCreatedAt = customerCreatedAtSpeed;
                firstOrderAmount = rfmSpeedResult.getFirstOrderAmount();
            }
        }

        String customerEmail = rfmBatchResult.getCustomerEmail();
        String customerGroup = rfmBatchResult.getCustomerGroup();
        String customerName = rfmBatchResult.getCustomerName();
        String customerState = rfmBatchResult.getCustomerState();

        ZonedDateTime firstOrderDateBatch = rfmBatchResult.getFirstOrderDate();
        ZonedDateTime firstOrderDateSpeed = rfmSpeedResult.getFirstOrderDate();
        ZonedDateTime firstOrderDate;

        if (firstOrderDateBatch == null) {
            firstOrderDate = firstOrderDateSpeed;
        } else if (firstOrderDateSpeed == null) {
            firstOrderDate = firstOrderDateBatch;
        } else {
            if (firstOrderDateBatch.compareTo(firstOrderDateSpeed) <= 0) {
                firstOrderDate = firstOrderDateBatch;
            } else {
                firstOrderDate = firstOrderDateSpeed;
            }
        }

        BigDecimal lastOrderAmountBatch = rfmBatchResult.getLastOrderAmount();
        BigDecimal lastOrderAmountSpeed = rfmSpeedResult.getLastOrderAmount();
        BigDecimal totalLastOrderAmount = mergeLastOrderAmount(lastOrderAmountBatch, lastOrderAmountSpeed);

        ZonedDateTime lastOrderDateBatch = rfmBatchResult.getLastOrderDate();
        ZonedDateTime lastOrderDateSpeed = rfmSpeedResult.getLastOrderDate();
        ZonedDateTime lastOrderDate;

        if (lastOrderDateBatch == null) {
            lastOrderDate = lastOrderDateSpeed;
        } else if (lastOrderDateSpeed == null) {
            lastOrderDate = lastOrderDateBatch;
        } else {
            if (lastOrderDateBatch.compareTo(lastOrderDateSpeed) > 0) {
                lastOrderDate = lastOrderDateBatch;
            } else {
                lastOrderDate = lastOrderDateSpeed;
            }
        }

        Integer ordersCountBatch = rfmBatchResult.getOrdersCount();
        Integer ordersCountSpeed = rfmSpeedResult.getOrdersCount();
        Integer totalOrdersCount = mergeOrdersCount(ordersCountBatch, ordersCountSpeed);

        BigDecimal ordersSubtotalBatch = rfmBatchResult.getOrdersSubTotal();
        BigDecimal ordersSubtotalSpeed = rfmSpeedResult.getOrdersSubTotal();
        BigDecimal ordersSubtotal = mergedOrdersSubTotal(ordersSubtotalBatch, ordersSubtotalSpeed);

        return new RFMResult(applicationId, customerId, totalAverageNumberOfDaysBetweenOrders,
                             totalAverageOrdersPrice, companyName, customerCity, customerCountry,
                             customerCreatedAt, customerEmail, customerGroup,
                             customerName, customerState, firstOrderAmount, firstOrderDate,
                             totalLastOrderAmount, lastOrderDate, totalOrdersCount, ordersSubtotal);
    }
}
