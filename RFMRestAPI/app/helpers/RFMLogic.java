package helpers;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import play.Play;
import play.libs.F.Promise;
import play.libs.ws.WSClient;
import play.libs.ws.WSRequest;
import play.libs.ws.WSResponse;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by brettcooper on 7/25/16.
 */
@Singleton
public class RFMLogic implements RFM {

    @Inject
    private WSClient wsClient;

    public static String getLastRunDate(Session session) {
        String lastRunDate = null;
        ResultSet allRunsResultsSet = session.execute("select id, last_run_date from rfm_last_run");

        if (allRunsResultsSet != null) {
            List<Row> allRunsResultsList = allRunsResultsSet.all();

            // Order dates by id, ascending
            allRunsResultsList.sort((row1, row2) -> {
                Integer row1Id = row1.getInt(0);
                Integer row2Id = row2.getInt(0);

                return row1Id.compareTo(row2Id);
            });

            if (!allRunsResultsList.isEmpty()) {
                int allRunsResultsSize = allRunsResultsList.size();
                Row lastRunRow = allRunsResultsList.get(allRunsResultsSize - 1);
                lastRunDate = lastRunRow.getString(1);
            }
        }

        return lastRunDate;
    }

    @Override
    public ObjectNode getSummaryStatistics(Integer applicationId) {
        return RFMSummaryStatisticsLogic.getSummaryStatistics(applicationId);
    }

    @Override
    public ArrayNode getEightyTwentySummaryStatistics(Integer applicationId) {
        return RFMEightyTwentyLogic.getEightyTwentySummaryStatistics(applicationId);
    }

    @Override
    public ArrayNode getRfmResults(Integer applicationId) {
        return RFMResultsLogic.getRfmResults(applicationId);
    }

    @Override
    public Boolean checkForValidApplicationAndCompany(Integer applicationId, String companyAppName) {
        String checkApplicationAndCompanyAppNameBaseURL = Play.application().configuration().underlying()
                .getString("revenueconduit.checkApplicationAndCompanyAppName.url");
        String checkApplicationAndCompanyAppNameURL =
                String.format("%s%s/%s/", checkApplicationAndCompanyAppNameBaseURL, applicationId, companyAppName);
        String checkApplicationAndCompanyAppNameTimeout = Play.application().configuration().underlying()
                .getString("revenueconduit.checkApplicationAndCompanyAppName.timeout");
        Integer timeout = Integer.parseInt(checkApplicationAndCompanyAppNameTimeout);

        WSRequest request = wsClient.url(checkApplicationAndCompanyAppNameURL);
        Promise<WSResponse> wsResponsePromise = request.get();
        WSResponse wsResponse = wsResponsePromise.get(timeout);
        String body = wsResponse.getBody();

        if (body != null && !body.isEmpty()) {
            Integer returnValue;

            try {
                Pattern newLinePattern = Pattern.compile("\n", Pattern.CASE_INSENSITIVE);
                String bodyWithoutNewlines = newLinePattern.matcher(body).replaceAll("");

                returnValue = Integer.parseInt(bodyWithoutNewlines);
            } catch (NumberFormatException ex) {
                returnValue = 0;
            }

            return returnValue == 1;
        } else {
            return false;
        }
    }
}






