package helpers;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.inject.ImplementedBy;

/**
 * Created by brettcooper on 7/25/16.
 */
@ImplementedBy(RFMLogic.class)
public interface RFM {
    ObjectNode getSummaryStatistics(Integer applicationId);

    ArrayNode getEightyTwentySummaryStatistics(Integer applicationId);

    Boolean checkForValidApplicationAndCompany(Integer applicationId, String companyAppName);

    ArrayNode getRfmResults(Integer applicationId);
}
