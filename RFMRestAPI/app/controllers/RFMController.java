package controllers;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.inject.Inject;
import helpers.RFM;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import play.mvc.Controller;
import play.mvc.Result;

/**
 * Created by brettcooper on 7/1/16.
 */
@Api(value = "/rfm", description = "Gets RFM Summary Statistics and 80/20 Summary Statistics data for a given application and company app name." )
public class RFMController extends Controller {
    private final RFM rfmLogic;

    @Inject
    public RFMController(RFM rfmLogic) {
        this.rfmLogic = rfmLogic;
    }

    @ApiOperation(
            value = "index",
            notes = "Tests that the RFM API is up.",
            response = Result.class,
            httpMethod = "GET"
    )
    public Result index() {
        return ok();
    }

    @ApiOperation(
            value = "summaryStatistics",
            notes = "Gets the Summary Statistics for the given application and company app name.",
            response = Result.class,
            httpMethod = "GET"
    )
    public Result summaryStatistics(
            @ApiParam(value = "The company's application id.", required = true) Integer applicationId,
            @ApiParam(value = "The company's application name.", required = true) String companyAppName) {
        Boolean isValidApplicationAndCompanyAppName = rfmLogic
                .checkForValidApplicationAndCompany(applicationId, companyAppName);

        // Security: Check for a valid application id and company app name before retrieving the data
        if (isValidApplicationAndCompanyAppName) {
            ObjectNode summaryStatistics = rfmLogic.getSummaryStatistics(applicationId);

            return ok(summaryStatistics);
        }
        else {
            return notFound();
        }
    }

    @ApiOperation(
            value = "eightyTwentySummaryStatistics",
            notes = "Gets the 80/20 Summary Statistics for the given application and company app name.",
            response = Result.class,
            httpMethod = "GET"
    )
    public Result eightyTwentySummaryStatistics(
            @ApiParam(value = "The company's application id.", required = true) Integer applicationId,
            @ApiParam(value = "The company's application name.", required = true) String companyAppName) {
        Boolean isValidApplicationAndCompanyAppName = rfmLogic
                .checkForValidApplicationAndCompany(applicationId, companyAppName);

        // Security: Check for a valid application id and company app name before retrieving the data
        if (isValidApplicationAndCompanyAppName) {
            ArrayNode eightyTwentySummaryStatistics = rfmLogic.getEightyTwentySummaryStatistics(applicationId);

            return ok(eightyTwentySummaryStatistics);
        }
        else {
            return notFound();
        }
    }

    @ApiOperation(
            value = "rfmResults",
            notes = "Gets the RFM results for the given application and company app name.",
            response = Result.class,
            httpMethod = "GET"
    )
    public Result rfmResults(@ApiParam(value = "The company's application id.", required = true) Integer applicationId,
                             @ApiParam(value = "The company's application name.", required = true) String companyAppName) {
        Boolean isValidApplicationAndCompanyAppName = rfmLogic
                .checkForValidApplicationAndCompany(applicationId, companyAppName);

        // Security: Check for a valid application id and company app name before retrieving the data
        if (isValidApplicationAndCompanyAppName) {
            ArrayNode rfmResults = rfmLogic.getRfmResults(applicationId);

            return ok(rfmResults);
        }
        else {
            return notFound();
        }
    }
}