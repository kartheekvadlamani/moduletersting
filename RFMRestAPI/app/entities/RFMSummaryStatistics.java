package entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Created by brettcooper on 5/18/16.
 */
public class RFMSummaryStatistics implements Serializable {
    private final Integer applicationId;
    private final long totalCustomers;
    private final BigDecimal totalAmountOfOrders;
    private final BigDecimal percentageTotalAmountOfOrders;
    private final BigDecimal averageOrderValue;
    private final BigDecimal averageCustomerLifetimeValue;
    private final Integer totalNumberOfOrders;
    private final BigDecimal percentageTotalNumberOfOrders;
    private final BigDecimal averageNumberOfOrders;
    private final BigDecimal percentageOfRepeatBuyers;
    private final BigDecimal percentageRevenueFromRepeatBuyers;
    private final Integer averageDaysSinceLastOrder;

    public RFMSummaryStatistics(Integer applicationId, long totalCustomers, BigDecimal totalAmountOfOrders, BigDecimal percentageTotalAmountOfOrders, BigDecimal averageOrderValue, BigDecimal averageCustomerLifetimeValue, Integer totalNumberOfOrders, BigDecimal percentageTotalNumberOfOrders,
                                BigDecimal averageNumberOfOrders, BigDecimal percentageOfRepeatBuyers, BigDecimal percentageRevenueFromRepeatBuyers, Integer averageDaysSinceLastOrder)
    {
        this.applicationId = applicationId;
        this.totalCustomers = totalCustomers;
        this.totalAmountOfOrders = (totalAmountOfOrders != null) ? totalAmountOfOrders.setScale(2, RoundingMode.HALF_UP) : null;
        this.percentageTotalAmountOfOrders = (percentageTotalAmountOfOrders != null) ? percentageTotalAmountOfOrders.setScale(2, RoundingMode.HALF_UP) : null;
        this.averageOrderValue = (averageOrderValue != null) ? averageOrderValue.setScale(2, RoundingMode.HALF_UP) : null;
        this.averageCustomerLifetimeValue = (averageCustomerLifetimeValue != null) ? averageCustomerLifetimeValue.setScale(2, RoundingMode.HALF_UP) : null;
        this.totalNumberOfOrders = totalNumberOfOrders;
        this.percentageTotalNumberOfOrders = (percentageTotalNumberOfOrders != null) ? percentageTotalNumberOfOrders.setScale(2, RoundingMode.HALF_UP) : null;
        this.averageNumberOfOrders = averageNumberOfOrders;
        this.percentageOfRepeatBuyers = (percentageOfRepeatBuyers != null) ? percentageOfRepeatBuyers.setScale(2, RoundingMode.HALF_UP) : null;
        this.percentageRevenueFromRepeatBuyers = (percentageRevenueFromRepeatBuyers != null) ? percentageRevenueFromRepeatBuyers.setScale(2, RoundingMode.HALF_UP) : null;
        this.averageDaysSinceLastOrder = averageDaysSinceLastOrder;
    }

    public Integer getApplicationId() { return applicationId; }
    public long getTotalCustomers() { return totalCustomers; }
    public BigDecimal getTotalAmountOfOrders() { return  totalAmountOfOrders; }
    public BigDecimal getPercentageTotalAmountOfOrders() { return percentageTotalAmountOfOrders; }
    public BigDecimal getAverageOrderValue() { return averageOrderValue; }
    public BigDecimal getAverageCustomerLifetimeValue() { return averageCustomerLifetimeValue; }
    public Integer getTotalNumberOfOrders() { return totalNumberOfOrders; }
    public BigDecimal getPercentageTotalNumberOfOrders() { return percentageTotalNumberOfOrders; }
    public BigDecimal getAverageNumberOfOrders() { return averageNumberOfOrders; }
    public BigDecimal getPercentageOfRepeatBuyers() { return percentageOfRepeatBuyers; }
    public BigDecimal getPercentageRevenueFromRepeatBuyers() { return percentageRevenueFromRepeatBuyers; }
    public Integer getAverageDaysSinceLastOrder() { return averageDaysSinceLastOrder; }

    public String toString() {
        return String.format("%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s", applicationId, totalCustomers, totalAmountOfOrders, percentageTotalAmountOfOrders, averageOrderValue, averageCustomerLifetimeValue, totalNumberOfOrders, percentageTotalNumberOfOrders, averageNumberOfOrders, percentageOfRepeatBuyers,
                percentageRevenueFromRepeatBuyers, averageDaysSinceLastOrder);
    }
}

