package entities;


import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.time.ZonedDateTime;

/**
 * Created by kartheekvadlamani on 2/22/17.
 */
public class RFMResult implements Serializable {

    private final Integer applicationId;
    private final BigInteger customerId;
    private final BigDecimal averageDaysBetweenOrders;
    private final BigDecimal averageOrderPrice;
    private final String companyName;
    private final String customerCity;
    private final String customerCountry;
    private final ZonedDateTime customerCreatedAt;
    private final String customerEmail;
    private final String customerGroup;
    private final String customerName;
    private final String customerState;
    private final BigDecimal firstOrderAmount;
    private final ZonedDateTime firstOrderDate;
    private final BigDecimal lastOrderAmount;
    private final ZonedDateTime lastOrderDate;
    private final Integer ordersCount;
    private final BigDecimal ordersSubTotal;

    public RFMResult(Integer applicationId, BigInteger customerId, BigDecimal averageDaysBetweenOrders,
                     BigDecimal averageOrderPrice, String companyName, String customerCity, String customerCountry,
                     ZonedDateTime customerCreatedAt, String customerEmail,
                     String customerGroup, String customerName, String customerState, BigDecimal firstOrderAmount,
                     ZonedDateTime firstOrderDate, BigDecimal lastOrderAmount, ZonedDateTime lastOrderDate,
                     Integer ordersCount, BigDecimal ordersSubTotal)
    {
        this.applicationId = applicationId;
        this.customerId = customerId;
        this.averageDaysBetweenOrders = (averageDaysBetweenOrders != null) ? averageDaysBetweenOrders.setScale(2, RoundingMode.HALF_UP): null;
        this.averageOrderPrice =  (averageOrderPrice != null) ? averageOrderPrice.setScale(2, RoundingMode.HALF_UP): null;
        this.companyName = companyName;
        this.customerCity = customerCity;
        this.customerCountry = customerCountry;
        this.customerCreatedAt = customerCreatedAt;
        this.customerEmail = customerEmail;
        this.customerGroup = customerGroup;
        this.customerName = customerName;
        this.customerState = customerState;
        this.firstOrderAmount = (firstOrderAmount != null) ? firstOrderAmount.setScale(2, RoundingMode.HALF_UP): null;
        this.firstOrderDate = firstOrderDate;
        this.lastOrderAmount = lastOrderAmount;
        this.lastOrderDate = lastOrderDate;
        this.ordersCount = ordersCount;
        this.ordersSubTotal =  (ordersSubTotal != null) ? ordersSubTotal.setScale(2, RoundingMode.HALF_UP): null;
    }

    public String toString() {
        return  String.format("%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s", applicationId, customerId, averageDaysBetweenOrders, averageOrderPrice, companyName, customerCity, customerCountry, customerCreatedAt, customerEmail, customerGroup, customerName, customerState, firstOrderAmount, firstOrderDate, lastOrderAmount, lastOrderDate, ordersCount, ordersSubTotal);

    }

    public Integer getApplicationId() {
        return applicationId;
    }

    public BigInteger getCustomerId() {
        return customerId;
    }

    public BigDecimal getAverageDaysBetweenOrders() {
        return averageDaysBetweenOrders;
    }

    public BigDecimal getAverageOrderPrice() {
        return averageOrderPrice;
    }

    public String getCompanyName() {
        return companyName;
    }

    public String getCustomerCity() {
        return customerCity;
    }

    public String getCustomerCountry() {
        return customerCountry;
    }

    public ZonedDateTime getCustomerCreatedAt() {
        return customerCreatedAt;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public String getCustomerGroup() {
        return customerGroup;
    }

    public String getCustomerName() {
        return customerName;
    }

    public String getCustomerState() {
        return customerState;
    }

    public BigDecimal getFirstOrderAmount() {
        return firstOrderAmount;
    }

    public ZonedDateTime getFirstOrderDate() {
        return firstOrderDate;
    }

    public BigDecimal getLastOrderAmount() {
        return lastOrderAmount;
    }

    public ZonedDateTime getLastOrderDate() {
        return lastOrderDate;
    }

    public Integer getOrdersCount() {
        return ordersCount;
    }

    public BigDecimal getOrdersSubTotal() {
        return ordersSubTotal;
    }
}
