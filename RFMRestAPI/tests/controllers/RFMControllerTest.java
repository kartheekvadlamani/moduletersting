package controllers;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import helpers.RFM;
import org.junit.Test;
import play.core.j.JavaResultExtractor;
import play.libs.Json;
import play.mvc.Http.Status;
import play.mvc.Result;
import play.test.FakeApplication;
import play.test.Helpers;
import play.test.WithApplication;
import java.io.File;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashMap;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class RFMControllerTest extends WithApplication {
    @Override
    protected FakeApplication provideFakeApplication() {
        return new FakeApplication(new File("."), Helpers.class.getClassLoader(), new HashMap<>(), new ArrayList<>(),
                                   null);
    }

    @Test
    public void indexTest() {
        // Arrange
        RFM rfmLogic = mock(RFM.class);

        // Act
        Result result = new RFMController(rfmLogic).index();

        // Assert
        assertEquals(Status.OK, result.status());
    }

    @Test
    public void summaryStatisticsTest() {
        Integer applicationId = 1000;
        String companyAppName = "abcd";

        // Arrange
        RFM rfmLogic = mock(RFM.class);
        ObjectNode rfmSummaryStatistics = getRfmSummaryStatistics();

        when(rfmLogic.getSummaryStatistics(applicationId)).thenReturn(rfmSummaryStatistics);
        when(rfmLogic.checkForValidApplicationAndCompany(applicationId, companyAppName)).thenReturn(true);

        // Act
        Result result = new RFMController(rfmLogic).summaryStatistics(applicationId, companyAppName);

        // Assert
        assertEquals(Status.OK, result.status());

        String expectedBodyJson = rfmSummaryStatistics.toString();
        String bodyJson = getJSONString(result);

        assertEquals(expectedBodyJson, bodyJson);
    }

    @Test
    public void summaryStatisticsNotFoundTest() {
        Integer applicationId = 1000;
        String companyAppName = "abcd";

        // Arrange
        RFM rfmLogic = mock(RFM.class);
        ObjectNode rfmSummaryStatistics = getRfmSummaryStatistics();
        when(rfmLogic.getSummaryStatistics(applicationId)).thenReturn(rfmSummaryStatistics);
        when(rfmLogic.checkForValidApplicationAndCompany(applicationId, companyAppName)).thenReturn(false);

        // Act
        Result result = new RFMController(rfmLogic).summaryStatistics(applicationId, companyAppName);

        // Assert
        assertEquals(Status.NOT_FOUND, result.status());
    }

    @Test
    public void rfmResultsTest() {
        Integer applicationId = 1000;
        String companyAppName = "abcd";

        // Arrange
        RFM rfmLogic = mock(RFM.class);
        ArrayNode rfmResults = getRfmResults();

        when(rfmLogic.getRfmResults(applicationId)).thenReturn(rfmResults);
        when(rfmLogic.checkForValidApplicationAndCompany(applicationId, companyAppName)).thenReturn(true);

        // Act
        Result result = new RFMController(rfmLogic).rfmResults(applicationId, companyAppName);

        // Assert
        assertEquals(Status.OK, result.status());

        String expectedBodyJson = rfmResults.toString();
        String bodyJson = getJSONString(result);

        assertEquals(expectedBodyJson, bodyJson);
    }

    @Test
    public void rfmResultsNotFoundTest() {
        Integer applicationId = 1000;
        String companyAppName = "abcd";

        // Arrange
        RFM rfmLogic = mock(RFM.class);
        ArrayNode rfmResults = getRfmResults();

        when(rfmLogic.getRfmResults(applicationId)).thenReturn(rfmResults);
        when(rfmLogic.checkForValidApplicationAndCompany(applicationId, companyAppName)).thenReturn(false);

        // Act
        Result result = new RFMController(rfmLogic).rfmResults(applicationId, companyAppName);

        // Assert
        assertEquals(Status.NOT_FOUND, result.status());
    }

    @Test
    public void eightyTwentyStatisticsTest() {
        Integer applicationId = 1001;
        String companyAppName = "abcd";

        // Arrange
        RFM rfmLogic = mock(RFM.class);

        ObjectNode rfmTop1PercentSummaryStatistics = getRfmSummaryStatisticsTop1();
        ObjectNode rfmTop5PercentSummaryStatistics = getRfmSummaryStatisticsTop5();
        ObjectNode rfmTop15PercentSummaryStatistics = getRfmSummaryStatisticsTop15();
        ObjectNode rfmTop25PercentSummaryStatistics = getRfmSummaryStatisticsTop25();
        ObjectNode rfmTop50PercentSummaryStatistics = getRfmSummaryStatisticsTop50();
        ObjectNode rfmBottom50PercentSummaryStatistics = getRfmSummaryStatisticsBottom50();

        ArrayNode jsonNodes = Json.newArray();
        jsonNodes.add(rfmTop1PercentSummaryStatistics);
        jsonNodes.add(rfmTop5PercentSummaryStatistics);
        jsonNodes.add(rfmTop15PercentSummaryStatistics);
        jsonNodes.add(rfmTop25PercentSummaryStatistics);
        jsonNodes.add(rfmTop50PercentSummaryStatistics);
        jsonNodes.add(rfmBottom50PercentSummaryStatistics);

        when(rfmLogic.getEightyTwentySummaryStatistics(applicationId)).thenReturn(jsonNodes);
        when(rfmLogic.checkForValidApplicationAndCompany(applicationId, companyAppName)).thenReturn(true);

        // Act
        Result result = new RFMController(rfmLogic).eightyTwentySummaryStatistics(applicationId, companyAppName);

        // Assert
        assertEquals(Status.OK, result.status());

        String expectedBodyJson = jsonNodes.toString();
        String bodyJson = getJSONString(result);

        assertEquals(expectedBodyJson, bodyJson);
    }

    @Test
    public void eightyTwentyStatisticsNotFoundTest() {
        Integer applicationId = 1002;
        String companyAppName = "abcd";

        // Arrange
        RFM rfmLogic = mock(RFM.class);

        ObjectNode rfmTop1PercentSummaryStatistics = getRfmSummaryStatisticsTop1();
        ObjectNode rfmTop5PercentSummaryStatistics = getRfmSummaryStatisticsTop5();
        ObjectNode rfmTop15PercentSummaryStatistics = getRfmSummaryStatisticsTop15();
        ObjectNode rfmTop25PercentSummaryStatistics = getRfmSummaryStatisticsTop25();
        ObjectNode rfmTop50PercentSummaryStatistics = getRfmSummaryStatisticsTop50();
        ObjectNode rfmBottom50PercentSummaryStatistics = getRfmSummaryStatisticsBottom50();

        ArrayNode jsonNodes = Json.newArray();
        jsonNodes.add(rfmTop1PercentSummaryStatistics);
        jsonNodes.add(rfmTop5PercentSummaryStatistics);
        jsonNodes.add(rfmTop15PercentSummaryStatistics);
        jsonNodes.add(rfmTop25PercentSummaryStatistics);
        jsonNodes.add(rfmTop50PercentSummaryStatistics);
        jsonNodes.add(rfmBottom50PercentSummaryStatistics);


        when(rfmLogic.getEightyTwentySummaryStatistics(applicationId)).thenReturn(jsonNodes);
        when(rfmLogic.checkForValidApplicationAndCompany(applicationId, companyAppName)).thenReturn(false);

        // Act
        Result result = new RFMController(rfmLogic).eightyTwentySummaryStatistics(applicationId, companyAppName);

        // Assert
        assertEquals(Status.NOT_FOUND, result.status());
    }

    private ObjectNode getRfmSummaryStatistics() {
        ObjectNode rfmSummaryStatistics = Json.newObject();

        rfmSummaryStatistics.put("application_id", 1000);
        rfmSummaryStatistics.put("average_customer_lifetime_value", 100.00);
        rfmSummaryStatistics.put("average_days_since_last_order", 5);
        rfmSummaryStatistics.put("average_number_of_orders", 2);
        rfmSummaryStatistics.put("average_order_value", 15.00);
        rfmSummaryStatistics.put("percentage_of_repeat_buyers", 0.10);
        rfmSummaryStatistics.put("percentage_revenue_from_repeat_buyers", 5.00);
        rfmSummaryStatistics.put("percentage_total_amount_of_orders", 1000);
        rfmSummaryStatistics.put("percentage_total_number_of_orders", 0.50);
        rfmSummaryStatistics.put("total_amount_of_orders", 5);
        rfmSummaryStatistics.put("total_customers", 30);
        rfmSummaryStatistics.put("total_number_of_orders", 50);

        return rfmSummaryStatistics;
    }

    private ArrayNode getRfmResults() {
        ZonedDateTime now = ZonedDateTime.now(ZoneId.of("UTC"));

        ArrayNode jsonNodes = Json.newArray();
        ObjectNode rfmResults = Json.newObject();

        rfmResults.put("application_id", 1000);
        rfmResults.put("customer_id", 12345.00);
        rfmResults.put("average_days_between_orders", 4.00);
        rfmResults.put("average_order_price", 134556.00);
        rfmResults.put("company_name", "Revenue_conduit");
        rfmResults.put("customer_city", "Akron");
        rfmResults.put("customer_country", "USA");
        rfmResults.put("customer_created_at", now.toString());
        rfmResults.put("customer_email", "kartheek.vadlamani@revenueconduit.com");
        rfmResults.put("customer_group", "Electronics");
        rfmResults.put("customer_name", "Kartheek");
        rfmResults.put("customer_state", "Ohio");
        rfmResults.put("first_order_amount", 14567.88);
        rfmResults.put("first_order_date", now.toString());
        rfmResults.put("last_order_amount", 14567.00);
        rfmResults.put("last_order_date", now.toString());
        rfmResults.put("orders_count", 14);
        rfmResults.put("orders_sub_total", 13546.00);

        jsonNodes.add(rfmResults);

        return jsonNodes;
    }

    private String getJSONString(Result result) {
        byte[] body = JavaResultExtractor.getBody(result, 0L);

        String header = JavaResultExtractor.getHeaders(result).get("Content-Type");
        String charset = "utf-8";

        if (header != null && header.contains("; charset=")) {
            charset = header.substring(header.indexOf("; charset=") + 10, header.length()).trim();
        }

        String bodyStr = new String(body);

        return bodyStr;
    }

    private ObjectNode getRfmSummaryStatisticsTop1() {
        ObjectNode rfmTop1PercentSummaryStatistics = Json.newObject();

        rfmTop1PercentSummaryStatistics.put("application_id", 1001);
        rfmTop1PercentSummaryStatistics.put("average_customer_lifetime_value", 1.00);
        rfmTop1PercentSummaryStatistics.put("average_days_since_last_order", 1);
        rfmTop1PercentSummaryStatistics.put("average_number_of_orders", 1);
        rfmTop1PercentSummaryStatistics.put("average_order_value", 1.00);
        rfmTop1PercentSummaryStatistics.put("percentage_of_repeat_buyers", 0.1);
        rfmTop1PercentSummaryStatistics.put("percentage_revenue_from_repeat_buyers", 1.00);
        rfmTop1PercentSummaryStatistics.put("percentage_total_amount_of_orders", 1000);
        rfmTop1PercentSummaryStatistics.put("percentage_total_number_of_orders", 0.1);
        rfmTop1PercentSummaryStatistics.put("total_amount_of_orders", 1);
        rfmTop1PercentSummaryStatistics.put("total_customers", 1);
        rfmTop1PercentSummaryStatistics.put("total_number_of_orders", 1);

        return rfmTop1PercentSummaryStatistics;
    }

    private ObjectNode getRfmSummaryStatisticsTop5() {
        ObjectNode rfmTop5PercentSummaryStatistics = Json.newObject();

        rfmTop5PercentSummaryStatistics.put("application_id", 1001);
        rfmTop5PercentSummaryStatistics.put("average_customer_lifetime_value", 5.00);
        rfmTop5PercentSummaryStatistics.put("average_days_since_last_order", 5);
        rfmTop5PercentSummaryStatistics.put("average_number_of_orders", 5);
        rfmTop5PercentSummaryStatistics.put("average_order_value", 5.00);
        rfmTop5PercentSummaryStatistics.put("percentage_of_repeat_buyers", 0.50);
        rfmTop5PercentSummaryStatistics.put("percentage_revenue_from_repeat_buyers", 5.00);
        rfmTop5PercentSummaryStatistics.put("percentage_total_amount_of_orders", 5000);
        rfmTop5PercentSummaryStatistics.put("percentage_total_number_of_orders", 0.50);
        rfmTop5PercentSummaryStatistics.put("total_amount_of_orders", 5);
        rfmTop5PercentSummaryStatistics.put("total_customers", 5);
        rfmTop5PercentSummaryStatistics.put("total_number_of_orders", 5);

        return rfmTop5PercentSummaryStatistics;
    }

    private ObjectNode getRfmSummaryStatisticsTop15() {
        ObjectNode rfmTop15PercentSummaryStatistics = Json.newObject();

        rfmTop15PercentSummaryStatistics.put("application_id", 1001);
        rfmTop15PercentSummaryStatistics.put("average_customer_lifetime_value", 15.00);
        rfmTop15PercentSummaryStatistics.put("average_days_since_last_order", 15);
        rfmTop15PercentSummaryStatistics.put("average_number_of_orders", 15);
        rfmTop15PercentSummaryStatistics.put("average_order_value", 15.00);
        rfmTop15PercentSummaryStatistics.put("percentage_of_repeat_buyers", 0.15);
        rfmTop15PercentSummaryStatistics.put("percentage_revenue_from_repeat_buyers", 15.00);
        rfmTop15PercentSummaryStatistics.put("percentage_total_amount_of_orders", 15000);
        rfmTop15PercentSummaryStatistics.put("percentage_total_number_of_orders", 0.15);
        rfmTop15PercentSummaryStatistics.put("total_amount_of_orders", 15);
        rfmTop15PercentSummaryStatistics.put("total_customers", 15);
        rfmTop15PercentSummaryStatistics.put("total_number_of_orders", 15);

        return rfmTop15PercentSummaryStatistics;
    }

    private ObjectNode getRfmSummaryStatisticsTop25() {
        ObjectNode rfmTop25PercentSummaryStatistics = Json.newObject();

        rfmTop25PercentSummaryStatistics.put("application_id", 1001);
        rfmTop25PercentSummaryStatistics.put("average_customer_lifetime_value", 25.00);
        rfmTop25PercentSummaryStatistics.put("average_days_since_last_order", 25);
        rfmTop25PercentSummaryStatistics.put("average_number_of_orders", 25);
        rfmTop25PercentSummaryStatistics.put("average_order_value", 25.00);
        rfmTop25PercentSummaryStatistics.put("percentage_of_repeat_buyers", 0.25);
        rfmTop25PercentSummaryStatistics.put("percentage_revenue_from_repeat_buyers", 25.00);
        rfmTop25PercentSummaryStatistics.put("percentage_total_amount_of_orders", 25000);
        rfmTop25PercentSummaryStatistics.put("percentage_total_number_of_orders", 0.25);
        rfmTop25PercentSummaryStatistics.put("total_amount_of_orders", 25);
        rfmTop25PercentSummaryStatistics.put("total_customers", 25);
        rfmTop25PercentSummaryStatistics.put("total_number_of_orders", 25);

        return rfmTop25PercentSummaryStatistics;
    }

    private ObjectNode getRfmSummaryStatisticsTop50() {
        ObjectNode rfmTop50PercentSummaryStatistics = Json.newObject();

        rfmTop50PercentSummaryStatistics.put("application_id", 1001);
        rfmTop50PercentSummaryStatistics.put("average_customer_lifetime_value", 50.00);
        rfmTop50PercentSummaryStatistics.put("average_days_since_last_order", 50);
        rfmTop50PercentSummaryStatistics.put("average_number_of_orders", 50);
        rfmTop50PercentSummaryStatistics.put("average_order_value", 50.00);
        rfmTop50PercentSummaryStatistics.put("percentage_of_repeat_buyers", 0.50);
        rfmTop50PercentSummaryStatistics.put("percentage_revenue_from_repeat_buyers", 50.00);
        rfmTop50PercentSummaryStatistics.put("percentage_total_amount_of_orders", 50000);
        rfmTop50PercentSummaryStatistics.put("percentage_total_number_of_orders", 0.50);
        rfmTop50PercentSummaryStatistics.put("total_amount_of_orders", 50);
        rfmTop50PercentSummaryStatistics.put("total_customers", 50);
        rfmTop50PercentSummaryStatistics.put("total_number_of_orders", 50);

        return rfmTop50PercentSummaryStatistics;
    }

    private ObjectNode getRfmSummaryStatisticsBottom50() {
        ObjectNode rfmBottom50PercentSummaryStatistics = Json.newObject();

        rfmBottom50PercentSummaryStatistics.put("application_id", 1001);
        rfmBottom50PercentSummaryStatistics.put("average_customer_lifetime_value", 50.00);
        rfmBottom50PercentSummaryStatistics.put("average_days_since_last_order", 5);
        rfmBottom50PercentSummaryStatistics.put("average_number_of_orders", 2);
        rfmBottom50PercentSummaryStatistics.put("average_order_value", 15.00);
        rfmBottom50PercentSummaryStatistics.put("percentage_of_repeat_buyers", 0.10);
        rfmBottom50PercentSummaryStatistics.put("percentage_revenue_from_repeat_buyers", 5.00);
        rfmBottom50PercentSummaryStatistics.put("percentage_total_amount_of_orders", 1000);
        rfmBottom50PercentSummaryStatistics.put("percentage_total_number_of_orders", 0.50);
        rfmBottom50PercentSummaryStatistics.put("total_amount_of_orders", 5);
        rfmBottom50PercentSummaryStatistics.put("total_customers", 30);
        rfmBottom50PercentSummaryStatistics.put("total_number_of_orders", 50);

        return rfmBottom50PercentSummaryStatistics;
    }
}

