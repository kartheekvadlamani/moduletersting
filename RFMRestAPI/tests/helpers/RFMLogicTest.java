package helpers;

import entities.RFMResult;
import org.junit.Test;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.ZoneId;
import java.time.ZonedDateTime;

import static org.junit.Assert.assertEquals;

/**
 * Created by brettcooper on 2/27/17.
 */
public class RFMLogicTest {

    @Test
    public void mergeRfmResultsSpeedResultIsNullTest() {
        ZonedDateTime now = ZonedDateTime.now(ZoneId.of("UTC"));
        ZonedDateTime yesterday = now.minusDays(1L);
        Integer applicationId = 1000;
        BigInteger customerId = BigInteger.valueOf(1);
        String companyName = "test company";
        String customerEmail = "test@test.com";
        String customerCity = "Akron";
        String customerCountry = "USA";
        String customerName = "test customer";
        String customerState = "OH";

        RFMResult rfmBatchResult =
                new RFMResult(applicationId, customerId, BigDecimal.valueOf(5), BigDecimal.valueOf(14.99d),
                              companyName, customerCity, customerCountry, yesterday, customerEmail, null,
                              customerName, customerState, BigDecimal.valueOf(5.00d), yesterday,
                              BigDecimal.valueOf(10.00d), yesterday, 3, BigDecimal.valueOf(20.00d));

        RFMResult rfmSpeedResult = null;

        RFMResult mergedRfmResult = RFMResultsLogic.merge(rfmBatchResult, rfmSpeedResult);

        assertEquals(applicationId, mergedRfmResult.getApplicationId());
        assertEquals(customerId, mergedRfmResult.getCustomerId());
        assertEquals(rfmBatchResult.getAverageDaysBetweenOrders(), mergedRfmResult.getAverageDaysBetweenOrders());
        assertEquals(rfmBatchResult.getAverageOrderPrice(), mergedRfmResult.getAverageOrderPrice());
        assertEquals(companyName, mergedRfmResult.getCompanyName());
        assertEquals(customerCity, mergedRfmResult.getCustomerCity());
        assertEquals(rfmBatchResult.getCustomerCreatedAt(), mergedRfmResult.getCustomerCreatedAt());
        assertEquals(customerEmail, mergedRfmResult.getCustomerEmail());
        assertEquals(null, mergedRfmResult.getCustomerGroup());
        assertEquals(customerName, mergedRfmResult.getCustomerName());
        assertEquals(companyName, mergedRfmResult.getCompanyName());
        assertEquals(customerState, mergedRfmResult.getCustomerState());
        assertEquals(rfmBatchResult.getFirstOrderAmount(), mergedRfmResult.getFirstOrderAmount());
        assertEquals(yesterday, mergedRfmResult.getFirstOrderDate());
        assertEquals(rfmBatchResult.getLastOrderDate(), mergedRfmResult.getLastOrderDate());
        assertEquals(rfmBatchResult.getOrdersCount(), mergedRfmResult.getOrdersCount());
        assertEquals(rfmBatchResult.getOrdersSubTotal(), mergedRfmResult.getOrdersSubTotal());
    }

    @Test
    public void mergeRfmResultsBatchResultIsNullTest() {
        ZonedDateTime now = ZonedDateTime.now(ZoneId.of("UTC"));
        Integer applicationId = 1000;
        BigInteger customerId = BigInteger.valueOf(1);
        String companyName = "test company";
        String customerEmail = "test@test.com";
        String customerCity = "Akron";
        String customerCountry = "USA";
        String customerName = "test customer";
        String customerState = "OH";

        RFMResult rfmBatchResult = null;

        RFMResult rfmSpeedResult =
                new RFMResult(applicationId, customerId, BigDecimal.valueOf(2), BigDecimal.valueOf(5.99d),
                              companyName, customerCity, customerCountry, now, customerEmail, null,
                              customerName, customerState, BigDecimal.valueOf(3.00d), now, BigDecimal.valueOf(4.99d),
                              now, 1, BigDecimal.valueOf(14.55d));

        RFMResult mergedRfmResult = RFMResultsLogic.merge(rfmBatchResult, rfmSpeedResult);

        assertEquals(applicationId, mergedRfmResult.getApplicationId());
        assertEquals(customerId, mergedRfmResult.getCustomerId());
        assertEquals(rfmSpeedResult.getAverageDaysBetweenOrders(), mergedRfmResult.getAverageDaysBetweenOrders());
        assertEquals(rfmSpeedResult.getAverageOrderPrice(), mergedRfmResult.getAverageOrderPrice());
        assertEquals(companyName, mergedRfmResult.getCompanyName());
        assertEquals(customerCity, mergedRfmResult.getCustomerCity());
        assertEquals(now, mergedRfmResult.getCustomerCreatedAt());
        assertEquals(customerEmail, mergedRfmResult.getCustomerEmail());
        assertEquals(null, mergedRfmResult.getCustomerGroup());
        assertEquals(customerName, mergedRfmResult.getCustomerName());
        assertEquals(companyName, mergedRfmResult.getCompanyName());
        assertEquals(customerState, mergedRfmResult.getCustomerState());
        assertEquals(rfmSpeedResult.getFirstOrderAmount(), mergedRfmResult.getFirstOrderAmount());
        assertEquals(rfmSpeedResult.getFirstOrderDate(), mergedRfmResult.getFirstOrderDate());
        assertEquals(now, mergedRfmResult.getLastOrderDate());
        assertEquals(rfmSpeedResult.getOrdersCount(), mergedRfmResult.getOrdersCount());
        assertEquals(rfmSpeedResult.getOrdersSubTotal(), mergedRfmResult.getOrdersSubTotal());
    }

    @Test
    public void mergeRfmResultsTest() {
        ZonedDateTime now = ZonedDateTime.now(ZoneId.of("UTC"));
        ZonedDateTime yesterday = now.minusDays(1L);
        Integer applicationId = 1000;
        BigInteger customerId = BigInteger.valueOf(1);
        String companyName = "test company";
        String customerEmail = "test@test.com";
        String customerCity = "Akron";
        String customerCountry = "USA";
        String customerName = "test customer";
        String customerState = "OH";

        RFMResult rfmBatchResult =
                new RFMResult(applicationId, customerId, BigDecimal.valueOf(5), BigDecimal.valueOf(14.99d),
                              companyName, customerCity, customerCountry, yesterday, customerEmail, null,
                              customerName, customerState, BigDecimal.valueOf(5.00d), yesterday,
                              BigDecimal.valueOf(10.00d), yesterday, 3, BigDecimal.valueOf(20.00d));

        RFMResult rfmSpeedResult =
                new RFMResult(applicationId, customerId, BigDecimal.valueOf(2), BigDecimal.valueOf(5.99d),
                              companyName, customerCity, customerCountry, now, customerEmail, null,
                              customerName, customerState, BigDecimal.valueOf(3.00d), now, BigDecimal.valueOf(4.99d),
                              now, 1, BigDecimal.valueOf(14.55d));

        RFMResult mergedRfmResult = RFMResultsLogic.merge(rfmBatchResult, rfmSpeedResult);

        assertEquals(applicationId, mergedRfmResult.getApplicationId());
        assertEquals(customerId, mergedRfmResult.getCustomerId());
        assertEquals(BigDecimal.valueOf(3.50d).setScale(2), mergedRfmResult.getAverageDaysBetweenOrders());
        assertEquals(BigDecimal.valueOf(10.49d).setScale(2), mergedRfmResult.getAverageOrderPrice());
        assertEquals(companyName, mergedRfmResult.getCompanyName());
        assertEquals(customerCity, mergedRfmResult.getCustomerCity());
        assertEquals(rfmBatchResult.getCustomerCreatedAt(), mergedRfmResult.getCustomerCreatedAt());
        assertEquals(customerEmail, mergedRfmResult.getCustomerEmail());
        assertEquals(null, mergedRfmResult.getCustomerGroup());
        assertEquals(customerName, mergedRfmResult.getCustomerName());
        assertEquals(companyName, mergedRfmResult.getCompanyName());
        assertEquals(customerState, mergedRfmResult.getCustomerState());
        assertEquals(rfmBatchResult.getFirstOrderAmount(), mergedRfmResult.getFirstOrderAmount());
        assertEquals(rfmBatchResult.getFirstOrderDate(), mergedRfmResult.getFirstOrderDate());
        assertEquals(rfmSpeedResult.getLastOrderDate(), mergedRfmResult.getLastOrderDate());
        assertEquals(4, mergedRfmResult.getOrdersCount().intValue());
        assertEquals(BigDecimal.valueOf(34.55d).setScale(2), mergedRfmResult.getOrdersSubTotal());
    }
}
