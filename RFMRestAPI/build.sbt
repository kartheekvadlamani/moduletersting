name := """RFMRestAPI"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.11.7"

libraryDependencies ++= Seq(
  "com.datastax.cassandra" % "cassandra-driver-core" % "2.1.0",
  "io.swagger" %% "swagger-play2" % "1.5.2",
  "org.webjars" % "swagger-ui" % "2.1.8-M1",
  javaWs,
  "junit" % "junit" % "4.11" % Test,
  "junit" % "junit" % "4.11" % Runtime,
  "junit" % "junit" % "4.11" % Compile,
  "org.mockito" % "mockito-all" % "1.10.19"
)

// Play provides two styles of routers, one expects its actions to be injected, the
// other, legacy style, accesses its actions statically.
routesGenerator := InjectedRoutesGenerator
