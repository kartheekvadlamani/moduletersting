# Analytics Engine of Revenue Conduit

## Revenue Conduit Big Data Setup Instructions

1) Go through Vagrant Installation: https://github.com/paragjagdale/RevenueConduit-Vagrant 
   - At a minimum you need to install Vagrant, Virtualbox and precise64 and the vagrant-vbguest plugin. 

2) SSH into Vagrant ("vagrant ssh") then, follow the below steps to install Ambari.

``` 
sudo wget -O /etc/apt/sources.list.d/ambari.list http://public-repo-1.hortonworks.com/ambari/ubuntu14/2.x/updates/2.2.1.0/ambari.list
sudo apt-key adv --recv-keys --keyserver keyserver.ubuntu.com B9733A7A07513CAD
sudo apt-get update
sudo apt-get install ambari-server -y
sudo ambari-server setup -s
sudo ambari-server start
```

3) Edit /etc/hosts on your host machine and add the following line if it is not present. 

```
192.168.50.4    precise64
```    

4) Go to http://precise64:8080 on your host machine and login with username: "admin", password: "admin".
   Run through the configuration wizard to install Ambari.  Make sure to include the following services. 
    
   - HDFS
   - MapReduce2
   - YARN
   - Hive
   - Oozie
   - ZooKeeper
   - AmbariMetrics
   - Spark
   - Pig
   - Tez
   - Sqoop

   If you run into problems during the install, press the "Retry" button to try to install again.  If this still doesn't work, see the troubleshooting section below.

5) Install Java.

```
sudo apt-add-repository ppa:webupd8team/java
sudo apt-get update
sudo apt-get install oracle-java8-installer
```

6) Install Cassandra.

```
VERSION=`hdp-select status hadoop-client | sed 's/hadoop-client - \([0-9]\.[0-9]\).*/\1/'`
sudo git clone https://github.com/Symantec/ambari-cassandra-service.git /var/lib/ambari-server/resources/stacks/HDP/$VERSION/services/CASSANDRA
sudo service ambari-server restart
```

   - Goto the Ambari Web UI and choose Actions => "Add Service" at the bottom left corner and choose Cassandra.  Go through the install wizard, use "localhost" for the missing Cassandra config value that the installer highlights.

https://github.com/Symantec/ambari-cassandra-service

7) Troubleshooting:

###### Error encountered when starting hive: org.apache.hadoop.hive.metastore.HiveMetaException: Failed to get schema version.
```
mysql> CREATE USER 'hive'@'localhost' IDENTIFIED BY 'vagrant';
mysql> show grants for hive;
mysql> grant all on *.* to "hive"@"localhost" identified by "vagrant";
mysql> flush privileges;

$ /usr/hdp/current/hive-metastore/bin/schematool -initSchema -dbType mysql -userName hive -passWord hive 
```

###### Error encounter when oozie won’t start:
resource_management.core.exceptions.Fail: Execution of '/usr/hdp/current/oozie-server/bin/oozie-setup.sh sharelib create -fs hdfs://precise64:8020 -locallib /usr/hdp/current/oozie-server/share' returned 1.   setting OOZIE_CONFIG=${OOZIE_CONFIG:-/usr/hdp/current/oozie-server/conf}

Change /etc/hadoop/conf/core-site.xml to include 127.0.0.1 and localhost (Do this in the Ambari editor and restart all affected services or the config changes won’t take affect).
```
<property>
     <name>hadoop.proxyuser.oozie.hosts</name>
     <value>precise64,127.0.0.1,localhost</value>
</property>
```

###### Cassandra installation error (package dsc21):
```
echo "deb http://debian.datastax.com/community stable main" | tee -a /etc/apt/sources.list.d/cassandra.sources.list
curl -L http://debian.datastax.com/debian/repo_key | apt-key add -
apt-get install dsc21=2.1.8-1 cassandra=2.1.8
```

###### Miscellaneous:

```
sudo apt-get install ntp -y
sudo apt-get install hugepages -y
sudo hugeadm --thp-never
```

###### Set JAVA_HOME in /etc/environment
```
JAVA_HOME=/usr/lib/jvm/java-8-oracle/
```
