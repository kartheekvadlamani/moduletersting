name := "ModuleTersting"

version := "1.0"

scalaVersion := "2.10.5"


// https://mvnrepository.com/artifact/com.datastax.spark/spark-cassandra-connector_2.11
libraryDependencies += "com.datastax.spark" % "spark-cassandra-connector_2.10" % "1.6.0"

// https://mvnrepository.com/artifact/org.apache.spark/spark-core_2.11
libraryDependencies += "org.apache.spark" % "spark-core_2.10" % "1.6.0"

// https://mvnrepository.com/artifact/org.apache.cassandra/cassandra-thrift
libraryDependencies += "org.apache.cassandra" % "cassandra-thrift" % "2.1.2"

// https://mvnrepository.com/artifact/org.apache.spark/spark-streaming-twitter_2.11
libraryDependencies += "org.apache.spark" % "spark-streaming-twitter_2.10" % "1.6.0"

// https://mvnrepository.com/artifact/com.datastax.spark/spark-cassandra-connector-java_2.11
libraryDependencies += "com.datastax.spark" % "spark-cassandra-connector-java_2.10" % "1.5.1"

// https://mvnrepository.com/artifact/org.slf4j/slf4j-log4j12
libraryDependencies += "org.slf4j" % "slf4j-log4j12" % "1.7.21"

// https://mvnrepository.com/artifact/org.apache.spark/spark-streaming_2.11
libraryDependencies += "org.apache.spark" % "spark-streaming_2.10" % "1.6.0"